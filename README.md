# README #

SHIV - Simple HTML Interactive Visualizator, is a simple web based interactive data visualization software, which uses the ESA's Gaia Mission, DPAC Coordination Unit 9 Visualization Infrastructure WP [ObjectServer](https://bitbucket.org/sim-vis/objectserver/) software as the server backend.

### What is this repository for? ###

This repository stores the Javascript code used to build SHIV as well as a sample html page to test the software.

Check the [Issue tracker](https://bitbucket.org/miguel_gomes/shiv/issues?status=new&status=open) for the current milestone [Alpha](https://bitbucket.org/miguel_gomes/shiv/issues?milestone=Alpha) and the [Wiki](https://bitbucket.org/miguel_gomes/shiv/wiki) for other information.

### How do I get set up? ###

To set up the application you just need to checkout the code and open index.html on any recent browser.
You will also need a running instance of [ObjectServer](https://bitbucket.org/miguel_gomes/objectserver/overwiew) for which you have credentials.

### Contribution guidelines ###

* Usual git workflow, see [here](https://confluence.atlassian.com/display/BITBUCKET/Work+with+pull+requests).

### Who do I talk to? ###

* Myself, [Miguel Gomes](mailto:alka.setzer@gmail.com)
/**
 * SHIV Enumeration objects.
 *
 * @author Miguel Gomes (alka.setzer@gmail.com)
 * @since 0.1
 * @depends shiv.js
 */

console.log("loading shiv-enums.js...");

/**
 * Enumeration of possible Server states
 */
 SHIV.Enums.ServerState = {
	'UNKNWON': { 'id':"UNKNWON",'description': "Server state is unknown" }, // i8n?
	'OFFLINE': { 'id':"OFFLINE",'description': "Server is offline (i.e. a connection attempt was made but there was no response)" }, // i8n?
	'CONNECTING': { 'id':"CONNECTING",'description': "Connecting to the server" }, // i8n?
	'CONNECTED': { 'id':"CONNECTED",'description': "connected to the server" }// i8n?
};

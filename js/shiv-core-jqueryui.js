/**
 * Method used to show a message.
 * @param type Message type (one of: "info", "warning", "error")
 * @param msg Message content
 * @param header Message header (optional)
 * @param title Message title (optional)
 * @param targetToAppend Where to append the message (optional, default is content) 
 * @param onClickFunction Function to call onclick after the close action is executed
 */
SHIV.showMessage = function(type,msg,header,title,targetToAppend,onClickFunction) {
	if (type == null || type === undefined) return; // no type, no message
	type = type.toLowerCase();
	if (type != "info" &&
		type != "warning" &&
		type != "error") return; // invalid type
	var msgId = "msg-"+type+"-"+Date.now();
	var contentDiv = null;
	if (targetToAppend == null || targetToAppend === undefined)
		contentDiv = document.getElementById("content");
	else if ((typeof targetToAppend).toLowerCase() == 'string')
		contentDiv = document.getElementById(targetToAppend);
	else if ((typeof targetToAppend).toLowerCase() == 'object')
		contentDiv = targetToAppend;
	if (contentDiv == null)
		contentDiv = document.getElementById("content");
	var msgDiv = document.createElement("div");
	msgDiv.id = msgId;
	msgDiv.className = "msg "+type;
	if (title != null && title !== undefined)
		msgDiv.title = title;
	else {
		if (type == "info") msgDiv.title = "Information";// i8n
		else if (type == "warning") msgDiv.title = "Warning";// i8n
		else if (type == "error") msgDiv.title = "Error";// i8n
	}				
	if (header != null && header !== undefined) {
		var h2 = document.createElement("h2");
		h2.className = "header"
		if (header == null && header === undefined)
			header = type[0].toUpperCase()+type.substring(1);
		h2.appendChild(document.createTextNode(header));
		msgDiv.appendChild(h2);
	}
	var cntDiv = document.createElement("div");
	cntDiv.className = "ui-corner-all ui-state-";
	if (type == "info") cntDiv.className +="default";
	else if (type == "warning") cntDiv.className += "highlight";
	else if (type == "error") cntDiv.className += "error";
	cntDiv.style.padding="0 .7em";
	var p = document.createElement("p");
	var span = document.createElement("span");
	span.className="ui-icon ui-icon-";
	if (type == "info") span.className +="info";
	else if (type == "warning") span.className += "notice";
	else if (type == "error") span.className += "alert";
	span.style.float = "left";
	span.style.marginRight = ".3em";
	p.appendChild(span);
	var msgCnt = document.createElement("span");
	msgCnt.className = "content"
	if (msg.indexOf("<") >= 0 && msg.indexOf(">") > 0) {
		msgCnt.innerHTML = msg;
	} else
		msgCnt.appendChild(document.createTextNode(msg));
	p.appendChild(msgCnt);
	cntDiv.appendChild(p);
	msgDiv.appendChild(cntDiv);
	contentDiv.appendChild(msgDiv);
	return $( "#"+msgId ).dialog({
		autoOpen: true,
		width: 320,
		resizable: true,
		buttons: [
			{
				text: "OK",// i8n
				click: function() {
					$( this ).dialog( "close" );
					if (onClickFunction != null && onClickFunction !== undefined) 
						onClickFunction();
				}
			}
		]
	});
}
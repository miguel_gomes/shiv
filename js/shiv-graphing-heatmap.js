/**
 * SHIV graphing methods.
 *
 * @author Miguel Gomes (alka.setzer@gmail.com)
 * @depends d3.js (>= 3.5)
 * @depends shiv-graphing.js (>= 0.1)
 * @since 0.1
 */

console.log("loading shiv-graphing-heatmap.js...");

/**
 * Creates an Heatmap plot.
 * @param data Data to render (array of arrays, where each row is a new data point)
 * @param params Plot parameters
 * @return Heatmap
 */
SHIV.graph.HeatMap = function(params) {
	var chart = this.chart = this; // recursive link
	if (params === undefined || params == null)
		params = SHIV.graph.defaultParams();
	else {
		params = SHIV.graph.mergeParams(params);
	}
	this.params = params;
	params.chart = this;
	SHIV.graphManager.addGraph(params);
	var xScale = this.xScale = null;
	var yScale = this.yScale = null;
	var grid = null;
	var paramsMap = {}; // parameters map
	
	this.updateAxisType(paramsMap);

	// then we need to create the graph
	var svg = this.svg = d3.select(params.bindto == null ? 'body' : params.bindto).append("svg")
		.attr("width", params.width)
		.attr("height", params.height)
		.append("g")
		.attr("transform", "translate(" + params.margin.left + "," + params.margin.top + ")");
	var svgElem = this.svgElem = d3.select(params.bindto == null ? 'body' : params.bindto).select('svg');
	this.params['svg-elem'] = this.svgElem[0][0];

	var clipPath = this.svg.append("svg:clipPath")
						.attr("id","clip")
						.append("svg:rect")
							.attr("id","clip-rect")
							.attr("x",0)
							.attr("y",0)
							.attr("width",params.width-(params.margin.left+params.margin.right))
							.attr("height",params.height-(params.margin.top+params.margin.bottom));

	var auxArea = this.svg.append("g")
					.attr("id","aux-area");
	this.auxArea = this.params["auxArea"] = auxArea;

	// create zoom area
	var zoomArea = svg.append("g")
			.attr("class","zoomarea")
			.append("rect")
				.attr("x", "0")
				.attr("y", "0")
				.attr("width", params.width-(params.margin.right+params.margin.left))
				.attr("height", params.height-(params.margin.top+params.margin.bottom))
				.style("opacity","0");
	this.zoomArea = params.zooming.elem = zoomArea;

	var chartBody = this.svg.append("g")
				.attr("clip-path", "url(#clip)");
	this.params['chartbody'] = chartBody;
	this.chartBody = chartBody;

	// tooltip
	var tooltipDiv = null;
	var tooltipFormat = null;
	if (params.tooltip.enabled) {
		this.tooltipDiv = tooltipDiv = d3.select("body").append("div") 
					.attr("class", "tooltip right")
					.attr("role", "tooltip")
					.attr("id", "tooltip-"+(params.bindto == null ? 'body' : params.bindto))
					.style("opacity", 0);
		params.tooltip.elem = tooltipDiv;
		if (params.tooltip.format !== undefined && params.tooltip.format != null)
			tooltipFormat = params.tooltip.format;
		else
			tooltipFormat = d3.format(".3f");
		this.tooltipFormat = tooltipFormat;
	}

	// now bind some usefull stuf
	this.params.svg = svg;
	// then we add the axis

	//	this.updateAxis();

	this.updateAxisX();
	this.updateAxisY();
	if (params.legend.enabled)
		this.updateLegend();

	var xAxis = this.xAxis;
	var xAxisMinor = this.xAxisMinor;
	var xAxisGrid = this.xAxisGrid;
	var yAxis = this.yAxis;
	var yAxisMinor = this.yAxisMinor;
	var yAxisGrid = this.yAxisGrid;

	// attach brush
	if (params.brushing.enabled)
		this.attachBrush();

	// attach zoom
	if (params.zooming.enabled)
		this.attachZoom();

	this.updateData();
}

SHIV.graph.HeatMap.prototype.ObjectType = "HeatMap";
SHIV.graph.HeatMap.prototype.ObjectClassName = "tile";
SHIV.graph.HeatMap.prototype.Dimensions = 2;
SHIV.graph.HeatMap.prototype.SupportsSize = false;
SHIV.graph.HeatMap.prototype.SupportsColor = false;
SHIV.graph.HeatMap.prototype.SupportsShape = false;
SHIV.graph.HeatMap.prototype.SupportsColorRange = true;

SHIV.graph.HeatMap.prototype.updateAxisType = function(paramsMap) {
	var params = this.params;
	var xInverted = this.params.axis.x.inverted;
	var yInverted = this.params.axis.y.inverted;
	if (params.data !== undefined && params.data != null) {
		for (var i = 0; i < params.data.length; i++) {
			var dataset = params.data[i].data;
			var data = params.data[i];

			if (paramsMap !== undefined && paramsMap != null && data['source-id'] != null)
				paramsMap[data['source-id']] = data;

			// first we need to create the scales
			if (data.x.scale == null) 
				xScale = SHIV.graph.createScale(data.x.type);
			else {
				var _xScale = SHIV.graph.createScale(data.x.type);
				if (""+_xScale != ""+xScale)
					xScale = _xScale;
				else
					xScale = data.x.scale;
			}
			if (data.y.scale == null) 
				yScale = SHIV.graph.createScale(data.y.type);
			else {
				var _yScale = SHIV.graph.createScale(data.y.type);
				if (""+_yScale != ""+yScale)
					yScale = _yScale;
				else
					yScale = data.y.scale;
			}
			// then figure out the domains
			if (data.x.type != "ordinal") {
				if (data.x.domain == null) {
					var dfunctor = data.x.domainFunctor;
					if (dfunctor == null)
						dfunctor = function(d) { 
							return d[data.x.idx]; 
						}
					// also sort data
					data.data.sort(function(a,b) {
						a = a[data.x.idx];
						b = b[data.x.idx];
						return a - b;
					});
					xScale.domain(d3.extent(dataset,dfunctor));
					data.x.domain = xScale.domain();
				}
				else {
					var dfunctor = data.x.domainFunctor;
					if (dfunctor == null)
						dfunctor = function(d) { 
							return d[data.x.idx]; 
						}
					// also sort data
					data.data.sort(function(a,b) {
						a = a[data.x.idx];
						b = b[data.x.idx];
						return a - b;
					});
					var newDomain = d3.extent(dataset,dfunctor);
					var oldDomain = data.x.domain;
					if (newDomain[0] != oldDomain[0] || newDomain[1] != oldDomain[1])
						xScale.domain(newDomain);
					else
						xScale.domain(oldDomain);
				}

				xScale.range([0, params.width - (params.margin.left + params.margin.right)]);
			} else {
				xScale.rangeRoundBands([0,params.width-(params.margin.left+params.margin.right)],0.01);
				xScale.domain(data.x["rows-description"].map(function(d) { return d; }));
			}
			if (data.y.type != "ordinal") {
				if (data.y.domain == null) {
					var dfunctor = data.y.domainFunctor;
					if (dfunctor == null)
						dfunctor = function(d) { 
							return (yInverted ? -1.0 : 1.0) * d[data.y.idx]; 
						}
					yScale.domain(d3.extent(dataset,dfunctor));
					data.y.domain = yScale.domain();
				}
				else
				{
					var dfunctor = data.y.domainFunctor;
					if (dfunctor == null)
						dfunctor = function(d) { 
							return (yInverted ? -1.0 : 1.0) * d[data.y.idx]; 
						}
					var newDomain = d3.extent(dataset,dfunctor);
					var oldDomain = data.y.domain;
					if (newDomain[0] != oldDomain[0] || newDomain[1] != oldDomain[1])
						yScale.domain(newDomain);
					else
						yScale.domain(oldDomain);
				}

				yScale.range([params.height - (params.margin.top + params.margin.bottom),0]);
			} else {
				yScale.rangeRoundBands([params.height-(params.margin.top+params.margin.bottom),0],0.01);
				yScale.domain(data.y["cols-description"].map(function(d) { return d; }));
			}
			
			this.xScale = data.x.scale = xScale;
			this.yScale = data.y.scale = yScale;

			// heatmap has no options other than color

			// color / alpha
			var opacity = data.opacity;
			if (opacity === undefined || opacity == null)
				opacity = 1.0;
			else {
				var aux = parseFloat(opacity);
				if (isNaN(aux)) opacity = 1.0;
				else if (aux < 0) opacity = 0.0;
				else if (aux > 1.0) opacity = 1.0;
				else
					opacity = aux;
			}
			var fillColor = data.color;
			if (fillColor === undefined)
				fillColor = null;
			data.opacity = opacity;

			if (data.color === undefined || data.color == null) {
				data.color = { "min": "#FFFFFF", "max": "#4682B4" };
			} else {
				if (data.color.min === undefined || data.color.min == null) data.color.min = "#FFFFFF";
				if (data.color.max === undefined || data.color.max == null) data.color.max = "#4682B4";
			}

			if (params.type != "heatmap-table")
				this.gridFromData(dataset,xScale,yScale,data);
			else { // for this we also have a color scale
				data.color.scale = SHIV.graph.createScale("linear");
				if (data.color.range === undefined || data.color.range == null) {
					data.color.range = "custom";
					data.color.scale.range([data.color.min,data.color.max]);
				}
				else {
					var card = data.color.cardinality;
					if (card === undefined || card == null) {
						card = "8";
						data.color.cardinality = card;
					}
					if (data.color.range != "custom")
						data.color.scale.range(colorbrewer[data.color.range][card]);
					else
						data.color.scale.range([data.color.min,data.color.max]);
				}
				var dfunctor = data.color.domainFunctor;
				if (dfunctor == null)
					dfunctor = function(d) { 
						return d[data.color.idx]; 
					}
				var newDomain = d3.extent(dataset,dfunctor);
				var oldDomain = data.color.domain;
				if (oldDomain === undefined || oldDomain == null) oldDomain = [Number.NaN,Number.NaN];
				if (newDomain[0] != oldDomain[0] || newDomain[1] != oldDomain[1])
					data.color.scale.domain(newDomain);
				else
					data.color.scale.domain(oldDomain);
				data.color.domain = data.color.scale.domain();
			}
		}
	}
}

SHIV.graph.HeatMap.prototype.updateData = function() {
	// first remove all previous points
	this.chartBody.selectAll(".tile").remove();
	this.tiles = tiles = this.chartBody.selectAll(".tile");
	// now, data
	var start = Date.now();
	var totalPoints = 0;
	for (var i = 0; i < this.params.data.length; i++) {
		var dataParams = this.params.data[i];
		var dataset = dataParams.data;
		totalPoints+= dataset.length;
		var dataPoints = this.chartBody.selectAll(".tile");
		var sizeFn = dataParams.size.functor;
		var opacity = dataParams.opacity;
		if (this.params.type != "heatmap-table") {
			var sourceUidFn = dataParams.source.functor;
			var xFunc = function(d) { return d[0]; };
			var yFunc = function(d) { return d[1]; };
			var fillColor = function(d) { return dataParams.grid.scale(d[2]); };
			var cp = Date.now();
			dataPoints = this.createTiles(dataParams,dataPoints,dataParams.grid.data,dataParams.keyFunction,sizeFn,xFunc,yFunc,sourceUidFn,opacity,fillColor);
			//console.log("Create[B] Create points took: "+(Date.now() - cp)+" ms");
			this.createTilesEventHandlers(false, dataParams, dataPoints, this.params);
		} else {
			var rowsDesc = dataParams.x['rows-description'];
			var colsDesc = dataParams.y['cols-description'];
			var xFunc = function(d) { return dataParams.x.scale(rowsDesc[d[dataParams.x.idx]]); };
			var yFunc = function(d) { return dataParams.y.scale(rowsDesc[d[dataParams.y.idx]]); };
			var fillColor = function(d) { return dataParams.color.scale(d[dataParams.color.idx]); };
			var sourceUidFn = function(d) { return d[dataParams.x.idx]; };
			dataPoints = this.createTableTiles(dataParams,dataPoints,dataParams.data,dataParams.keyFunction,sizeFn,xFunc,yFunc,sourceUidFn,opacity,fillColor);
			this.createTableTilesEventHandlers(false, dataParams, dataPoints, this.params);
		}

	}
}

SHIV.graph.HeatMap.prototype.createTableTiles = function(dataParams,selection,data,keyFunction,sizeFn,xFunc,yFunc,sourceUidFn,opacity,fillColor) {
	var width = dataParams.x.scale.rangeBand();
	var height = dataParams.y.scale.rangeBand();
	return selection.data(data,keyFunction)
			.enter()
				.append("rect")
					.attr("class", "tile")
					.attr("width", width)
					.attr("height", height)
					.attr("x", xFunc)
					.attr("y", yFunc)
					.attr("source-id", dataParams['source-id'])
					.attr("source-uid", sourceUidFn)
					.style("opacity",opacity)
					.style("fill",fillColor);
}

SHIV.graph.HeatMap.prototype.createTableTilesEventHandlers = function(isGrid, dataParams, selection, params)
{
	var tooltipDiv = this.tooltipDiv !== undefined ? this.tooltipDiv : params.tooltip.div;
	var tooltipFormat = this.tooltipFormat !== undefined ? this.tooltipFormat : params.tooltip.format;
	var rowsDesc = dataParams.x['rows-description'];
	var colsDesc = dataParams.y['cols-description'];
	selection.on("mouseover", function(d) {
		if (tooltipDiv != null) {
			tooltipDiv/*.transition()
				.duration(200)*/
				.style("opacity", .9);
			var xVal = rowsDesc[d[dataParams.x.idx]];
			var yVal = rowsDesc[d[dataParams.y.idx]];
			var zVal = d[dataParams.color.idx];
			var uids = d[dataParams.x.idx];
			// <div class=\"tooltip-arrow\"></div><div class=\"tooltip-inner\">+"</div>"
			tooltipDiv.html("x: "+xVal+"<br />y: "+yVal+"<br />value: "+zVal+"<br />uid: "+uids)
				.style("left", (d3.event.pageX + 10) + "px")
				.style("top", (d3.event.pageY - 28) + "px");
		}
		this.style.cursor = "pointer";
	})
	.on("mouseout", function(d) {
		if (tooltipDiv != null)
						tooltipDiv/*.transition()
					.duration(500)*/
					.style("opacity", 0);
					this.style.cursor = "auto";
				})
	.on("click", function(d) {
		var elem = d3.select(this);
		if (!SHIV.utils.keyboard.ctrl && !SHIV.utils.keyboard.shift) {
			var res = d3.select(this.parentNode).selectAll(".tile")
			.each(function(d) { 
				d.scanned = d.selected = false; 
				d3.select(this).attr("class","tile");
			}
			);
		}
		d.selected = !d.selected;
		var pNode = this.parentNode.parentNode;
		while (pNode.nodeName != "svg")
			pNode = pNode.parentNode;
		var sid = elem.attr("source-id");
		var puid = elem.attr("source-uid");
		if (d.selected)
			elem.attr("class","tile selected");
		else
			elem.attr("class","tile");
		if (puid != null)
			SHIV.graphManager.highlight(pNode,sid,puid,d.selected);
		else if (d[3].length > 1)
			SHIV.graphManager.highlightAll(pNode,sid,d[3],d.selected);
	});
}

SHIV.graph.HeatMap.prototype.updateAxisX = function() {
	SHIV.graph.updateAxisX(this);
}
SHIV.graph.HeatMap.prototype.updateAxisY = function() {
	SHIV.graph.updateAxisY(this);
}

SHIV.graph.HeatMap.prototype.updateLegend = function() {
	// Add a legend for the color values.
	var data0 = this.params.data[0];
	if (data0 === undefined || data0 == null) return;
	var scale = data0.color.scale;
	var width = this.params.width-(this.params.margin.left+this.params.margin.right);
	this.params.auxArea.selectAll(".legend").remove();
	var legend = this.params.auxArea.selectAll(".legend")
			.data(scale.ticks(10).slice(1).reverse())
		.enter().append("g")
			.attr("class", "legend")
			.attr("transform", function(d, i) { return "translate(" + (width+10) + "," + (20 + i * 20) + ")"; });

	legend.append("rect")
			.attr("width", 20)
			.attr("height", 20)
			.style("fill", scale);

	legend.append("text")
			.attr("x", 26)
			.attr("y", 10)
			.attr("dy", ".35em")
			.text(String);
}

SHIV.graph.HeatMap.prototype.resizeViewport = function(width,height,maintainDomains) {
	SHIV.graph.resizeViewport(this,width,height,maintainDomains);
}

SHIV.graph.HeatMap.prototype.detachBrush = function() {
	SHIV.graph.detachBrush(this);
}

SHIV.graph.HeatMap.prototype.attachBrush = function(params) {
	if (params === undefined || params == null) params = this.params;

	// create brush area
	var brushArea = this.svg.append("g")
				.attr("class", "brush");
	this.brushArea = params.brushing.elem = brushArea;

	var chart = this;

	var brush = d3.svg.brush()
		.x(d3.scale.identity().domain([0, params.width - (params.margin.left+params.margin.right)]))
		.y(d3.scale.identity().domain([0, params.height - (params.margin.top+params.margin.bottom)]))
		.on("brushstart", params.brushing.events.brushstart != null ? params.brushing.events.brushstart : null)
		.on("brush", params.brushing.events.brush != null ? params.brushing.events.brush : function() { 
			// find extents by brush
			var extent = d3.select(this).select(".extent");
			var width = parseInt(extent.attr('width'));
			var height = parseInt(extent.attr('height'));
			var ex = parseInt(extent.attr("x"));
			var ey = parseInt(extent.attr("y"));
			var addPoints = SHIV.utils.keyboard.ctrl;
			var removePoints = SHIV.utils.keyboard.shift;
			if (!addPoints && !removePoints) {
				d3.select(this.parentNode.parentNode).selectAll(".tile").attr()
				.each(function(d) { 
					d.scanned = d.selected = false; 
					d3.select(this).attr("class","tile");
				}
				);
			}
			if (width == 0 && height == 0) return; // nothing to do
			if (!removePoints) addPoints = true;
			d3.select(this.parentNode.parentNode)
			.selectAll(".tile")
				.each(function(d,i) { //
					var dp = d3.select(this);
					var dx, dy;
					if (this.nodeName == "circle") {
						dx = parseInt(dp.attr('cx'));
						dy = parseInt(dp.attr('cy'));
					} else {
						dx = parseInt(dp.attr('x'));
						dy = parseInt(dp.attr('y'));
					}
					var inside = !( dx < Math.max(ex,0) || dx > ex + width ||
						dy < Math.max(ey,0) || dy > ey + height);
					if (!inside) return;
					if (addPoints)
						dp.attr("class","tile selected")
					else if (removePoints && inside)
						dp.attr("class","tile")
						//console.log(this);
						return this;
					});
			})
		.on("brushend",params.brushing.events.brushend != null ? params.brushing.events.brushend : function() {
			// find extents by brush
			var extent = d3.select(this).select(".extent");
			var width = parseInt(extent.attr('width'));
			var height = parseInt(extent.attr('height'));
			var ex = parseInt(extent.attr("x"));
			var ey = parseInt(extent.attr("y"));
			if (width == 0 && height == 0) return; // nothing to do
			var sels = {};
			var addPoints = SHIV.utils.keyboard.ctrl;
			var removePoints = SHIV.utils.keyboard.shift;
			d3.select(this.parentNode.parentNode)
			.selectAll(".tile.selected")
			.each(function(d) {
				var elem = d3.select(this);
				var dx, dy;
				var sourceId = elem.attr("source-id");
				var uid = elem.attr("source-uid");
				var vals; 
				if ((vals = sels[sourceId]) == null)
					vals = sels[sourceId] = [];
				if (uid == null && d[3] !== undefined) {
					for (var jj = 0; jj < d[3].length; jj++)
						vals.push(d[3][jj]);
				} else
					vals.push(uid);
			});
			for (var p in sels) {
				var vals = sels[p];
				if (vals === undefined || vals == null) continue;
				SHIV.graphManager.highlightAll(this.parentNode.parentNode,p,vals,!removePoints);
			}
		});
	params.brushing.enabled = true;
	params.brushing.brush = brush;
	params.brushing.elem.call(brush);
}

SHIV.graph.HeatMap.prototype.zoomEvent = function(howMuch,evtSource) {
	if (howMuch === undefined || howMuch == null) return;
	if (!this.params.zooming.enabled) return;
	var zoom = this.params.zooming.zoomVar;

	//this.svg.call(zoom.event); // https://github.com/mbostock/d3/issues/2387

	// Record the coordinates (in data space) of the center (in screen space).
	var center0 = zoom.center();
	if (center0 == null) {
		// need to set the center
		var xScale = zoom.x();
		var yScale = zoom.y();
		var xDomain = xScale.range();
		var yDomain = yScale.range();
		var x = (xDomain[1]+xDomain[0])*0.5;
		var y = (yDomain[0]+yDomain[1])*0.5;
		center0 = [x,y];
	}
	var translate0 = zoom.translate();
	var scale = zoom.scale();
	var coordinates0 = [(center0[0] - translate0[0]) / scale, (center0[1] - translate0[1]) / scale];
	zoom.scale(zoom.scale() * Math.pow(2, +howMuch));
	
	// Translate back to the center
	var translate = zoom.translate();
	scale = zoom.scale();
	var center1 = [coordinates0[0] * scale + translate[0], coordinates0[1] * scale + translate[1]];
	zoom.translate([translate0[0] + center0[0] - center1[0], translate0[1] + center0[1] - center1[1]]);
	translate = zoom.translate();

	this.updateAxis();

	// no point in doing transitions because we clear the points
	//zoom.event.scale =zoom.scale();
	//zoom.event.translate = zoom.translate();
	if (d3.event == null) {
		d3.event = {}
	}
	d3.event.sourceEvent = evtSource;
	d3.event.type = "zoom";
	d3.event.target = zoom;
	d3.event.scale = scale;
	d3.event.translate = translate;
	this.svg.call(zoom.event);
}

SHIV.graph.HeatMap.prototype.zoomIn = function(evt,howMuch) {
	if (howMuch === undefined || howMuch == null) howMuch = "+1";
	this.zoomEvent(howMuch,evt);
}

SHIV.graph.HeatMap.prototype.zoomOut = function(evt,howMuch) {
	if (howMuch === undefined || howMuch == null) howMuch = "-1";
	this.zoomEvent(howMuch,evt);
}

SHIV.graph.HeatMap.prototype.detachZoom = function() {
	this.params.zooming.enabled = false;
	this.params.zooming.elem.on(".zoom",null);
}

SHIV.graph.HeatMap.prototype.attachZoom = function() {
	var params = this.params;
	params.zooming.enabled = true;

	var attachToPoints = true;
	var svg = params.svg;
	var chartBody = params.chartbody;
	// TODO: support multiple axis and scales
	var xAxis = params.axis.x.xAxis !== undefined ? params.axis.x.xAxis : this.xAxis;
	var yAxis = params.axis.y.yAxis !== undefined ? params.axis.y.yAxis : this.yAxis;
	var xAxisMinor = params.axis.x.xAxisMinor !== undefined ? params.axis.x.xAxisMinor : this.xAxisMinor;
	var yAxisMinor = params.axis.y.yAxisMinor !== undefined ? params.axis.y.yAxisMinor : this.yAxisMinor;
	var xAxisGrid = params.axis.grid.xAxisGrid !== undefined ? params.axis.grid.xAxisGrid : this.xAxisGrid;
	var yAxisGrid = params.axis.grid.yAxisGrid !== undefined ? params.axis.grid.yAxisGrid : this.yAxisGrid;
	var xScale = params.data[0].x.scale !== undefined ? params.data[0].x.scale : this.xScale;
	var yScale = params.data[0].y.scale !== undefined ? params.data[0].y.scale : this.yScale;
	var chart = this;
	var zoom = d3.behavior.zoom(svg).x(xScale).y(yScale).size([xScale.range()[1],yScale.range()[0]]);
	params.zooming.zoomVar = zoom;
	params.zooming.lastScale = zoom.scale();
		
	if (params.zooming.events.zoomstart != null)
		zoom.on("zoomstart", params.zooming.events.zoomstart);
	if (params.zooming.events.zoom != null)
		zoom.on("zoom", params.zooming.events.zoom);
	else
		zoom.on("zoom", function() {
			var evt = d3.event;
			if (Object.prototype.toString.call(evt.sourceEvent) == "[object Object]")
				evt = evt.sourceEvent; // bubbling
			if (xAxis != null) svg.select(".x.axis").call(xAxis);
			if (yAxis != null) svg.select(".y.axis").call(yAxis);
			if (xAxisMinor != null) svg.select(".x.axis.minor").call(xAxisMinor);
			if (yAxisMinor != null) svg.select(".y.axis.minor").call(yAxisMinor);
			if (xAxisGrid != null) svg.select(".x.grid").call(xAxisGrid);
			if (yAxisGrid != null) svg.select(".y.grid").call(yAxisGrid);
			chart.updateAxis();

			var params = chart.params;

			params.zooming.lastScale = evt.scale;
			chartBody.selectAll(".tile").remove(); // and any points
			for (var i = 0; i < params.data.length; i++) {
				var pts = [];
				var dataParams = params.data[i];
				var xScale = dataParams.x.scale;
				var yScale = dataParams.y.scale;
				var xRange = xScale.range();
				var yRange = yScale.range();
				var _xScale = SHIV.graph.createScale(dataParams.x.type).domain(dataParams.x.domain).range(xRange);
				var _yScale = SHIV.graph.createScale(dataParams.y.type).domain(dataParams.y.domain).range(yRange);
				var xDomain = xScale.domain();
				var yDomain = yScale.domain();
				var lines = chartBody.selectAll(".tile");
		
				var opacity = dataParams.opacity;
				var sourceUidFn = dataParams.source.functor;
				var fillColor = dataParams.color;
				var sizeFn = dataParams.size.functor;
				var xFunc = function(d) { return dataParams.x.scale(d[dataParams.x.idx]); };
				var yFunc = function(d) { return dataParams.y.scale(d[dataParams.y.idx]); };
				var cp = Date.now();

				// need to createBars
				lines = chart.createTiles(dataParams,lines,dataParams.data,dataParams.keyFunction,sizeFn,xFunc,yFunc,sourceUidFn,opacity,fillColor);
				// need to createBarsEvens
				chart.createTilesEventHandlers(false, dataParams, lines, this.params); 
				// need to call actions
				if (pts.length > params.reductionPts) {
					var func = params.zooming.actions['high-data'];
					if (func !== undefined && func != null)
						func(params,dataParams,pts.length); // execute high-data function
				} else if (pts.length > 0 ) {
					var func = params.zooming.actions['low-data'];
					if (func !== undefined && func != null)
						func(params,dataParams,pts.length); // execute low-data function
				} else {
					// actions
					var func = params.zooming.actions['no-data'];
					if (func !== undefined && func != null)
						func(params,dataParams,pts.length); // execute no-data function
				}
			}
		});
	if (params.zooming.events.zoomend != null)
		zoom.on("zoomend", params.zooming.events.zoomend);

	// call the function
	params.zooming.elem.call(zoom);
	if (attachToPoints)
		svg.selectAll(".tile").call(zoom);
}

SHIV.graph.HeatMap.prototype.gridFromData = function(dataset,xScale,yScale,data,noQuad,gridData) {
	if (data.length == 0) return;
	start = Date.now();
	var sparsegrid = [];
	var sparsegridMin = Number.MAX_VALUE;
	var sparsegridMax = 0; // can't be less than this
	if (noQuad === undefined) noQuad = false;
	// domains
//	console.log("xScale.domain() = "+xScale.domain()+", yScale.domain() = "+yScale.domain());
	var scaleSizeX = Math.ceil((data.x.scale.domain()[1] - data.x.scale.domain()[0]) / (data.x.scale.ticks().length*0.5));
	var scaleSizeY = Math.ceil((data.y.scale.domain()[1] - data.y.scale.domain()[0]) / (data.y.scale.ticks().length*0.5));
	var quadtree = null;
	var addedPoints = 0;
	var outsidePoints = 0;
	var xMax = xScale.range[1];
	var yMax = yScale.range[0];
	if (!noQuad) {
		// need to use actual data domains for quadtree
		var _xScale = SHIV.graph.createScale(data.x.type);
		var _yScale = SHIV.graph.createScale(data.y.type);
		_xScale.domain(data.x.domain).range(xScale.range());
		_yScale.domain(data.y.domain).range(yScale.range());
		quadtree = d3.geom.quadtree().extent([[-1, -1], [xScale.range()[1] + 1, yScale.range()[0] + 1]])([]);
		for (var i = 0; i < dataset.length; i++) {
			var point = dataset[i];
			var x = xScale(point[data.x.idx]);
			var y = yScale(point[data.y.idx]);
			if (x < 0 || x > xMax || y < 0 || y > yMax) {
				outsidePoints++;
				continue; // skip this point as it's outside of the viewport
			}
			// bins are 5 pixels wide
			var ix = Math.floor(x / scaleSizeX);
			var iy = Math.floor(y / scaleSizeY);
			var ygrid = sparsegrid[ix];
			if (ygrid == null) {
				sparsegrid[ix] = [];
				ygrid = sparsegrid[ix];
				ygrid[iy] = []; // so that we can update the value in the next step
			}
			if (ygrid[iy] == null/* || isNaN(ygrid[iy])*/ || ygrid[iy] === undefined)
				ygrid[iy] = []; // so that we can update the value in the next step
			var pushIndex = data.source.idx != -1 ? point[data.source.idx]: i;
			if (ygrid[iy].indexOf(pushIndex) >= 0) continue;
			ygrid[iy].push(pushIndex);
			var aux = ygrid[iy].length;
			if (aux < sparsegridMin) sparsegridMin = aux;
			if (aux > sparsegridMax) sparsegridMax = aux;
			// this is actual data scale
			x = _xScale(point[data.x.idx]);
			y = _yScale(point[data.y.idx]);
			//console.log("quadtree.add("+x+","+y+","+point+")...");
			quadtree.add([x,y,point]); // add backing to quadtree
			addedPoints++;
		}
		quadtree.lastMod = Date.now();
		//console.log("added "+dataset.length+" points to the quadtree("+quadtree.lastMod+")");

	} else {
		for (var i = 0; i < dataset.length; i++) {
			var point = dataset[i];
			var x = xScale(point[data.x.idx]);
			var y = yScale(point[data.y.idx]);
			if (x < 0 || x > xMax || y < 0 || y > yMax) {
				outsidePoints++;
				continue; // skip this point as it's outside of the viewport
			}
			// bins are 5 pixels wide
			var ix = Math.floor(x / scaleSizeX);
			var iy = Math.floor(y / scaleSizeY);
			var ygrid = sparsegrid[ix];
			if (ygrid == null) {
				sparsegrid[ix] = [];
				ygrid = sparsegrid[ix];
				ygrid[iy] = []; // so that we can update the value in the next step
			}
			if (ygrid[iy] == null/* || isNaN(ygrid[iy])*/ || ygrid[iy] === undefined)
				ygrid[iy] = []; // so that we can update the value in the next step
			var pushIndex = data.source.idx != -1 ? point[data.source.idx]: i;
			if (ygrid[iy].indexOf(pushIndex) >= 0) continue;
			ygrid[iy].push(pushIndex);
			var aux = ygrid[iy].length;
			if (aux < sparsegridMin) sparsegridMin = aux;
			if (aux > sparsegridMax) sparsegridMax = aux;
			addedPoints++;
		}
	}

	// convert the grid into x,y,c data
	var colorScale = d3.scale.linear().domain([sparsegridMin,sparsegridMax]).range([data.color.min,data.color.max]/*[0.3, 1.0]*/);
	var sparsegridData = [];
	for (var i = 0; i < sparsegrid.length; i++) {
		var xgrid = sparsegrid[i];
		if (xgrid == null) continue;
		for (var j = 0; j < xgrid.length; j++) {
			var ygrid = xgrid[j];
			if (ygrid == null) continue;
			sparsegridData.push([i*scaleSizeX,j*scaleSizeY,ygrid.length,ygrid.sort(function(a,b){return a-b;})]);
		}
	}
	var gridObject = data.grid = {
		'data': sparsegridData,
		'scale': colorScale
	}
	if (!noQuad)
		data.quadtree = quadtree;
	var t = Date.now() - start;
	if (t > 100)
		console.log("Sparse grid construction took "+(Date.now() - start)+" ms with "+sparsegridData.length+" of "+dataset.length+" points (added: "+addedPoints+", outside: "+outsidePoints+")");
	return gridObject;
}

SHIV.graph.HeatMap.prototype.createTiles = function(dataParams,selection,data,keyFunction,sizeFn,xFunc,yFunc,sourceUidFn,opacity,fillColor) {
	// create points if asked for
	var scaleSizeX = Math.ceil((dataParams.x.scale.domain()[1] - dataParams.x.scale.domain()[0]) / (dataParams.x.scale.ticks().length*0.5));
	var scaleSizeY = Math.ceil((dataParams.y.scale.domain()[1] - dataParams.y.scale.domain()[0]) / (dataParams.y.scale.ticks().length*0.5));
	return selection.data(data,keyFunction)
			.enter()
				.append("rect")
					.attr("class", "tile")
					.attr("width", scaleSizeX)
					.attr("height", scaleSizeY)
					.attr("x", xFunc)
					.attr("y", yFunc)
					.attr("source-id", dataParams['source-id'])
					.attr("source-uid", sourceUidFn)
					.style("opacity",opacity)
					.style("fill",fillColor);
}

SHIV.graph.HeatMap.prototype.createTilesEventHandlers = function(isGrid, dataParams, selection, params)
{
	var tooltipDiv = this.tooltipDiv !== undefined ? this.tooltipDiv : params.tooltip.div;
	var tooltipFormat = this.tooltipFormat !== undefined ? this.tooltipFormat : params.tooltip.format;
	selection.on("mouseover", function(d) {
		if (tooltipDiv != null) {
			tooltipDiv/*.transition()
				.duration(200)*/
				.style("opacity", .9);
			var xVal = tooltipFormat(parseFloat(dataParams.x.scale.invert(d[0])));
			var yVal = tooltipFormat(parseFloat(dataParams.y.scale.invert(d[1])));
			var uids = d[3] = d[3].sort(function(a,b){return a-b; });
			if (uids.length > 5) {
				var aux = "";
				for (var j = 0; j < 5; j++) {
					aux += d[3][j] +",";
				}
				aux+=".. ("+(uids.length-5)+" more)";
				uids = aux;
			}
			tooltipDiv.html("x: "+xVal+"<br />y: "+yVal+"<br />c: "+d[2]+"<br />uid: "+uids)
				.style("left", (d3.event.pageX + 10) + "px")
				.style("top", (d3.event.pageY - 28) + "px");
		}
		this.style.cursor = "pointer";
	})
	.on("mouseout", function(d) {
		if (tooltipDiv != null)
						tooltipDiv/*.transition()
					.duration(500)*/
					.style("opacity", 0);
					this.style.cursor = "auto";
				})
	.on("click", function(d) {
		var elem = d3.select(this);
		if (!SHIV.utils.keyboard.ctrl && !SHIV.utils.keyboard.shift) {
			var res = d3.select(this.parentNode).selectAll(".tile")
			.each(function(d) { 
				d.scanned = d.selected = false; 
				d3.select(this).attr("class","tile");
			}
			);
		}
		d.selected = !d.selected;
		var pNode = this.parentNode.parentNode;
		while (pNode.nodeName != "svg")
			pNode = pNode.parentNode;
		var sid = elem.attr("source-id");
		var puid = elem.attr("source-uid");
		if (d.selected)
			elem.attr("class","tile selected");
		else
			elem.attr("class","tile");
		if (d[3].length == 1)
			SHIV.graphManager.highlight(pNode,sid,d[3][0],d.selected);
		else if (d[3].length > 1)
			SHIV.graphManager.highlightAll(pNode,sid,d[3],d.selected);
	});
}

SHIV.graph.HeatMap.prototype.updateAxis = function() {

	if (this.xAxis != null) this.svg.select(".x.axis").call(this.xAxis);
	if (this.yAxis != null) this.svg.select(".y.axis").call(this.yAxis);
	if (this.xAxisMinor != null) this.svg.select(".x.axis.minor").call(this.xAxisMinor);
	if (this.yAxisMinor != null) this.svg.select(".y.axis.minor").call(this.yAxisMinor);
	if (this.xAxisGrid != null) this.svg.select(".x.grid").call(this.xAxisGrid);
	if (this.yAxisGrid != null) this.svg.select(".y.grid").call(this.yAxisGrid);

	if (this.params.zooming.zoomVar !== undefined) {
		var xScale = this.params.data[0].x.scale;
		var yScale = this.params.data[0].y.scale;
		this.params.zooming.zoomVar.x(xScale).y(yScale);
	}
}

SHIV.graph.HeatMap.prototype.clear = function() {
	this.svg.selectAll(".tile").remove();
}

SHIV.graph.HeatMap.prototype.reset = function() {
	// for the moment we reset the domain from the first series
	if (this.params.data[0].x.type != "ordinal") {
		var xdomain = this.params.data[0].x.domain;
		this.xScale.domain(xdomain);
	}
	if (this.params.data[0].y.type != "ordinal") {
		var ydomain = this.params.data[0].y.domain;
		this.yScale.domain(ydomain);
	}
	this.updateAxis();
	var zoom = this.params.zooming.zoomVar;
	if (d3.event == null) {
		d3.event = {}
	}
	if (this.params.type != "heatmap-table") {
		d3.event.sourceEvent = this.reset;
		d3.event.type = "zoom";
		d3.event.target = zoom;
		d3.event.scale = 1;
		d3.event.translate = [0,0];
		this.svg.call(zoom.event);
	}
	updateAxisX();
	updateAxisY();
	updateLegend();
	// todo: reset zoom
}

SHIV.graph.HeatMap.prototype.appendData = function(data,recenterDomain) {
	var start = Date.now();
	var totalPoints = 0;
	if (recenterDomain === undefined || recenterDomain == null)
		recenterDomain = true;

	for (var i = 0; i < data.length; i++) {
		var dataParams = data[i];
		var originalData = dataParams;
		var newDataset = dataParams.data;
		dataParams.data = null; // null it so that the next step doesn't overwrite it
		var originalDataset = this.params.data[i].data;
		dataParams = SHIV.utils.MergeRecursive(originalData, this.params.data[i]);
		for (var j = 0; j < newDataset.length; j++)
			originalDataset.push(newDataset[j]);
		dataParams.data = originalDataset;
		var opacity = dataParams.opacity;

		if (opacity === undefined || opacity == null)
			opacity = 1.0;
		else {
			var aux = parseFloat(opacity);
			if (isNaN(aux)) opacity = 1.0;
			else if (aux < 0) opacity = 0.0;
			else if (aux > 1.0) opacity = 1.0;
			else
				opacity = aux;
		}
		var fillColor = dataParams.color;
		if (fillColor === undefined)
			fillColor = null;
		dataParams.opacity = opacity;
		var sizeScale = dataParams.size.scale;
		var sizeFn = null;
		if (dataParams.size.idx != -1) {
			sizeFn = function(d) { return sizeScale(d[dataParams.size.idx]); };
		} else if (dataParams.size.functor !== undefined && dataParams.size.functor != null) {
			sizeFn = dataParams.size.functor;
		} else
			sizeFn = 3;
		var sourceUidFn = null;
		if (dataParams.source.idx != -1) {
			sourceUidFn = function(d) { return d[dataParams.source.idx]; }
			dataParams.source.functor = sourceUidFn;
			if (dataParams.keyFunction === undefined || dataParams.keyFunction == null)
				dataParams.keyFunction = sourceUidFn;
		} else if (dataParams.source.functor !== undefined && dataParams.source.functor != null) {
			sourceUidFn = dataParams.source.functor;
		}
		var dataset = newDataset;

		if (this.params.type != "heatmap-table") {
			// update xScale and yScale
			var updateOldData = false;
			var updateOldDomain = false;
			var dfunctor = dataParams.x.domainFunctor;
			if (dfunctor == null)
				dfunctor = function(d) { 
					return d[dataParams.x.idx]; 
				}
			// need to sort data
			dataParams.data.sort(function(a,b) { return a[dataParams.x.idx] - b[dataParams.x.idx]; });
			var newDomain = d3.extent(dataset,dfunctor);
			var oldDomain = dataParams.x.scale.domain();
			var oldDataDomain = dataParams.x.domain;
			if (oldDataDomain === undefined || oldDataDomain == null) oldDataDomain = [Number.NaN, Number.NaN];
			if (isNaN(oldDataDomain[0]) && isNaN(oldDataDomain[1])) {
				oldDataDomain = newDomain;
				updateOldDomain = true;
			}
			if (oldDomain[0] < newDomain[0]) {
				newDomain[0] = oldDomain[0];
				updateOldData = true;	
			}
			if (newDomain[0] < oldDataDomain[0]) { 
				oldDataDomain[0] = newDomain[0];
				updateOldDomain = true;
			}
			if (oldDomain[1] > newDomain[1]) {
				newDomain[1] = oldDomain[1];
				updateOldData = true;
			}
			if (newDomain[1] > oldDataDomain[1]) { 
				oldDataDomain[1] = newDomain[1];
				updateOldDomain = true;
			}
			if (recenterDomain && newDomain[0] !== undefined && newDomain[1] !== undefined)
				this.xScale.domain(newDomain);
			if (updateOldDomain) // only update domains if needed
				dataParams.x.domain = oldDataDomain;

			dfunctor = dataParams.y.domainFunctor;
			if (dfunctor == null)
				dfunctor = function(d) { 
					return d[dataParams.y.idx]; 
				}
			newDomain = d3.extent(dataset,dfunctor);
			oldDomain = dataParams.y.scale.domain();
			oldDataDomain = dataParams.y.domain;
			if (oldDataDomain === undefined || oldDataDomain == null) oldDataDomain = [Number.NaN, Number.NaN];
			if (isNaN(oldDataDomain[0]) && isNaN(oldDataDomain[1])) {
				oldDataDomain = newDomain;
				updateOldDomain = true;
			}
			if (oldDomain[0] < newDomain[0]) {
				newDomain[0] = oldDomain[0];
				updateOldData = true;	
			}
			if (newDomain[0] < oldDataDomain[0]) { 
				oldDataDomain[0] = newDomain[0];
				updateOldDomain = true;
			}
			if (oldDomain[1] > newDomain[1]) {
				newDomain[1] = oldDomain[1];
				updateOldData = true;
			}
			if (newDomain[1] > oldDataDomain[1]) { 
				oldDataDomain[1] = newDomain[1];
				updateOldDomain = true;
			}
			if (recenterDomain && newDomain[0] !== undefined && newDomain[1] !== undefined)
				this.yScale.domain(newDomain);
			if (updateOldDomain) // only update domains if needed
				dataParams.y.domain = oldDataDomain;

			this.gridFromData(dataset,dataParams.x.scale,dataParams.y.scale,dataParams);
		}
		else 
		{
			dataParams.x.scale.rangeRoundBands([0,this.params.width-(this.params.margin.left+this.params.margin.right)],0.01);
			dataParams.x.scale.domain(dataParams.x["rows-description"].map(function(d) { return d; }));

			dataParams.y.scale.rangeRoundBands([this.params.height-(this.params.margin.top+this.params.margin.bottom),0],0.01);
			dataParams.y.scale.domain(dataParams.y["cols-description"].map(function(d) { return d; }));

			dataParams.color.scale = SHIV.graph.createScale("linear");
			if (dataParams.color.range === undefined || dataParams.color.range == null) {
				dataParams.color.range == "custom";
				dataParams.color.scale.range([dataParams.color.min,dataParams.color.max]);
			}
			else {
				var card = dataParams.color.cardinality;
				if (card === undefined || card == null) {
					card = "8";
					dataParams.color.cardinality = card;
				}
				if (dataParams.color.range != "custom")
					dataParams.color.scale.range(colorbrewer[dataParams.color.range][card]);
				else
					dataParams.color.scale.range([dataParams.color.min,dataParams.color.max]);
			}
			var dfunctor = dataParams.color.domainFunctor;
			if (dfunctor == null)
				dfunctor = function(d) { 
					return d[dataParams.color.idx]; 
				}
			var newDomain = d3.extent(dataset,dfunctor);
			var oldDomain = dataParams.color.domain;
			if (oldDomain === undefined || oldDomain == null) oldDomain = [Number.NaN,Number.NaN];
			if (newDomain[0] != oldDomain[0] || newDomain[1] != oldDomain[1])
				dataParams.color.scale.domain(newDomain);
			else
				dataParams.color.scale.domain(oldDomain);
			dataParams.color.domain = dataParams.color.scale.domain();
		}

		this.params.data[i] = dataParams;
	}

	this.updateAxisX();
	this.updateAxisY();
	if (this.params.legend.enabled)
		this.updateLegend();
	this.updateData();
	
	//console.log("Points update took: "+(Date.now() - start)+" ms for "+totalPoints+" points");
}

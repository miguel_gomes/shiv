/**
 * SHIV Server representation.
 * Also includes the Credentials instance.
 *
 * @author Miguel Gomes (alka.setzer@gmail.com)
 * @since 0.1
 * @depends shiv.js
 * @depends shiv-enums.js
 */

console.log("loading shiv-classes-server.js...");

/**
 * Creates a new Credentials object.
 * @param username User name
 * @param password User password
 */
SHIV.Classes.Credentials = function(username,password) {
	this.username = username;
	this.password = password;
}
/** Object type */
SHIV.Classes.Credentials.prototype.ObjectType = "Credentials";

/**
 * Creates a new Server object.
 * @param hostname Server hostname
 * @param port Server port
 */
SHIV.Classes.Server = function(hostname,port) {
	this.hostname = hostname;
	if (port == null || port === undefined)
		port = 5555;
	port = parseInt(port,10);
	if (port == NaN)
		port = 5555;
	if (port < 1 || port > 65535)
		port = 5555;
	this.port =  port;
	this.credentials = null;
	this.connected = false;
	this.authenticated = false;
	this.state = SHIV.Enums.ServerState.UNKNWON;
	this.queuedCommands = [];
	this.token = null; // connection token
}
/** Object type */
SHIV.Classes.Server.prototype.ObjectType = "Server";
/**
 * Method that checks whether or not the server instance has credentials associated.
 * @return True if credentials are associated, false otherwise
 */
SHIV.Classes.Server.prototype.hasCredentials = function() { return this.credentials != null; };
/**
 * Method used to set credentials.
 * @param credentials Either a SHIV.Classes.Credentials object or a string indicating the username
 * @param password If the credentials argument is a String this will be the password (otherwise is ignored)
 */
SHIV.Classes.Server.prototype.setCredentials = function(credentials, password) {
	if (typeof credentials == "SHIV.Classes.Credentials") {
		this.credentials = credentials;
	} else if (typeof credentials == "string" || typeof credentials == "String") {
		if (this.credentials == null || this.credentials === undefined)
			this.credentials = new SHIV.Classes.Credentials(credentials,password);
		else {
			this.credentials.username = credentials;
			this.credentials.password = password;
		}
	} else
		alert(typeof credentials);
};
/**
 * Method used to test if the server is local to the client.
 * @return True if the server is local, false otherwise
 */
SHIV.Classes.Server.prototype.isLocal = function() {
	return (this.hostname == "localhost" || this.hostname=="127.0.0.1" || this.hostname=="::1");
};
/**
 * Method used to thest if the client is connected to the client.
 * @return True if the client is connected to the server, false otherwise
 */
SHIV.Classes.Server.prototype.isConnected = function() { return connected; };

/**
 * Method used to issue a Ping request.
 * @param callback Callback(rtt,jqXHR[,errorThrown]) to execute on success or failure
 */
SHIV.Classes.Server.prototype.Ping = function(callback) {
	console.log("Pinging server...");
	var request = $.ajax({
		url: "http://"+this.hostname+":"+this.port+"/ping?ofmt=json&msg-id="+Date.now(),
		cache: false,
		context: {'server':this,'callback':callback,'sent':Date.now()},
		dataType:"json",
		success: function(data, textStatus, jqXHR) {
			if (this.callback !== undefined && this.callback != null)
				this.callback(Date.now() - parseInt(this.sent),jqXHR);
		},
		error: function(jqXHR, textStatus, errorThrown ) {
			if (this.callback !== undefined && this.callback != null)
				this.callback(-1,jqXHR,errorThrown);
		}
	});
}
/**
 * Method used to connect to the server.
 * Note 1: A ping request will be issued before to test the connection.
 * Note 2: On ping success a client-ident request will be issued
 * Note 3: On client-ident request success a login request will be issued
 * @param callback Callback(jqXHR,data|errorThrown) to execute on all steps successful
 */
SHIV.Classes.Server.prototype.Connect = function(callback) {
	console.log("Connecting to "+this.hostname+":"+this.port+(this.credentials != null ? " using the following credentials "+this.credentials.username : ""));
	this.state = SHIV.Enums.ServerState.CONNECTING;
	// first ping and then connect on the callback
	this.Ping(function(rtt,jqXHR,errorThrown) {
		if (rtt > 0) {
			console.log("Connected to the server");
			this.server.connected = true;
			this.server.state = SHIV.Enums.ServerState.CONNECTED;
			this.server.doIdent(function(jqXHR,data) {
				if (jqXHR.status == 200) { // yei, go for login
					this.server.doLogin(function(jqXHR,data) {
						// TODO complete
						if (callback !== undefined && callback != null)
							callback(jqXHR,data);
						// now execute queued commands
						if (SHIV.server.queuedCommands.length > 0) {
							while (SHIV.server.queuedCommands.length > 0) {
								var cmd = SHIV.server.queuedCommands.pop();
								SHIV.server.executeCommand(cmd['command'],cmd['callback']);
							}
						}
					});
					// create a timeout to keep connection alinve
					this.server.keepAliveId = window.setInterval(function() {
						SHIV.server.executeCommand("ping",function(jqXHR,data) {
							if (jqXHR.status != 200) {
								this.server.state = SHIV.Enums.ServerState.OFFLINE;
								SHIV.showMessage("error","Could not keep connection with server.<br />Error: "+jqXHR.status+" - "+jqXHR.statusText+"<br />"+errorThrown);// i8n
								window.clearInterval(this.server.keepAliveId); // clear timeout
								this.server.connected = false;
							}

						});
					},60000); // keep alive every 60 seconds
				} else if (jqXHR.status == 0) { // connection error
					this.server.state = SHIV.Enums.ServerState.OFFLINE;
					SHIV.showMessage("error","Could not identify with server.<br />Error: "+jqXHR.status+" - "+jqXHR.statusText+"<br />"+errorThrown);// i8n
				} else { // any other issue
					this.server.state = SHIV.Enums.ServerState.OFFLINE;
					SHIV.showMessage("error","Could not identify with server.<br />Error: "+jqXHR.status+" - "+jqXHR.statusText);// i8n
				}
			});
		} else {
			this.server.connected = false;
			this.server.state = SHIV.Enums.ServerState.OFFLINE;
			SHIV.showMessage("error","Could not connect to server.<br />Error: "+jqXHR.status+" - "+jqXHR.statusText+"<br />"+errorThrown,null,null,null,SHIV.showConnectDialog);// i8n
		}
	});
};

/**
 * Disconnects from the server.
 * @param callback Callback(jqXHR,data|errorThrown) to execute on success or failure
 */
SHIV.Classes.Server.prototype.Disconnect = function(callback) {
	if (!this.connected) return;
	console.log("Disconnecting from remote server...");
	SHIV.server.connected = false;
	SHIV.server.state = SHIV.Enums.ServerState.NOT_CONNECTED;
	window.clearInterval(SHIV.server.keepAliveId);
	var request = $.ajax({
		url: "http://"+this.hostname+":"+this.port+"/client-disconnect&omit-reply=true",
		cache: false,
		context: {'server':this,'callback':callback,'sent':Date.now()},
		dataType:"json",
		success: function(data, textStatus, jqXHR) {
			if (this.callback !== undefined && this.callback != null)
				this.callback(jqXHR,data);
		},
		error: function(jqXHR, textStatus, errorThrown ) {
			if (this.callback !== undefined && this.callback != null)
				this.callback(jqXHR,errorThrown);
		}
	});
}

/**
 * Identifies this client with the server.
 * @param callback Callback(jqXHR,data|errorThrown) to execute on success or failure
 */
SHIV.Classes.Server.prototype.doIdent = function(callback) {
	if (!this.connected) return;
	console.log("Identifying with remote server...");
	var request = $.ajax({
		url: "http://"+this.hostname+":"+this.port+"/client-ident?ofmt=json&name=SHIV&api-level=7&version=0.2&msg-id="+Date.now(),
		cache: false,
		context: {'server':this,'callback':callback,'sent':Date.now()},
		dataType:"json",
		success:  function(data, textStatus, jqXHR) {
			console.log("Identified with remote server. Result: "+data['status-code']+" - "+data['status-msg']);
			if (data['token'] !== undefined && data['token'] != null)
				this.server.token = data['token'];
			if (this.callback !== undefined && this.callback != null)
				this.callback(jqXHR,data);
		},
		error: function(jqXHR, textStatus, errorThrown ) {
			if (this.callback !== undefined && this.callback != null)
				this.callback(jqXHR,errorThrown);
		}
	});
};

/**
 * Executes a login request on the server.
 * @param callback Callback(jqXHR,data|errorThrown) to execute on success or failure
 */
SHIV.Classes.Server.prototype.doLogin = function(callback) {
	if (!this.connected) return; // nothing to do
	console.log("Issuing login request...");
	if ((this.hostname == "localhost" || this.hostname=="127.0.0.1" || this.hostname=="::1") && 
		(this.credentials == null || this.credentials.username == null || this.credentials.username.trim().length == 0)) {
		this.authenticated = true;
		console.log("User is automaticaly logged in.");
		if (callback !== undefined && callback != null)
			callback({'status':200},{'status-code':200,'status-msg':"OK","ifmt":"HTTP","cmd-id":"login"});
		return; // no need to login for localhost with as the server is very happy about that
	}
	var username = "";
	var password = "";
	if (this.credentials != null && this.credentials !== undefined) {
		username = this.credentials.username;
		password = this.credentials.password;
	}
	var url = "http://"+this.hostname+":"+this.port+"/login?ofmt=json&username="+username+"&password="+password;
	if (this.token != null)
		url += "&token="+this.token;
	var request = $.ajax({
		url: url,
		cache: false,
		context: {'server':this,'callback':callback,'sent':Date.now()},
		dataType: "json",
		success: function(data, textStatus, jqXHR) {
			this.server.authenticated = (data['status-code'] == 200);
			if (this.server.authenticated)
				console.log("User is logged in. Result: "+data['status-code']+" - "+data['status-msg']);
			else
				console.log("Login failed");
			if (this.callback !== undefined && this.callback != null)
				this.callback(jqXHR,data);
			else
				SHIV.showMessage('error','Login failed, cause: '+data['status-msg']); // i8n
		},
		error : function(jqXHR, textStatus, errorThrown ) {
			if (this.callback !== undefined && this.callback != null)
				this.callback(jqXHR,errorThrown);
			else
				SHIV.showMessage('error','Login failed'); // i8n
		}
	});
}

/**
 * Executes a logout request on the server.
 * @param callback Callback(jqXHR,data|errorThrown) to execute on success or failure
 */
SHIV.Classes.Server.prototype.doLogout = function(callback) {
	if (!this.connected) return; // nothing to do
	if (!this.authenticated) return; // again, nothing to do
	console.log("Issuing logout request...");
	if ((this.hostname == "localhost" || this.hostname=="127.0.0.1" || this.hostname=="::1") && 
		(this.credentials == null || this.credentials.username == null || this.credentials.username.trim().length == 0)) {
		this.authenticated = true;
		console.log("User is logged out.");
		return; // no need to logout for localhost with as the server is very happy about that
	}
	var url = "http://"+this.hostname+":"+this.port+"/logout?ofmt=json";
	if (this.token != null)
		url += "&token="+this.token;
	var request = $.ajax({
		url: url,
		cache: false,
		context: {'server':this,'callback':callback,'sent':Date.now()},
		dataType: "json",
		success: function(data, textStatus, jqXHR) {
			this.server.authenticated = !(data['status-code'] == 200);
			if (this.server.authenticated)
				console.log("User is logged out.");
			else
				console.log("Logout failed");
			if (this.callback !== undefined && this.callback != null)
				this.callback(jqXHR,data);
			else
				SHIV.showMessage('error','Logout failed, cause: '+data['status-msg']); // i8n
		},
		error: function(jqXHR, textStatus, errorThrown ) {
			if (this.callback !== undefined && this.callback != null)
				this.callback(jqXHR,errorThrown);
			else
				SHIV.showMessage('error','Logout failed'); // i8n
		}
	});
}

/**
 * Constructs a full URL from a given command to execute.
 * @param command Command to execute (without http://hostname:port/)
 * @return URL
 */
SHIV.Classes.Server.prototype.getURLForCommand = function(command) {
	if (command == null || command === undefined || command.trim().length == 0) return; // nothing to do
	var queryIndex = command.indexOf('?');
	// make sure we have the msg-id for tracking
	if (command.indexOf('msg-id') < 0) {
		command += (queryIndex > 0 ? '&' : '?');
		command += "msg-id="+Date.now();
	}
	// and that we have an json ofmt
	queryIndex = command.indexOf('?');
	if (command.indexOf('ofmt') < 0) {
		command += (queryIndex > 0 ? '&' : '?');
		command += "ofmt=json";
	}
	queryIndex = command.indexOf('?');
	if (this.token != null && command.indexOf('token=') < 0) {
		command += (queryIndex > 0 ? '&' : '?');
		command += "token="+this.token;
	}
	return "http://"+this.hostname+":"+this.port+"/"+command;
}

/**
 * Executes a command on the server.
 * @param command Command to execute (without http://hostname:port/)
 * @param callback Callback(jqXHR,data|errorThrown) to execute on success or failure
 */
SHIV.Classes.Server.prototype.executeCommand = function(command,callback) {
	if (!this.connected) {
		this.queuedCommands.push({'command':command,'callback':callback});
		return; // not connected
	}
	if (command == null || command === undefined || command.trim().length == 0) return; // nothing to do
	var url = this.getURLForCommand(command);
	var request = $.ajax({
		url: url,
		cache: false,
		context: {'server':this,'callback':callback,'sent':Date.now()},
		dataType: "json",
		success : function(data, textStatus, jqXHR) {
			if (this.callback !== undefined && this.callback != null)
				this.callback(jqXHR,data);
		},
		error : function(jqXHR, textStatus, errorThrown ) {
			SHIV.showMessage('error','Command failed, cause: '+errorThrown); // i8n
			if (errorThrown != null)
					console.log(errorThrown.stack);
			if (this.callback !== undefined && this.callback != null)
				this.callback(jqXHR,errorThrown);
		}
	});
}

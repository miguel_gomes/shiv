/**
 * SHIV graphing methods.
 *
 * @author Miguel Gomes (alka.setzer@gmail.com)
 * @depends d3.js (>= 3.5)
 * @depends shiv-graphing.js (>= 0.1)
 * @since 0.1
 */

console.log("loading shiv-graphing-scatterplot.js...");

/**
 * Creates a scatter plot.
 * @param data Data to render (array of arrays, where each row is a new data point)
 * @param params Plot parameters
 * @return Scatter plot
 */
SHIV.graph.ScatterPlot = function(params) {
	var chart = this.chart = this; // recursive link
	if (params === undefined || params == null)
		params = SHIV.graph.defaultParams();
	else {
		params = SHIV.graph.mergeParams(params);
	}
	this.params = params;
	params.chart = this;
	SHIV.graphManager.addGraph(params);
	var xScale = this.xScale = null;
	var yScale = this.yScale = null;
	var grid = null;
	var paramsMap = {}; // parameters map

	// this will create all the necessary scales
	this.updateAxisType(paramsMap);

	// then we need to create the graph
	var svg = this.svg = d3.select(params.bindto == null ? 'body' : params.bindto).append("svg")
		.attr("width", params.width)
		.attr("height", params.height)
		.append("g")
		.attr("transform", "translate(" + params.margin.left + "," + params.margin.top + ")");
	var svgElem = this.svgElem = d3.select(params.bindto == null ? 'body' : params.bindto).select('svg');
	this.params['svg-elem'] = this.svgElem[0][0];

	var clipPath = this.svg.append("svg:clipPath")
						.attr("id","clip")
						.append("svg:rect")
							.attr("id","clip-rect")
							.attr("x",0)
							.attr("y",0)
							.attr("width",params.width-(params.margin.left+params.margin.right))
							.attr("height",params.height-(params.margin.top+params.margin.bottom));

	var auxArea = this.svg.append("g")
					.attr("id","aux-area");
	this.auxArea = this.params["auxArea"] = auxArea;

	// create zoom area
	var zoomArea = svg.append("g")
			.attr("class","zoomarea")
			.append("rect")
				.attr("x", "0")
				.attr("y", "0")
				.attr("width", params.width-(params.margin.right+params.margin.left))
				.attr("height", params.height-(params.margin.top+params.margin.bottom))
				.style("opacity","0");
	this.zoomArea = params.zooming.elem = zoomArea;

	var chartBody = this.svg.append("g")
				.attr("clip-path", "url(#clip)");
	this.params['chartbody'] = chartBody;
	this.chartBody = chartBody;

	// tooltip
	var tooltipDiv = null;
	var tooltipFormat = null;
	if (params.tooltip.enabled) {
		this.tooltipDiv = tooltipDiv = d3.select("body").append("div") 
					.attr("class", "tooltip")
					.attr("id", "tooltip-"+(params.bindto == null ? 'body' : params.bindto))
					.style("opacity", 0);
		params.tooltip.elem = tooltipDiv;
		if (params.tooltip.format !== undefined && params.tooltip.format != null)
			tooltipFormat = params.tooltip.format;
		else
			tooltipFormat = d3.format(".3f");
		this.tooltipFormat = tooltipFormat;
	}

	// now bind some usefull stuf
	this.params.svg = svg;
	// then we add the axis

//	this.updateAxis();
	this.updateAxisX();
	this.updateAxisY();

	var xAxis = this.xAxis;
	var xAxisMinor = this.xAxisMinor;
	var xAxisGrid = this.xAxisGrid;
	var yAxis = this.yAxis;
	var yAxisMinor = this.yAxisMinor;
	var yAxisGrid = this.yAxisGrid;

	// attach brush
	if (params.brushing.enabled)
		this.attachBrush();

	// attach zoom
	if (params.zooming.enabled)
		this.attachZoom();

	this.updateData();
}

SHIV.graph.ScatterPlot.prototype.ObjectType = "ScatterPlot";
SHIV.graph.ScatterPlot.prototype.ObjectClassName = "point";
SHIV.graph.ScatterPlot.prototype.Dimensions = 2;
SHIV.graph.ScatterPlot.prototype.SupportsSize = true;
SHIV.graph.ScatterPlot.prototype.SupportsColor = true;
SHIV.graph.ScatterPlot.prototype.SupportsShape = true;
SHIV.graph.ScatterPlot.prototype.SupportsRegression = true;

SHIV.graph.ScatterPlot.prototype.updateAxisType = function(paramsMap) {
	var params = this.params;
	var xInverted = params.axis.x.inverted;
	var yInverted = params.axis.y.inverted;
	if (params.data !== undefined && params.data != null) {
		for (var i = 0; i < params.data.length; i++) {
			var dataset = params.data[i].data;
			var data = params.data[i];

			if (paramsMap !== undefined && paramsMap != null && data['source-id'] != null)
				paramsMap[data['source-id']] = data;

			// first we need to create the scales
			if (data.x.scale == null) 
				xScale = SHIV.graph.createScale(data.x.type);
			else {
				var _xScale = SHIV.graph.createScale(data.x.type);
				if (""+_xScale != ""+xScale)
					xScale = _xScale;
				else
					xScale = data.x.scale;
			}
			if (data.y.scale == null) 
				yScale = SHIV.graph.createScale(data.y.type);
			else {
				var _yScale = SHIV.graph.createScale(data.y.type);
				if (""+_yScale != ""+yScale)
					yScale = _yScale;
				else
					yScale = data.y.scale;
			}
			// then figure out the domains
			if (data.x.domain == null) {
				var dfunctor = data.x.domainFunctor;
				if (dfunctor == null)
					dfunctor = function(d) { 
						return (xInverted ? -1.0 : 1.0) * d[data.x.idx]; 
					}
				xScale.domain(d3.extent(dataset,dfunctor));
				data.x.domain = xScale.domain();
			}
			else 
			{
				var dfunctor = data.x.domainFunctor;
				if (dfunctor == null)
					dfunctor = function(d) { 
						return (xInverted ? -1.0 : 1.0) * d[data.x.idx]; 
					}
				var newDomain = d3.extent(dataset,dfunctor);
				var oldDomain = data.x.domain;
				if (newDomain[0] != oldDomain[0] || newDomain[1] != oldDomain[1])
					xScale.domain(newDomain);
				else
					xScale.domain(oldDomain);
			}
			if (data.y.domain == null) {
				var dfunctor = data.y.domainFunctor;
				if (dfunctor == null)
					dfunctor = function(d) { 
						return (yInverted ? -1.0 : 1.0) * d[data.y.idx]; 
					}
				yScale.domain(d3.extent(dataset,dfunctor));
				data.y.domain = yScale.domain();
			}
			else
			{
				var dfunctor = data.y.domainFunctor;
				if (dfunctor == null)
					dfunctor = function(d) { 
						return (yInverted ? -1.0 : 1.0) * d[data.y.idx]; 
					}
				var newDomain = d3.extent(dataset,dfunctor);
				var oldDomain = data.y.domain;
				if (newDomain[0] != oldDomain[0] || newDomain[1] != oldDomain[1])
					yScale.domain(newDomain);
				else
					yScale.domain(oldDomain);
			}
			// and set the scale ranges
			xScale.range([0, params.width - (params.margin.left + params.margin.right)]);
			yScale.range([params.height - (params.margin.top + params.margin.bottom),0]);
			this.xScale = data.x.scale = xScale;
			this.yScale = data.y.scale = yScale;

			var sizeScale = data.size.scale;
			if (data.size.idx != -1) {
				data.size.functor = function(d) { return sizeScale(d[data.size.idx]); };
			} else if (data.size.functor !== undefined && data.size.functor != null) {
				// no op
			} else
				data.size.functor = 3;
			var sizeFn = data.size.functor;
			if (data.source.idx != -1) {
				data.source.functor = function(d) { return d[data.source.idx]; }
				if (data.keyFunction === undefined || data.keyFunction == null)
					data.keyFunction = data.source.functor;
			} else if (data.source.functor !== undefined && data.source.functor != null) {
				// no op
			}

			// color / alpha
			var opacity = data.opacity;
			if (opacity === undefined || opacity == null)
				opacity = 1.0;
			else {
				var aux = parseFloat(opacity);
				if (isNaN(aux)) opacity = 1.0;
				else if (aux < 0) opacity = 0.0;
				else if (aux > 1.0) opacity = 1.0;
				else
					opacity = aux;
			}
			var fillColor = data.color;
			if (fillColor === undefined)
				fillColor = null;
			data.opacity = opacity;

			if (dataset.length > this.params.reductionPts)
				// above this limit stuff d3 svg gets to be crappy so we create a sparse grid from the data
				grid = this.gridFromData(dataset,xScale,yScale,data);
			else // never the less create the quadtree
				data.quadtree = this.quadtreeFromData(dataset,xScale,yScale,data);
		}
	}
}

SHIV.graph.ScatterPlot.prototype.updateData = function() {
	// first remove all previous points
	this.points = points = this.chartBody.selectAll(".point").remove();
	this.points = points = this.chartBody.selectAll(".point");
	// now, data
	var xInverted = this.params.axis.x.inverted;
	var yInverted = this.params.axis.y.inverted;
	var start = Date.now();
	var totalPoints = 0;
	for (var i = 0; i < this.params.data.length; i++) {
		var dataParams = this.params.data[i];
		var dataset = dataParams.data;
		if (dataParams.grid != null) {
			totalPoints+= dataParams.grid.data.length;
			var sizeFn = 5;//dataParams.size.functor;
			var xFunc = function(d) { return (xInverted ? -1.0 : 1.0) * d[0]; };
			var yFunc = function(d) { return (yInverted ? -1.0 : 1.0) * d[1]; };
			var opacity = function(d) { return dataParams.grid.scale(d[2]); };
			if (dataParams.shape == "circle")
				sizeFn = 2.5;
			var fillColor = dataParams.color;
			var cp = Date.now();
			this.points = points = this.createPoints(dataParams,points,dataParams.grid.data,null,sizeFn,xFunc,yFunc,null,opacity,fillColor);
			//console.log("Create[A] Create points took: "+(Date.now() - cp)+" ms");
			this.createPointsEventHandlers(dataParams, points, this.params);
		} else {
			totalPoints+= dataset.length;
			var sizeFn = dataParams.size.functor;
			var opacity = dataParams.opacity;
			var sizeScale = dataParams.size.scale;
			var sourceUidFn = dataParams.source.functor;
			var fillColor = dataParams.color;
			var xFunc = function(d) { return dataParams.x.scale((xInverted ? -1.0 : 1.0) * d[dataParams.x.idx]); };
			var yFunc = function(d) { 
				return dataParams.y.scale((yInverted ? -1.0 : 1.0) * d[dataParams.y.idx]); 
			};
			var cp = Date.now();
			this.points = points = this.createPoints(dataParams,points,dataset,dataParams.keyFunction,sizeFn,xFunc,yFunc,sourceUidFn,opacity,fillColor);
			//console.log("Create[B] Create points took: "+(Date.now() - cp)+" ms");
			this.createPointsEventHandlers(dataParams, points, this.params);
		}
	}
	this.points = points = this.chartBody.selectAll(".point");

	// see if we need to execute data regression
	SHIV.graph.executeRegression(this);
	//console.log("Points creation took: "+(Date.now() - start)+" ms for "+totalPoints+" points");
}

SHIV.graph.ScatterPlot.prototype.updateAxisX = function() {
	SHIV.graph.updateAxisX(this);
}
SHIV.graph.ScatterPlot.prototype.updateAxisY = function() {
	SHIV.graph.updateAxisY(this);
}

SHIV.graph.ScatterPlot.prototype.resizeViewport = function(width,height,maintainDomains) {
	SHIV.graph.resizeViewport(this,width,height,maintainDomains);
}

SHIV.graph.ScatterPlot.prototype.detachBrush = function() {
	SHIV.graph.detachBrush(this);
}

SHIV.graph.ScatterPlot.prototype.attachBrush = function(params) {
	if (params === undefined || params == null) params = this.params;

	// create brush area
	var brushArea = this.svg.append("g")
				.attr("class", "brush");
	this.brushArea = params.brushing.elem = brushArea;

	var chart = this;

	var brush = d3.svg.brush()
		.x(d3.scale.identity().domain([0, params.width - (params.margin.left+params.margin.right)]))
		.y(d3.scale.identity().domain([0, params.height - (params.margin.top+params.margin.bottom)]))
		.on("brushstart", params.brushing.events.brushstart != null ? params.brushing.events.brushstart : null)
		.on("brush", params.brushing.events.brush != null ? params.brushing.events.brush : function() { 
			// find extents by brush
			var extent = d3.select(this).select(".extent");
			var width = parseInt(extent.attr('width'));
			var height = parseInt(extent.attr('height'));
			var ex = parseInt(extent.attr("x"));
			var ey = parseInt(extent.attr("y"));
			var addPoints = SHIV.utils.keyboard.ctrl;
			var removePoints = SHIV.utils.keyboard.shift;
			var selectedColor = "#FF0000";
			var curColor  = "#4682B4"
			var test = false;
			var elem = this;
			while (elem != null && elem.nodeName.toLowerCase() != "svg") {
				elem = elem.parentNode;
			}
			var params = SHIV.graphManager.getParamsBySVG(elem);
			if (params != null) {
				var d0 = params.data[0].color;
				if (d0 != null && d0["selected-color"] !== undefined && d0["selected-color"] != null) {
					selectedColor = d0["selected-color"];
					test = selectedColor != "#FF0000";
					curColor = d0['functor'];
				}
			}
			if (!addPoints && !removePoints) {
				d3.select(this.parentNode.parentNode).selectAll(".point").attr()
				.each(function(d) { 
					d.scanned = d.selected = false; 
					d3.select(this).attr("class","point").style("fill",curColor);
				});
			}
			if (width == 0 && height == 0) return; // nothing to do
			if (!removePoints) addPoints = true;
			d3.select(this.parentNode.parentNode)
			.selectAll(".point")
				.each(function(d,i) { //
					var dp = d3.select(this);
					var dx, dy;
					if (this.nodeName == "circle") {
						dx = parseInt(dp.attr('cx'));
						dy = parseInt(dp.attr('cy'));
					} else {
						dx = parseInt(dp.attr('x'));
						dy = parseInt(dp.attr('y'));
					}
					var inside = !( dx < Math.max(ex,0) || dx > ex + width ||
						dy < Math.max(ey,0) || dy > ey + height);
					if (!inside) return;
					if (addPoints) {
						dp.attr("class","point selected");
						if (test) dp.style("fill",selectedColor);
						else dp.style("fill",curColor);
					}
					else if (removePoints && inside) {
						dp.attr("class","point");
						if(test) dp.style("fill",curColor);
					}
					//console.log(this);
					return this;
				});
			})
		.on("brushend",params.brushing.events.brushend != null ? params.brushing.events.brushend : function() {
			// find extents by brush
			var extent = d3.select(this).select(".extent");
			var width = parseInt(extent.attr('width'));
			var height = parseInt(extent.attr('height'));
			var ex = parseInt(extent.attr("x"));
			var ey = parseInt(extent.attr("y"));
			if (width == 0 && height == 0) return; // nothing to do
			var sels = {};
			var addPoints = SHIV.utils.keyboard.ctrl;
			var removePoints = SHIV.utils.keyboard.shift;
			d3.select(this.parentNode.parentNode)
			.selectAll(".point.selected")
			.each(function(d) {
				var elem = d3.select(this);
				var dx, dy;
				var sourceId = elem.attr("source-id");
				var uid = elem.attr("source-uid");
				var vals; 
				if ((vals = sels[sourceId]) == null)
					vals = sels[sourceId] = [];
				if (uid == null && d[3] !== undefined) {
					for (var jj = 0; jj < d[3].length; jj++)
						vals.push(d[3][jj]);
				} else
					vals.push(uid);
			});
			for (var p in sels) {
				var vals = sels[p];
				if (vals === undefined || vals == null) continue;
				SHIV.graphManager.highlightAll(this.parentNode.parentNode,p,vals,!removePoints);
			}
		});
	params.brushing.enabled = true;
	params.brushing.brush = brush;
	params.brushing.elem.call(brush);
}

SHIV.graph.ScatterPlot.prototype.zoomEvent = function(howMuch,evtSource) {
	if (howMuch === undefined || howMuch == null) return;
	if (!this.params.zooming.enabled) return;
	var zoom = this.params.zooming.zoomVar;

	//this.svg.call(zoom.event); // https://github.com/mbostock/d3/issues/2387

	// Record the coordinates (in data space) of the center (in screen space).
	var center0 = zoom.center();
	if (center0 == null) {
		// need to set the center
		var xScale = zoom.x();
		var yScale = zoom.y();
		var xDomain = xScale.range();
		var yDomain = yScale.range();
		var x = (xDomain[1]+xDomain[0])*0.5;
		var y = (yDomain[0]+yDomain[1])*0.5;
		center0 = [x,y];
	}
	var translate0 = zoom.translate();
	var scale = zoom.scale();
	var coordinates0 = [(center0[0] - translate0[0]) / scale, (center0[1] - translate0[1]) / scale];
	zoom.scale(zoom.scale() * Math.pow(2, +howMuch));
	
	// Translate back to the center
	var translate = zoom.translate();
	scale = zoom.scale();
	var center1 = [coordinates0[0] * scale + translate[0], coordinates0[1] * scale + translate[1]];
	zoom.translate([translate0[0] + center0[0] - center1[0], translate0[1] + center0[1] - center1[1]]);
	translate = zoom.translate();

	this.updateAxis();

	// no point in doing transitions because we clear the points
	//zoom.event.scale =zoom.scale();
	//zoom.event.translate = zoom.translate();
	if (d3.event == null) {
		d3.event = {}
	}
	d3.event.sourceEvent = evtSource;
	d3.event.type = "zoom";
	d3.event.target = zoom;
	d3.event.scale = scale;
	d3.event.translate = translate;
	this.svg.call(zoom.event);
}

SHIV.graph.ScatterPlot.prototype.zoomIn = function(evt,howMuch) {
	if (howMuch === undefined || howMuch == null) howMuch = "+1";
	this.zoomEvent(howMuch,evt);
}

SHIV.graph.ScatterPlot.prototype.zoomOut = function(evt,howMuch) {
	if (howMuch === undefined || howMuch == null) howMuch = "-1";
	this.zoomEvent(howMuch,evt);
}

SHIV.graph.ScatterPlot.prototype.detachZoom = function() {
	this.params.zooming.enabled = false;
	this.params.zooming.elem.on(".zoom",null);
}

SHIV.graph.ScatterPlot.prototype.attachZoom = function() {
	var params = this.params;
	params.zooming.enabled = true;

	var attachToPoints = true;
	var svg = params.svg;
	var chartBody = params.chartbody;
	// TODO: support multiple axis and scales
	var xAxis = params.axis.x.xAxis !== undefined ? params.axis.x.xAxis : this.xAxis;
	var yAxis = params.axis.y.yAxis !== undefined ? params.axis.y.yAxis : this.yAxis;
	var xAxisMinor = params.axis.x.xAxisMinor !== undefined ? params.axis.x.xAxisMinor : this.xAxisMinor;
	var yAxisMinor = params.axis.y.yAxisMinor !== undefined ? params.axis.y.yAxisMinor : this.yAxisMinor;
	var xAxisGrid = params.axis.grid.xAxisGrid !== undefined ? params.axis.grid.xAxisGrid : this.xAxisGrid;
	var yAxisGrid = params.axis.grid.yAxisGrid !== undefined ? params.axis.grid.yAxisGrid : this.yAxisGrid;
	var xScale = params.data[0].x.scale !== undefined ? params.data[0].x.scale : this.xScale;
	var yScale = params.data[0].y.scale !== undefined ? params.data[0].y.scale : this.yScale;
	var chart = this;
	var zoom = d3.behavior.zoom(svg).x(xScale).y(yScale).size([xScale.range()[1],yScale.range()[0]]);
	params.zooming.zoomVar = zoom;
	params.zooming.lastScale = zoom.scale();
		
	if (params.zooming.events.zoomstart != null)
		zoom.on("zoomstart", params.zooming.events.zoomstart);
	if (params.zooming.events.zoom != null)
		zoom.on("zoom", params.zooming.events.zoom);
	else
		zoom.on("zoom", function() {
			var evt = d3.event;
			if (Object.prototype.toString.call(evt.sourceEvent) == "[object Object]")
				evt = evt.sourceEvent; // bubbling
/*
			if (xAxis != null) svg.select(".x.axis").call(xAxis);
			if (yAxis != null) svg.select(".y.axis").call(yAxis);
			if (xAxisMinor != null) svg.select(".x.axis.minor").call(xAxisMinor);
			if (yAxisMinor != null) svg.select(".y.axis.minor").call(yAxisMinor);
			if (xAxisGrid != null) svg.select(".x.grid").call(xAxisGrid);
			if (yAxisGrid != null) svg.select(".y.grid").call(yAxisGrid);
*/
			console.log("evt.scale: "+evt.scale+", evt.translate: "+evt.translate);
			//console.log("params.zooming.lastScale: "+chart.params.zooming.lastScale+", current scale: "+zoom.scale());

			// todo: apply a different behavior when panning
			/*if (chart.params.zooming.lastScale == evt.scale) { // panning

			} else*/ { // zoom
				// remove all points and resolve using quadtree
				var params = chart.params;
				params.zooming.lastScale = evt.scale;
				// grab selected points
				var selectedPoints = [];
				chartBody.selectAll(".point.selected").each(function(d) {
					var elem = d3.select(this);
					var puid = elem.attr("source-uid");
					if (puid == null) {
						var uids = d[3];
						for (var ii = 0; ii < uids.length; ii++)
							selectedPoints.push(uids[ii]);
					} else {
						selectedPoints.push(parseInt(puid));
					}
				});
				// no need to sort, as the highlight functions handle that
				//selectedPoints = selectedPoints.sort(function(a,b) { return a-b; });
				// continue with the zoom operation
				chartBody.selectAll(".point").remove();
				for (var i = 0; i < params.data.length; i++) {
					var pts = [];
					var dataParams = params.data[i];
					var xScale = dataParams.x.scale;
					var yScale = dataParams.y.scale;
					var xRange = xScale.range();
					var yRange = yScale.range();
					// quadtree domain
					var xDomain = xScale.domain();
					var yDomain = yScale.domain();
					var x0 = xDomain[0];
					var y0 = yDomain[0];
					var x3 = xDomain[1];
					var y3 = yDomain[1];

//					console.log("xScale.domain() = ["+xDomain[0]+","+xDomain[1]+"], yScale.domain(): ["+yDomain[0]+","+yDomain[1]+"]")
					if (dataParams.quadtree != null) {
						//console.log("processing quadtree("+dataParams.quadtree.lastMod+")...");
						pts = dataParams.quadtree.walk([[x0,y0],[x3,y3]]);
					} else {
						// console.log("processing data points...");
						pts = dataParams.data;
					}
					if (pts.length > params.reductionPts) {
						//console.log("Creating new grid (replacing quadtree: "+(dataParams.quadtree == null)+")...");
						// create a new grid
						var grid = chart.gridFromData(pts,xScale,yScale,dataParams,dataParams.quadtree != null,true);
						// then show points
						var selection = chartBody.selectAll(".point");
						var sizeFn = 5;//dataParams.size.functor;
						var xFunc = function(d) { return d[0]; };
						var yFunc = function(d) { return d[1]; };
						var opacity = function(d) { return grid.scale(d[2]); };
						if (dataParams.shape == "circle")
							sizeFn = dataParams.size.functor / 2.0;
						var fillColor = dataParams.color;
						var cp = Date.now();
						selection = chart.createPoints(dataParams,selection,dataParams.grid.data,null,sizeFn,xFunc,yFunc,null,opacity,fillColor);
						var t = (Date.now() - cp);
						if (t > 1000) // 1s threshold
							console.log("Create points took: "+t+" ms for "+pts.length+" points");
						chart.createPointsEventHandlers(dataParams,selection);
						// actions
						var func = params.zooming.actions['high-data'];
						if (func !== undefined && func != null)
							func(params,dataParams,pts.length); // execute high-data function
					} else if (pts.length > 0 ) {
						//console.log("Showing existing points...");
						// show points
						var selection = chartBody.selectAll(".point").remove();
						selection = chartBody.selectAll(".point");
						var sizeFn = dataParams.size.functor;
						var opacity = dataParams.opacity;
						var sizeScale = dataParams.size.scale;
						var fillColor = dataParams.color;
						var xFunc = function(d) { 
							return xScale(d[2][dataParams.x.idx]); 
						};
						var yFunc = function(d) { return yScale(d[2][dataParams.y.idx]); };
						var sourceUidFn = function(d) { return d[2][dataParams.source.idx]; };
						var keyFn = sourceUidFn;
						var cp = Date.now();
						selection = chart.createPoints(dataParams,selection,pts,keyFn,sizeFn,xFunc,yFunc,sourceUidFn,opacity,fillColor);
						var t = (Date.now() - cp);
						if (t > 1000) // 1s threshold
							console.log("Create points took: "+t+" ms for "+pts.length+" points");
						chart.createPointsEventHandlers(dataParams,selection);
						dataParams.grid = null; // delete grid so brushing can work
						// actions
						var func = params.zooming.actions['low-data'];
						if (func !== undefined && func != null)
							func(params,dataParams,pts.length); // execute low-data function
					} else {
						// actions
						var func = params.zooming.actions['no-data'];
						if (func !== undefined && func != null)
							func(params,dataParams,pts.length); // execute no-data function
					}
					// see if we need to execute data regression
					params.chart.updateAxisX();
					params.chart.updateAxisY();
					SHIV.graph.executeRegression(params.chart);
				}
				// re-select points
				if (selectedPoints.length > 0) {
					SHIV.graphManager.highlightAll(params['svg-elem'],dataParams['source-id'],selectedPoints,true,true);
				}
			}
		});
	if (params.zooming.events.zoomend != null)
		zoom.on("zoomend", params.zooming.events.zoomend);

	// call the function
	params.zooming.elem.call(zoom);
	if (attachToPoints)
		chartBody.selectAll(".point").call(zoom);
}

SHIV.graph.ScatterPlot.prototype.createPoints = function(dataParams,selection,data,keyFunction,sizeFn,xFunc,yFunc,sourceUidFn,opacity,fillColor) {
	if (dataParams.shape == "square") {
		return selection.data(data,keyFunction)
			.enter()
				.append("rect")
					.attr("class", "point")
					.attr("width", sizeFn)
					.attr("height", sizeFn)
					.attr("x", xFunc)
					.attr("y", yFunc)
					.attr("source-id", dataParams['source-id'])
					.attr("source-uid", sourceUidFn)
					.style("opacity",opacity)
					.style("fill",fillColor);
	} else if (dataParams.shape == "circle") { // other shapes revert to circle
		return selection.data(data,keyFunction)
			.enter()
				.append("circle")
					.attr("class", "point")
					.attr("r", sizeFn)
					.attr("cx", xFunc)
					.attr("cy", yFunc)
					.attr("source-id", dataParams['source-id'])
					.attr("source-uid", sourceUidFn)
					.style("opacity", opacity)
					.style("fill", fillColor);
	} else {
		return selection.data(data,keyFunction)
			.enter()
				.append("path")
					.attr("class", "point")
					.attr("d",d3.svg.symbol().type(dataParams.shape))
					.attr("transform", function(d) { 
						var cx = xFunc(d);
						var cy = yFunc(d);
						return "translate("+cx+","+cy+")";
					})
					.attr("x", xFunc)
					.attr("y", yFunc)
					.attr("source-id", dataParams['source-id'])
					.attr("source-uid", sourceUidFn)
					.style("opacity", opacity)
					.style("fill", fillColor);
	}
}

SHIV.graph.ScatterPlot.prototype.createPointsEventHandlers = function(dataParams, selection, params)
{
	var tooltipDiv = this.tooltipDiv !== undefined ? this.tooltipDiv : params.tooltip.div;
	var tooltipFormat = this.tooltipFormat !== undefined ? this.tooltipFormat : params.tooltip.format;
	var xScale = this.xScale;
	var yScale = this.yScale;
	selection.on("mouseover", function(d) {
		if (tooltipDiv != null) {
			tooltipDiv/*.transition()
				.duration(200)*/
				.style("opacity", .9);
			var elem = d3.select(this);
			var puid = elem.attr("source-uid");
			var xVal = "";
			var yVal = "";
			var uids = "";
			if (puid == null) {
				xVal = tooltipFormat(parseFloat(dataParams.x.scale.invert(d[0])));
				yVal = tooltipFormat(parseFloat(dataParams.y.scale.invert(d[1])));
				uids = d[3];
				if (uids == null && d[2] != null)
					uids = d[2][dataParams.source.idx];
				if (uids.length > 5) {
					var aux = "";
					for (var j = 0; j < 5; j++) {
						aux += d[3][j] +",";
					}
					aux+=".. ("+(uids.length-5)+" more)";
					uids = aux;
				}
			} else {
				xVal = tooltipFormat(parseFloat(d[dataParams.x.idx]));
				yVal = tooltipFormat(parseFloat(d[dataParams.y.idx]));
				uids = puid;
			}
			tooltipDiv.html("x: "+xVal+"<br />y: "+yVal+"<br />uid: "+uids)
				.style("left", (d3.event.pageX + 10) + "px")
				.style("top", (d3.event.pageY - 28) + "px");
		}
		this.style.cursor = "pointer";
	})
	.on("mouseout", function(d) {
		if (tooltipDiv != null)
				tooltipDiv/*.transition()
			.duration(500)*/
			.style("opacity", 0);
			this.style.cursor = "auto";
		})
	.on("click", function(d) {
		var elem = d3.select(this);
		if (!SHIV.utils.keyboard.ctrl && !SHIV.utils.keyboard.shift) {
			var res = d3.select(this.parentNode).selectAll(".point")
			.each(function(d) { 
					d.scanned = d.selected = false; 
					d3.select(this).attr("class","point");
				}
			);
		}
		d.selected = !d.selected;
		var pNode = this.parentNode.parentNode;
		while (pNode.nodeName != "svg")
			pNode = pNode.parentNode;
		var sid = elem.attr("source-id");
		if (d.selected)
			elem.attr("class","point selected");
		else
			elem.attr("class","point");
		var puid = elem.attr("source-uid");
		if (puid == null) {
			if (d[3].length == 1)
				SHIV.graphManager.highlight(pNode,sid,d[3][0],d.selected);
			else if (d[3].length > 1)
				SHIV.graphManager.highlightAll(pNode,sid,d[3],d.selected);
		} else {
			SHIV.graphManager.highlight(pNode,sid,puid,d.selected);
		}

	});
}

SHIV.graph.ScatterPlot.prototype.quadtreeFromData = function(dataset,xScale,yScale,data) {
	start = Date.now();
	// need to use actual data domains for quadtree
	var _xScale = SHIV.graph.createScale(data.x.type);
	var _yScale = SHIV.graph.createScale(data.y.type);
	_xScale.domain(data.x.domain).range(xScale.range());
	_yScale.domain(data.y.domain).range(yScale.range());
	// var quadtree = d3.geom.quadtree().extent([[-1, -1], [xScale.range()[1] + 1, yScale.range()[0] + 1]])([]);
	var min = [xScale.domain()[0],yScale.domain()[0]];
	var max = [xScale.domain()[1],yScale.domain()[1]];
	quadtree = new SHIV.geom.quadtree().extent([min,max]);
	for (var i = 0; i < dataset.length; i++) {
		var point = dataset[i];
		//var x = _xScale(point[data.x.idx]);
		//var y = _yScale(point[data.y.idx]);
		quadtree.add([point[data.x.idx],point[data.y.idx],point]); // add backing to quadtree
	}
	quadtree.lastMod = Date.now();
	//console.log("Quadtree construction of "+dataset.length+" points took "+(Date.now() - start)+" ms");
	return quadtree;
}

SHIV.graph.ScatterPlot.prototype.dataFromQuadtree = function(dataParams) {
	if (dataParams.quadtree === undefined || dataParams.quadtree == null) return dataParams.data; // failsafe
	var visible = dataParams.quadtree.walk();
	var newData = [];
	for (var i = 0; i < visible.length; i++) {
		var p = visible[i];
		newData.push(p[2]);
	}
/**
	dataParams.quadtree.visit(function(node,x1,y1,x2,y2) {
		var p = node.point;
		if (p != null)
			newData.push(p[2]);
		return false; // so that this keeps on going
	});
*/
	return newData;
}

SHIV.graph.ScatterPlot.prototype.gridFromData = function(dataset,xScale,yScale,data,noQuad,gridData) {
	if (data.length == 0) return;
	if (gridData === undefined || gridData == null) gridData = false;
	start = Date.now();
	var sparsegrid = [];
	var sparsegridMin = Number.MAX_VALUE;
	var sparsegridMax = 0; // can't be less than this
	if (noQuad === undefined) noQuad = false;
	// domains
//	console.log("xScale.domain() = "+xScale.domain()+", yScale.domain() = "+yScale.domain());
	var scaleSize = data.size.functor;
	var shapeAdd = 0.0;
	if (data.shape == "circle")
		shapeAdd = data.size.functor / 2.0;
	var quadtree = null;
	var addedPoints = 0;
	var outsidePoints = 0;
	var xMax = xScale.range[1];
	var yMax = yScale.range[0];
	if (!noQuad) {
		// need to use actual data domains for quadtree
		var _xScale = SHIV.graph.createScale(data.x.type);
		var _yScale = SHIV.graph.createScale(data.y.type);
		_xScale.domain(data.x.domain).range(xScale.range());
		_yScale.domain(data.y.domain).range(yScale.range());
		// quadtree = d3.geom.quadtree().extent([[-1, -1], [xScale.range()[1] + 1, yScale.range()[0] + 1]])([]);
		var min = [xScale.domain()[0],yScale.domain()[0]];
		var max = [xScale.domain()[1],yScale.domain()[1]];
		quadtree = new SHIV.geom.quadtree().extent([min,max]);
		for (var i = 0; i < dataset.length; i++) {
			var point = dataset[i];
			if (gridData) point = point[2];
			var x = xScale(point[data.x.idx]);
			var y = yScale(point[data.y.idx]);
			if (x < 0 || x > xMax || y < 0 || y > yMax) {
				outsidePoints++;
				continue; // skip this point as it's outside of the viewport
			}
			// bins are 5 pixels wide
			var ix = Math.floor(x / scaleSize);
			var iy = Math.floor(y / scaleSize);
			var ygrid = sparsegrid[ix];
			if (ygrid == null) {
				sparsegrid[ix] = [];
				ygrid = sparsegrid[ix];
				ygrid[iy] = []; // so that we can update the value in the next step
			}
			if (ygrid[iy] == null/* || isNaN(ygrid[iy])*/ || ygrid[iy] === undefined)
				ygrid[iy] = []; // so that we can update the value in the next step
			var pushIndex = data.source.idx != -1 ? point[data.source.idx]: i;
			if (ygrid[iy].indexOf(pushIndex) >= 0) continue;
			ygrid[iy].push(pushIndex);
			var aux = ygrid[iy].length;
			if (aux < sparsegridMin) sparsegridMin = aux;
			if (aux > sparsegridMax) sparsegridMax = aux;
			// this is actual data scale
			x = _xScale(point[data.x.idx]);
			y = _yScale(point[data.y.idx]);
			//console.log("quadtree.add("+x+","+y+","+point+")...");
			quadtree.add([point[data.x.idx],point[data.y.idx],point]); // add backing to quadtree
			addedPoints++;
		}
		quadtree.lastMod = Date.now();
		//console.log("added "+dataset.length+" points to the quadtree("+quadtree.lastMod+")");

	} else {
		for (var i = 0; i < dataset.length; i++) {
			var point = dataset[i];
			if (gridData) point = point[2];
			var x = xScale(point[data.x.idx]);
			var y = yScale(point[data.y.idx]);
			if (x < 0 || x > xMax || y < 0 || y > yMax) {
				outsidePoints++;
				continue; // skip this point as it's outside of the viewport
			}
			// bins are 5 pixels wide
			var ix = Math.floor(x / scaleSize);
			var iy = Math.floor(y / scaleSize);
			var ygrid = sparsegrid[ix];
			if (ygrid == null) {
				sparsegrid[ix] = [];
				ygrid = sparsegrid[ix];
				ygrid[iy] = []; // so that we can update the value in the next step
			}
			if (ygrid[iy] == null/* || isNaN(ygrid[iy])*/ || ygrid[iy] === undefined)
				ygrid[iy] = []; // so that we can update the value in the next step
			var pushIndex = data.source.idx != -1 ? point[data.source.idx]: i;
			if (ygrid[iy].indexOf(pushIndex) >= 0) continue;
			ygrid[iy].push(pushIndex);
			var aux = ygrid[iy].length;
			if (aux < sparsegridMin) sparsegridMin = aux;
			if (aux > sparsegridMax) sparsegridMax = aux;
			addedPoints++;
		}
	}

	// convert the grid into x,y,c data
	var colorScale = d3.scale.linear().domain([sparsegridMin,sparsegridMax]).range([0.3,1.0]);
	var sparsegridData = [];
	for (var i = 0; i < sparsegrid.length; i++) {
		var xgrid = sparsegrid[i];
		if (xgrid == null) continue;
		for (var j = 0; j < xgrid.length; j++) {
			var ygrid = xgrid[j];
			if (ygrid == null) continue;
			sparsegridData.push([i*scaleSize+shapeAdd,j*scaleSize+shapeAdd,ygrid.length,ygrid.sort(function(a,b){return a-b;})]);
		}
	}
	var gridObject = data.grid = {
		'data': sparsegridData,
		'scale': colorScale
	}
	if (!noQuad)
		data.quadtree = quadtree;
	var t = Date.now() - start;
	if (t > 100)
		console.log("Sparse grid construction took "+(Date.now() - start)+" ms with "+sparsegridData.length+" of "+dataset.length+" points (added: "+addedPoints+", outside: "+outsidePoints+")");
	return gridObject;
}

SHIV.graph.ScatterPlot.prototype.updateAxis = function() {

	if (this.xAxis != null) this.svg.select(".x.axis").call(this.xAxis);
	if (this.yAxis != null) this.svg.select(".y.axis").call(this.yAxis);
	if (this.xAxisMinor != null) this.svg.select(".x.axis.minor").call(this.xAxisMinor);
	if (this.yAxisMinor != null) this.svg.select(".y.axis.minor").call(this.yAxisMinor);
	if (this.xAxisGrid != null) this.svg.select(".x.grid").call(this.xAxisGrid);
	if (this.yAxisGrid != null) this.svg.select(".y.grid").call(this.yAxisGrid);

	if (this.params.zooming.zoomVar !== undefined) {
		var xScale = this.params.data[0].x.scale;
		var yScale = this.params.data[0].y.scale;
		this.params.zooming.zoomVar.x(xScale).y(yScale);
	}
}

SHIV.graph.ScatterPlot.prototype.clear = function() {
	this.chartBody.selectAll(".point").remove();
	var params = this.params;
	for (var i = 0; i < params.data.length; i++) {
		var dataParams = params.data[i];
		dataParams.data = [];
		dataParams.quadtree = null;
		dataParams.grid = null;
	}
}

SHIV.graph.ScatterPlot.prototype.reset = function() {
	// for the moment we reset the domain from the first series
	var xdomain = this.params.data[0].x.domain;
	var ydomain = this.params.data[0].y.domain;
	this.xScale.domain(xdomain);
	this.yScale.domain(ydomain);
	this.updateAxis();
	var zoom = this.params.zooming.zoomVar;
	if (d3.event == null) {
		d3.event = {}
	}
	d3.event.sourceEvent = this.reset;
	d3.event.type = "zoom";
	d3.event.target = zoom;
	d3.event.scale = 1;
	d3.event.translate = [0,0];
	this.svg.call(zoom.event);
	// todo: reset zoom
}

SHIV.graph.ScatterPlot.prototype.appendData = function(data,recenterDomain) {
	var start = Date.now();
	var totalPoints = 0;
	if (recenterDomain === undefined || recenterDomain == null)
		recenterDomain = true;
	for (var i = 0; i < data.length; i++) {
		var dataParams = data[i];
		var originalData = dataParams;
		var newDataset = dataParams.data;
		dataParams.data = null; // null it so that the next step doesn't overwrite it
		var originalDataset = this.params.data[i].data;
		dataParams = SHIV.utils.MergeRecursive(originalData, this.params.data[i]);
		for (var j = 0; j < newDataset.length; j++)
			originalDataset.push(newDataset[j]);
		dataParams.data = originalDataset;
		var opacity = dataParams.opacity;
		if (opacity === undefined || opacity == null)
			opacity = 1.0;
		else {
			var aux = parseFloat(opacity);
			if (isNaN(aux)) opacity = 1.0;
			else if (aux < 0) opacity = 0.0;
			else if (aux > 1.0) opacity = 1.0;
			else
				opacity = aux;
		}
		var fillColor = dataParams.color;
		if (fillColor === undefined)
			fillColor = null;
		dataParams.opacity = opacity;
		var sizeScale = dataParams.size.scale;
		var sizeFn = null;
		if (dataParams.size.idx != -1) {
			sizeFn = function(d) { return sizeScale(d[dataParams.size.idx]); };
		} else if (dataParams.size.functor !== undefined && dataParams.size.functor != null) {
			sizeFn = dataParams.size.functor;
		} else
			sizeFn = 3;
		var sourceUidFn = null;
		if (dataParams.source.idx != -1) {
			sourceUidFn = function(d) { return d[dataParams.source.idx]; }
			dataParams.source.functor = sourceUidFn;
			if (dataParams.keyFunction === undefined || dataParams.keyFunction == null)
				dataParams.keyFunction = sourceUidFn;
		} else if (dataParams.source.functor !== undefined && dataParams.source.functor != null) {
			sourceUidFn = dataParams.source.functor;
		}
		var dataset = newDataset;

		// update xScale and yScale
		var updateOldData = false;
		var updateOldDomain = false;
		var dfunctor = dataParams.x.domainFunctor;
		if (dfunctor == null)
			dfunctor = function(d) { 
				return d[dataParams.x.idx]; 
			}
		var newDomain = d3.extent(dataset,dfunctor);
		var oldDomain = dataParams.x.scale.domain();
		var oldDataDomain = dataParams.x.domain;
		if (isNaN(oldDataDomain[0]) && isNaN(oldDataDomain[1])) {
			oldDataDomain = newDomain;
			updateOldDomain = true;
		}
		if (oldDomain[0] < newDomain[0]) {
			newDomain[0] = oldDomain[0];
			updateOldData = true;	
		}
		if (newDomain[0] < oldDataDomain[0]) { 
			oldDataDomain[0] = newDomain[0];
			updateOldDomain = true;
		}
		if (oldDomain[1] > newDomain[1]) {
			newDomain[1] = oldDomain[1];
			updateOldData = true;
		}
		if (newDomain[1] > oldDataDomain[1]) { 
			oldDataDomain[1] = newDomain[1];
			updateOldDomain = true;
		}
		if (recenterDomain && newDomain[0] !== undefined && newDomain[1] !== undefined)
			this.xScale.domain(newDomain);
		if (updateOldDomain) // only update domains if needed
			dataParams.x.domain = oldDataDomain;

		dfunctor = dataParams.y.domainFunctor;
		if (dfunctor == null)
			dfunctor = function(d) { 
				return d[dataParams.y.idx]; 
			}
		newDomain = d3.extent(dataset,dfunctor);
		oldDomain = dataParams.y.scale.domain();
		oldDataDomain = dataParams.y.domain;
		if (isNaN(oldDataDomain[0]) && isNaN(oldDataDomain[1])) {
			oldDataDomain = newDomain;
			updateOldDomain = true;
		}
		if (oldDomain[0] < newDomain[0]) {
			newDomain[0] = oldDomain[0];
			updateOldData = true;	
		}
		if (newDomain[0] < oldDataDomain[0]) { 
			oldDataDomain[0] = newDomain[0];
			updateOldDomain = true;
		}
		if (oldDomain[1] > newDomain[1]) {
			newDomain[1] = oldDomain[1];
			updateOldData = true;
		}
		if (newDomain[1] > oldDataDomain[1]) { 
			oldDataDomain[1] = newDomain[1];
			updateOldDomain = true;
		}
		if (recenterDomain && newDomain[0] !== undefined && newDomain[1] !== undefined)
			this.yScale.domain(newDomain);
		if (updateOldDomain) // only update domains if needed
			dataParams.y.domain = oldDataDomain;

		// update points
		var xScale = this.xScale;
		var yScale = this.yScale;
		var tooltipDiv = this.tooltipDiv;
		var tooltipFormat = this.tooltipFormat;
		var grid = null;

		if (dataParams.data.length > this.params.reductionPts)
			grid = this.gridFromData(dataParams.data,xScale,yScale,dataParams);
		else
			dataParams.quadtree = this.quadtreeFromData(dataParams.data,xScale,yScale,dataParams);

		dataParams.data = this.dataFromQuadtree(dataParams); // resize data to actual size

		this.params.data[i].quadtree = dataParams.quadtree;

		this.params.data[i] = dataParams;
	}

	this.updateAxisX();
	this.updateAxisY();
	this.updateAxis();
	this.updateData();
	//console.log("Points update took: "+(Date.now() - start)+" ms for "+totalPoints+" points");
}


/**
 * SHIV graphing methods.
 *
 * @author Miguel Gomes (alka.setzer@gmail.com)
 * @depends d3.js (>= 3.5)
 * @depends shiv-graphing.js (>= 0.1)
 * @since 0.1
 */

console.log("loading shiv-graphing-histogram.js...");

/**
 * Creates an histogram plot.
 * @param data Data to render (array of arrays, where each row is a new data point)
 * @param params Plot parameters
 * @return Histogram
 */
SHIV.graph.Histogram = function(params) {
	// d3 has a very nice histogram class which we will use for data
	var chart = this.chart = this; // recursive link
	if (params === undefined || params == null)
		params = SHIV.graph.defaultParams();
	else {
		params = SHIV.graph.mergeParams(params);
	}
	this.params = params;
	params.chart = this;
	SHIV.graphManager.addGraph(params);
	var xScale = this.xScale = null;
	var yScale = this.yScale = null;
	var grid = null;
	var paramsMap = {}; // parameters map
	this.binCount = 20;
	if (params.binning !== undefined && params.binning != null) {
		this.binCount = params.binning.count !== undefined && params.binning.count != null ? params.binning.count : 20;
	}
	this.updateAxisType(paramsMap);

	// then we need to create the graph
	var svg = this.svg = d3.select(params.bindto == null ? 'body' : params.bindto).append("svg")
		.attr("width", params.width)
		.attr("height", params.height)
		.append("g")
		.attr("transform", "translate(" + params.margin.left + "," + params.margin.top + ")");
	var svgElem = this.svgElem = d3.select(params.bindto == null ? 'body' : params.bindto).select('svg');
	this.params['svg-elem'] = this.svgElem[0][0];

	var clipPath = this.svg.append("svg:clipPath")
						.attr("id","clip")
						.append("svg:rect")
							.attr("id","clip-rect")
							.attr("x",0)
							.attr("y",0)
							.attr("width",params.width-(params.margin.left+params.margin.right))
							.attr("height",params.height-(params.margin.top+params.margin.bottom));

	var auxArea = this.svg.append("g")
					.attr("id","aux-area");
	this.auxArea = this.params["auxArea"] = auxArea;

	// create zoom area
	var zoomArea = svg.append("g")
			.attr("class","zoomarea")
			.append("rect")
				.attr("x", "0")
				.attr("y", "0")
				.attr("width", params.width-(params.margin.right+params.margin.left))
				.attr("height", params.height-(params.margin.top+params.margin.bottom))
				.style("opacity","0");
	this.zoomArea = params.zooming.elem = zoomArea;

	var chartBody = this.svg.append("g")
				.attr("clip-path", "url(#clip)");
	this.params['chartbody'] = chartBody;
	this.chartBody = chartBody;

	// tooltip
	var tooltipDiv = null;
	var tooltipFormat = null;
	if (params.tooltip.enabled) {
		this.tooltipDiv = tooltipDiv = d3.select("body").append("div") 
					.attr("class", "tooltip")
					.attr("id", "tooltip-"+(params.bindto == null ? 'body' : params.bindto))
					.style("opacity", 0);
		params.tooltip.elem = tooltipDiv;
		if (params.tooltip.format !== undefined && params.tooltip.format != null)
			tooltipFormat = params.tooltip.format;
		else
			tooltipFormat = d3.format(".3f");
		this.tooltipFormat = tooltipFormat;
	}

	// now bind some usefull stuf
	this.params.svg = svg;
	// then we add the axis

	//	this.updateAxis();

	this.updateAxisX();
	this.updateAxisY();

	var xAxis = this.xAxis;
	var xAxisMinor = this.xAxisMinor;
	var xAxisGrid = this.xAxisGrid;
	var yAxis = this.yAxis;
	var yAxisMinor = this.yAxisMinor;
	var yAxisGrid = this.yAxisGrid;

	// attach brush
	if (params.brushing.enabled)
		this.attachBrush();

	// attach zoom
	if (params.zooming.enabled)
		this.attachZoom();

	this.updateData();

}

SHIV.graph.Histogram.prototype.ObjectType = "Histogram";
SHIV.graph.Histogram.prototype.ObjectClassName = "bar";
SHIV.graph.Histogram.prototype.Dimensions = 1;
SHIV.graph.Histogram.prototype.SupportsSize = false;
SHIV.graph.Histogram.prototype.SupportsColor = true;
SHIV.graph.Histogram.prototype.SupportsShape = false;

SHIV.graph.Histogram.prototype.updateAxisType = function(paramsMap) {
	var params = this.params;
	var xInverted = this.params.axis.x.inverted;
	var yInverted = this.params.axis.y.inverted;
	if (params.data !== undefined && params.data != null) {
		for (var i = 0; i < params.data.length; i++) {
			var dataset = params.data[i].data;
			var data = params.data[i];

			if (paramsMap !== undefined && paramsMap != null && data['source-id'] != null)
				paramsMap[data['source-id']] = data;

			// first we need to create the scales
			if (data.x.scale == null) 
				xScale = SHIV.graph.createScale(data.x.type);
			else {
				var _xScale = SHIV.graph.createScale(data.x.type);
				if (""+_xScale != ""+xScale)
					xScale = _xScale;
				else
					xScale = data.x.scale;
			}
			if (data.y.scale == null) 
				yScale = SHIV.graph.createScale(data.y.type);
			else {
				var _yScale = SHIV.graph.createScale(data.y.type);
				if (""+_yScale != ""+yScale)
					yScale = _yScale;
				else
					yScale = data.y.scale;
			}
			// then figure out the domains
			if (data.x.domain == null) {
				var dfunctor = data.x.domainFunctor;
				if (dfunctor == null)
					dfunctor = function(d) { 
						return d[data.x.idx]; 
					}
				xScale.domain(d3.extent(dataset,dfunctor));
				data.x.domain = xScale.domain();
			}
			else
			{
				var dfunctor = data.x.domainFunctor;
				if (dfunctor == null)
					dfunctor = function(d) { 
						return (xInverted ? -1.0 : 1.0) * d[data.x.idx]; 
					}
				var newDomain = d3.extent(dataset,dfunctor);
				var oldDomain = data.x.domain;
				if (newDomain[0] != oldDomain[0] || newDomain[1] != oldDomain[1])
					xScale.domain(newDomain);
				else
					xScale.domain(oldDomain);
			}
			// now we have to bin the data
			data.histogram = d3.layout.histogram().bins(xScale.ticks(this.binCount))
				.value(function(d) { return d[data.x.idx]; })
				.range([xScale.domain()[0]-1,xScale.domain()[1]+1]);
			data.histogramData = data.histogram(dataset);

			if (data.y.domain == null) {
				yScale.domain([0, d3.max(data.histogramData, function(d) { return d.y; })]);
				data.y.domain = yScale.domain();
			}
			else
				yScale.domain(data.y.domain);
			// and set the scale ranges
			xScale.range([0, params.width - (params.margin.left + params.margin.right)]);
			yScale.range([params.height - (params.margin.top + params.margin.bottom),0]);
			this.xScale = data.x.scale = xScale;
			this.yScale = data.y.scale = yScale;

			var d = ((data.x.scale.domain()[1] - data.x.scale.domain()[0]) / this.binCount);// * 2;
			var xd = data.x.scale.domain();
			data.x.scale.domain([xd[0]-d*0.5,xd[1]+d*0.5]);

			// histograms don't support size nor glyph, just color
			// color / alpha
			var opacity = data.opacity;
			if (opacity === undefined || opacity == null)
				opacity = 1.0;
			else {
				var aux = parseFloat(opacity);
				if (isNaN(aux)) opacity = 1.0;
				else if (aux < 0) opacity = 0.0;
				else if (aux > 1.0) opacity = 1.0;
				else
					opacity = aux;
			}
			var fillColor = data.color;
			if (fillColor === undefined)
				fillColor = null;
			data.opacity = opacity;
		}
	}
}

SHIV.graph.Histogram.prototype.updateData = function() {
	// remove bars
	this.svg.selectAll(".bar").remove();
	// now, data
	start = Date.now();
	var totalPoints = 0;
	for (var i = 0; i < this.params.data.length; i++) {
		var dataParams = this.params.data[i];
		var dataset = dataParams.data;
		totalPoints+= dataset.length;
		var bars = this.chartBody.selectAll(".bar");
		var opacity = dataParams.opacity;
		var sourceUidFn = dataParams.source.functor;
		var fillColor = dataParams.color;
		var cp = Date.now();
		bars = this.createBars(dataParams,bars,dataParams.histogramData,dataParams.keyFunction,dataParams.x.scale,dataParams.y.scale,sourceUidFn,opacity,fillColor);
		//console.log("Create[B] Create points took: "+(Date.now() - cp)+" ms");
		this.createBarsEventHandlers(false, dataParams, bars, this.params);
	}
}

SHIV.graph.Histogram.prototype.updateAxisX = function() {
	SHIV.graph.updateAxisX(this);
}
SHIV.graph.Histogram.prototype.updateAxisY = function() {
	SHIV.graph.updateAxisY(this);
}

SHIV.graph.Histogram.prototype.resizeViewport = function(width,height,maintainDomains) {
	SHIV.graph.resizeViewport(this,width,height,maintainDomains);
}

SHIV.graph.Histogram.prototype.detachBrush = function() {
	SHIV.graph.detachBrush(this);
}

SHIV.graph.Histogram.prototype.attachBrush = function(params) {
	if (params === undefined || params == null) params = this.params;

	// create brush area
	var brushArea = this.svg.append("g")
				.attr("class", "brush");
	this.brushArea = params.brushing.elem = brushArea;

	var chart = this;

	var brush = d3.svg.brush()
		.x(d3.scale.identity().domain([0, params.width - (params.margin.left+params.margin.right)]))
		.y(d3.scale.identity().domain([0, params.height - (params.margin.top+params.margin.bottom)]))
		.on("brushstart", params.brushing.events.brushstart != null ? params.brushing.events.brushstart : null)
		.on("brush", params.brushing.events.brush != null ? params.brushing.events.brush : function() { 
			// find extents by brush
			var extent = d3.select(this).select(".extent");
			var width = parseInt(extent.attr('width'));
			var height = parseInt(extent.attr('height'));
			var ex = parseInt(extent.attr("x"));
			var ey = parseInt(extent.attr("y"));
			var addPoints = SHIV.utils.keyboard.ctrl;
			var removePoints = SHIV.utils.keyboard.shift;
			if (!addPoints && !removePoints) {
				d3.select(this.parentNode.parentNode).selectAll(".bar").attr()
				.each(function(d) { 
					d.scanned = d.selected = false; 
					d3.select(this).attr("class","bar");
				}
				);
			}
			if (width == 0 && height == 0) return; // nothing to do
			if (!removePoints) addPoints = true;
			d3.select(this.parentNode.parentNode)
			.selectAll(".bar")
				.each(function(d,i) { //
					var dp = d3.select(this);
					var dx, dy;
					if (this.nodeName == "circle") {
						dx = parseInt(dp.attr('cx'));
						dy = parseInt(dp.attr('cy'));
					} else {
						dx = parseInt(dp.attr('x'));
						dy = parseInt(dp.attr('y'));
					}
					var inside = !( dx < Math.max(ex,0) || dx > ex + width ||
						dy < Math.max(ey,0) || dy > ey + height);
					if (!inside) return;
					if (addPoints)
						dp.attr("class","bar selected")
					else if (removePoints && inside)
						dp.attr("class","bar")
						//console.log(this);
						return this;
					});
			})
		.on("brushend",params.brushing.events.brushend != null ? params.brushing.events.brushend : function() {
			// find extents by brush
			var extent = d3.select(this).select(".extent");
			var width = parseInt(extent.attr('width'));
			var height = parseInt(extent.attr('height'));
			var ex = parseInt(extent.attr("x"));
			var ey = parseInt(extent.attr("y"));
			if (width == 0 && height == 0) return; // nothing to do
			var sels = {};
			var addPoints = SHIV.utils.keyboard.ctrl;
			var removePoints = SHIV.utils.keyboard.shift;
			d3.select(this.parentNode.parentNode)
			.selectAll(".bar.selected")
			.each(function(d) {
				var elem = d3.select(this);
				var dx, dy;
				var sourceId = elem.attr("source-id");
				var uid = elem.attr("source-uid");
				var vals; 
				if ((vals = sels[sourceId]) == null)
					vals = sels[sourceId] = [];
				if (uid == null && d[3] !== undefined) {
					for (var jj = 0; jj < d[3].length; jj++)
						vals.push(d[3][jj]);
				} else
					vals.push(uid);
			});
			for (var p in sels) {
				var vals = sels[p];
				if (vals === undefined || vals == null) continue;
				SHIV.graphManager.highlightAll(this.parentNode.parentNode,p,vals,!removePoints);
			}
		});
	params.brushing.enabled = true;
	params.brushing.brush = brush;
	params.brushing.elem.call(brush);
}

SHIV.graph.Histogram.prototype.zoomEvent = function(howMuch,evtSource) {
	if (howMuch === undefined || howMuch == null) return;
	if (!this.params.zooming.enabled) return;
	var zoom = this.params.zooming.zoomVar;

	//this.svg.call(zoom.event); // https://github.com/mbostock/d3/issues/2387

	// Record the coordinates (in data space) of the center (in screen space).
	var center0 = zoom.center();
	if (center0 == null) {
		// need to set the center
		var xScale = zoom.x();
		var yScale = zoom.y();
		var xDomain = xScale.range();
		var yDomain = yScale.range();
		var x = (xDomain[1]+xDomain[0])*0.5;
		var y = (yDomain[0]+yDomain[1])*0.5;
		center0 = [x,y];
	}
	var translate0 = zoom.translate();
	var scale = zoom.scale();
	var coordinates0 = [(center0[0] - translate0[0]) / scale, (center0[1] - translate0[1]) / scale];
	zoom.scale(zoom.scale() * Math.pow(2, +howMuch));
	
	// Translate back to the center
	var translate = zoom.translate();
	scale = zoom.scale();
	var center1 = [coordinates0[0] * scale + translate[0], coordinates0[1] * scale + translate[1]];
	zoom.translate([translate0[0] + center0[0] - center1[0], translate0[1] + center0[1] - center1[1]]);
	translate = zoom.translate();

	this.updateAxis();

	// no point in doing transitions because we clear the points
	//zoom.event.scale =zoom.scale();
	//zoom.event.translate = zoom.translate();
	if (d3.event == null) {
		d3.event = {}
	}
	d3.event.sourceEvent = evtSource;
	d3.event.type = "zoom";
	d3.event.target = zoom;
	d3.event.scale = scale;
	d3.event.translate = translate;
	this.svg.call(zoom.event);
}

SHIV.graph.Histogram.prototype.zoomIn = function(evt,howMuch) {
	if (howMuch === undefined || howMuch == null) howMuch = "+1";
	this.zoomEvent(howMuch,evt);
}

SHIV.graph.Histogram.prototype.zoomOut = function(evt,howMuch) {
	if (howMuch === undefined || howMuch == null) howMuch = "-1";
	this.zoomEvent(howMuch,evt);
}

SHIV.graph.Histogram.prototype.detachZoom = function() {
	this.params.zooming.enabled = false;
	this.params.zooming.elem.on(".zoom",null);
}

SHIV.graph.Histogram.prototype.attachZoom = function() {
	var params = this.params;
	params.zooming.enabled = true;

	var attachToPoints = true;
	var svg = params.svg;
	var chartBody = params.chartbody;
	// TODO: support multiple axis and scales
	var xAxis = params.axis.x.xAxis !== undefined ? params.axis.x.xAxis : this.xAxis;
	var yAxis = params.axis.y.yAxis !== undefined ? params.axis.y.yAxis : this.yAxis;
	var xAxisMinor = params.axis.x.xAxisMinor !== undefined ? params.axis.x.xAxisMinor : this.xAxisMinor;
	var yAxisMinor = params.axis.y.yAxisMinor !== undefined ? params.axis.y.yAxisMinor : this.yAxisMinor;
	var xAxisGrid = params.axis.grid.xAxisGrid !== undefined ? params.axis.grid.xAxisGrid : this.xAxisGrid;
	var yAxisGrid = params.axis.grid.yAxisGrid !== undefined ? params.axis.grid.yAxisGrid : this.yAxisGrid;
	var xScale = params.data[0].x.scale !== undefined ? params.data[0].x.scale : this.xScale;
	var yScale = params.data[0].y.scale !== undefined ? params.data[0].y.scale : this.yScale;
	var chart = this;
	var zoom = d3.behavior.zoom(svg).x(xScale).y(yScale).size([xScale.range()[1],yScale.range()[0]]);
	params.zooming.zoomVar = zoom;
	params.zooming.lastScale = zoom.scale();
		
	if (params.zooming.events.zoomstart != null)
		zoom.on("zoomstart", params.zooming.events.zoomstart);
	if (params.zooming.events.zoom != null)
		zoom.on("zoom", params.zooming.events.zoom);
	else
		zoom.on("zoom", function() {
			var evt = d3.event;
			if (Object.prototype.toString.call(evt.sourceEvent) == "[object Object]")
				evt = evt.sourceEvent; // bubbling
			if (xAxis != null) svg.select(".x.axis").call(xAxis);
			if (yAxis != null) svg.select(".y.axis").call(yAxis);
			if (xAxisMinor != null) svg.select(".x.axis.minor").call(xAxisMinor);
			if (yAxisMinor != null) svg.select(".y.axis.minor").call(yAxisMinor);
			if (xAxisGrid != null) svg.select(".x.grid").call(xAxisGrid);
			if (yAxisGrid != null) svg.select(".y.grid").call(yAxisGrid);
			chart.updateAxis();

			var params = chart.params;

			var binCount = 20;
			if (params.binning !== undefined && params.binning != null) {
				binCount = params.binning.count !== undefined && params.binning.count != null ? params.binning.count : 20;
			}

			params.zooming.lastScale = evt.scale;
			chartBody.selectAll(".bar").remove();
			for (var i = 0; i < params.data.length; i++) {
				var pts = [];
				var dataParams = params.data[i];
				var xScale = dataParams.x.scale;
				var xRange = xScale.range();
				// quadtree domain
				var _xScale = SHIV.graph.createScale(dataParams.x.type).domain(dataParams.x.domain).range(xRange);
				var xDomain = xScale.domain();
				var bars = chartBody.selectAll(".bar");
		
				var opacity = dataParams.opacity;
				var sourceUidFn = dataParams.source.functor;
				var fillColor = dataParams.color;
				var cp = Date.now();
				// update range

				dataParams.histogram = d3.layout.histogram()
										.bins(xScale.ticks(binCount))
										.value(function(d) { return d[dataParams.x.idx]; })
										.range(xDomain);
				dataParams.histogramData = dataParams.histogram(dataParams.data);
				// need to update yScale // which is this one
				dataParams.y.scale.domain([0, d3.max(dataParams.histogramData, function(d) { return d.y; })]);
				var yDomain = dataParams.y.scale.domain();
				console.log("xScale.domain() = ["+xDomain[0]+","+xDomain[1]+"], yScale.domain() = ["+yDomain[0]+","+yDomain[1]+"]");
				if (params.axis.y.yAxis) params.svg.select(".y.axis").call(params.axis.y.yAxis);
				if (params.axis.y.yAxisMinor != null) params.svg.select(".y.axis.minor").call(params.axis.y.yAxisMinor);
				if (params.axis.grid.yAxisGrid != null) svg.select(".y.grid").call(params.axis.grid.yAxisGrid);
				// need to createBars
				bars = chart.createBars(dataParams,bars,dataParams.histogramData,dataParams.keyFunction,xScale,yScale,sourceUidFn,opacity,fillColor);
				// need to createBarsEvens
				chart.createBarsEventHandlers(false, dataParams, bars, this.params); 
				// need to call actions
				if (pts.length > params.reductionPts) {
					var func = params.zooming.actions['high-data'];
					if (func !== undefined && func != null)
						func(params,dataParams,pts.length); // execute high-data function
				} else if (pts.length > 0 ) {
					var func = params.zooming.actions['low-data'];
					if (func !== undefined && func != null)
						func(params,dataParams,pts.length); // execute low-data function
				} else {
					// actions
					var func = params.zooming.actions['no-data'];
					if (func !== undefined && func != null)
						func(params,dataParams,pts.length); // execute no-data function
				}
			}
		});
	if (params.zooming.events.zoomend != null)
		zoom.on("zoomend", params.zooming.events.zoomend);

	// call the function
	params.zooming.elem.call(zoom);
	if (attachToPoints)
		svg.selectAll(".bar").call(zoom);
}

SHIV.graph.Histogram.prototype.createBars = function(dataParams,selection,data,keyFunction,xFunc,yFunc,sourceUidFn,opacity,fillColor) {
	var height = this.params.height - (this.params.margin.top+this.params.margin.bottom);
	var formatCount = d3.format(",.0f");
	var min = xFunc.domain()[0];
	var bar = selection
			.data(data)
		.enter().append("g")
			.attr("class", "bar")
			.attr("source-id", dataParams['source-id'])
			.style("opacity",opacity)
			.style("fill",fillColor)
			.attr("transform", function(d) { 
				return "translate(" + xFunc(d.x) + "," + yFunc(d.y) + ")"; 
			});

	bar.append("rect")
			.attr("x", 1)
			.attr("width", function(d) {
				return xFunc(min+d.dx)-1;
			})
			.attr("height", function(d) { 
				return height - yFunc(d.y); 
			});

	bar.append("text")
			.attr("dy", ".75em")
			.attr("y", 5)
			.attr("x", 0/*function(d) { return xFunc(min+d.dx) / 2; }*/)
			.attr("text-anchor", "end")
			.attr("transform", "rotate(-90)")
			.text(function(d) { 
				var cnt = formatCount(d.y);
				return cnt != "0" ? cnt : "";
			});
	// add an extra attribute to the data
	bar.each(function(d) {
		var uids = [];
		for (var j = 0; j < d.length; j++) {
			uids.push(d[j][dataParams.source.idx]);
		}
		d.sourceIds = uids.sort(function(a,b) { return a-b; });
	});
	return bar;
}

SHIV.graph.Histogram.prototype.createBarsEventHandlers = function(isGrid, dataParams, selection, params)
{
	var tooltipDiv = this.tooltipDiv !== undefined ? this.tooltipDiv : params.tooltip.div;
	var tooltipFormat = this.tooltipFormat !== undefined ? this.tooltipFormat : params.tooltip.format;
	selection.on("mouseover", function(d) {
		if (tooltipDiv != null) {
			tooltipDiv/*.transition()
				.duration(200)*/
				.style("opacity", .9);
			var xVal = "["+d.x+","+(d.x+d.dx)+"]";
			var yVal = d.y;
			var uids = "";
			if (d.length > 5) {
				var aux = "";
				for (var j = 0; j < 5; j++) {
					aux += d.sourceIds[j] +",";
				}
				aux+=".. ("+(d.y-5)+" more)";
				uids = aux;
			} else if (d.length > 0 && d.length <= 5) {
				for (var j = 0; j < d.length - 1; j++) {
					uids += d.sourceIds[j] +",";
				}
				uids += d.sourceIds[d.length - 1];
			}
			tooltipDiv.html("x: "+xVal+"<br />frequency: "+yVal+"<br />uids: "+uids)
			.style("left", (d3.event.pageX + 10) + "px")
			.style("top", (d3.event.pageY - 28) + "px");
		}
		this.style.cursor = "pointer";
	})
	.on("mouseout", function(d) {
		if (tooltipDiv != null)
			tooltipDiv/*.transition()
				.duration(500)*/
				.style("opacity", 0);
		this.style.cursor = "auto";
	})
	.on("click", function(d) {
		var elem = d3.select(this);
		if (!SHIV.utils.keyboard.ctrl && !SHIV.utils.keyboard.shift) {
			var res = d3.select(this.parentNode).selectAll(".bar")
			.each(function(d) { 
				d.scanned = d.selected = false; 
				d3.select(this).attr("class","bar");
			}
			);
		}
		d.selected = !d.selected;
		var pNode = this.parentNode.parentNode;
		while (pNode.nodeName != "svg")
			pNode = pNode.parentNode;
		var sid = elem.attr("source-id");
		if (d.selected)
			elem.attr("class","bar selected");
		else
			elem.attr("class","bar");
		if (d.length == 1)
			SHIV.graphManager.highlight(pNode,sid,d[0][dataParams.source.idx],d.selected);
		else if (d.length > 1) {
			SHIV.graphManager.highlightAll(pNode,sid,d.sourceIds,d.selected);
		}
	});
}

SHIV.graph.Histogram.prototype.updateAxis = function() {

	if (this.xAxis != null) this.svg.select(".x.axis").call(this.xAxis);
	if (this.yAxis != null) this.svg.select(".y.axis").call(this.yAxis);
	if (this.xAxisMinor != null) this.svg.select(".x.axis.minor").call(this.xAxisMinor);
	if (this.yAxisMinor != null) this.svg.select(".y.axis.minor").call(this.yAxisMinor);
	if (this.xAxisGrid != null) this.svg.select(".x.grid").call(this.xAxisGrid);
	if (this.yAxisGrid != null) this.svg.select(".y.grid").call(this.yAxisGrid);

	if (this.params.zooming.zoomVar !== undefined) {
		var xScale = this.params.data[0].x.scale;
		var yScale = this.params.data[0].y.scale;
		this.params.zooming.zoomVar.x(xScale).y(yScale);
	}
}

SHIV.graph.Histogram.prototype.clear = function() {
	this.svg.selectAll(".bar").remove();
}

SHIV.graph.Histogram.prototype.reset = function() {
	// for the moment we reset the domain from the first series
	var xdomain = this.params.data[0].x.domain;
	var ydomain = this.params.data[0].y.domain;
	this.xScale.domain(xdomain);
	this.yScale.domain(ydomain);
	this.updateAxis();
	var zoom = this.params.zooming.zoomVar;
	if (d3.event == null) {
		d3.event = {}
	}
	d3.event.sourceEvent = this.reset;
	d3.event.type = "zoom";
	d3.event.target = zoom;
	d3.event.scale = 1;
	d3.event.translate = [0,0];
	this.svg.call(zoom.event);
	// todo: reset zoom
}

SHIV.graph.Histogram.prototype.appendData = function(data,recenterDomain) {
	var start = Date.now();
	var totalPoints = 0;
	if (recenterDomain === undefined || recenterDomain == null)
		recenterDomain = true;

	this.binCount = 20;
	if (this.params.binning !== undefined && this.params.binning != null) {
		this.binCount = this.params.binning.count !== undefined && this.params.binning.count != null ? this.params.binning.count : 20;
	}

	for (var i = 0; i < data.length; i++) {
		var dataParams = data[i];
		var originalData = dataParams;
		var newDataset = dataParams.data;
		dataParams.data = null; // null it so that the next step doesn't overwrite it
		var originalDataset = this.params.data[i].data;
		dataParams = SHIV.utils.MergeRecursive(originalData, this.params.data[i]);
		for (var j = 0; j < newDataset.length; j++)
			originalDataset.push(newDataset[j]);
		dataParams.data = originalDataset;
		var opacity = dataParams.opacity;
		if (opacity === undefined || opacity == null)
			opacity = 1.0;
		else {
			var aux = parseFloat(opacity);
			if (isNaN(aux)) opacity = 1.0;
			else if (aux < 0) opacity = 0.0;
			else if (aux > 1.0) opacity = 1.0;
			else
				opacity = aux;
		}
		var fillColor = dataParams.color;
		if (fillColor === undefined)
			fillColor = null;
		dataParams.opacity = opacity;
		var sourceUidFn = null;
		if (dataParams.source.idx != -1) {
			sourceUidFn = function(d) { return d[dataParams.source.idx]; }
			dataParams.source.functor = sourceUidFn;
			if (dataParams.keyFunction === undefined || dataParams.keyFunction == null)
				dataParams.keyFunction = sourceUidFn;
		} else if (dataParams.source.functor !== undefined && dataParams.source.functor != null) {
			sourceUidFn = dataParams.source.functor;
		}
		var dataset = newDataset;

		// update xScale and yScale
		var updateOldData = false;
		var updateOldDomain = false;
		var dfunctor = dataParams.x.domainFunctor;
		if (dfunctor == null)
			dfunctor = function(d) { 
				return d[dataParams.x.idx]; 
			}
		var newDomain = d3.extent(dataset,dfunctor);
		var oldDomain = dataParams.x.scale.domain();
		var oldDataDomain = dataParams.x.domain;
		if (isNaN(oldDataDomain[0]) && isNaN(oldDataDomain[1])) {
			oldDataDomain = newDomain;
			updateOldDomain = true;
		}
		if (oldDomain[0] < newDomain[0]) {
			newDomain[0] = oldDomain[0];
			updateOldData = true;	
		}
		if (newDomain[0] < oldDataDomain[0]) { 
			oldDataDomain[0] = newDomain[0];
			updateOldDomain = true;
		}
		if (oldDomain[1] > newDomain[1]) {
			newDomain[1] = oldDomain[1];
			updateOldData = true;
		}
		if (newDomain[1] > oldDataDomain[1]) { 
			oldDataDomain[1] = newDomain[1];
			updateOldDomain = true;
		}
		if (recenterDomain && newDomain[0] !== undefined && newDomain[1] !== undefined)
			this.xScale.domain(newDomain);
		if (updateOldDomain) // only update domains if needed
			dataParams.x.domain = oldDataDomain;

		// update points
		var xScale = this.xScale;
		var yScale = this.yScale;
		var tooltipDiv = this.tooltipDiv;
		var tooltipFormat = this.tooltipFormat;

		// extend domain so that data fits
		var d = ((dataParams.x.scale.domain()[1] - dataParams.x.scale.domain()[0]) / this.binCount);// * 2;
		var xd = dataParams.x.scale.domain();
		dataParams.x.scale.domain([xd[0]-d*0.5,xd[1]+d*0.5]);

		dataParams.histogram = d3.layout.histogram().bins(dataParams.x.scale.ticks(this.binCount))
				.value(function(d) { 
					return d[dataParams.x.idx]; 
				});
		dataParams.histogramData = dataParams.histogram(dataParams.data);

		yScale = dataParams.y.scale;
		yScale.domain([0, d3.max(dataParams.histogramData, function(d) { return d.y; })]);
		dataParams.y.domain = yScale.domain();

		this.params.data[i] = dataParams;
	}

	this.updateAxisX();
	this.updateAxisY();
	this.updateAxis();
	this.updateData();
	//console.log("Points update took: "+(Date.now() - start)+" ms for "+totalPoints+" points");
}

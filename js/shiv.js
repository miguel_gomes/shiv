/**
 * SHIV main methods.
 *
 * @author Miguel Gomes (alka.setzer@gmail.com)
 * @since 0.1
 */

console.log("loading shiv.js...");

// Compatibility for Date.now which is missing on IE 8
if (!Date.now) {
   Date.now = function() { return new Date().getTime(); }
}

// Create the SHIV top level object
var SHIV = (Window.SHIV === null || Window.SHIV === undefined ? (Window.SHIV = new Object()) : Window.SHIV);
SHIV.Classes = {};
SHIV.Enums = {};
SHIV.server = {};
SHIV.utils = {};

/*
 * Recursively merge properties of two objects
 * @param obj1 Object to merge properties with
 * @param obj2 Object form which we want to merge properties
 * @return obj1 merged with obj2 
 */
SHIV.utils.MergeRecursive = function(obj1, obj2) {
	for (var p in obj2) {
		//console.log("processing: "+p+" = "+obj2[p]);
		try {
			// Property in destination object set; update its value.
			if ( typeof obj2[p] == "object" ) {
				if (obj1[p] === undefined)
					obj1[p] = null;
				obj1[p] = SHIV.utils.MergeRecursive(obj1[p], obj2[p]);
			} else if (obj1[p] == undefined && obj2[p] != undefined) {
				obj1[p] = obj2[p];
			}
		} catch(e) {
			// Property in destination object not set; create it and set its value.
			obj1[p] = obj2[p];
		}
	}
	return obj1;
}

SHIV.utils.keyboard = {
	'alt': false,
	'ctrl': false,
	'shift': false,
	'meta': false,
	'repeat': false,
	'code': 0,
	'key': 0,
	'evt': null
}

SHIV.utils.keyboardHandler = function(e) {
	SHIV.utils.keyboard.evt = e;
	SHIV.utils.keyboard.code = e.code;
	SHIV.utils.keyboard.repeat = e.repeat;
	SHIV.utils.keyboard.key = e.key;
	SHIV.utils.keyboard.alt = e.altKey;
	SHIV.utils.keyboard.ctrl = e.ctrlKey;
	SHIV.utils.keyboard.shift = e.shiftKey;
	SHIV.utils.keyboard.meta = e.metaKey;
}

SHIV.utils.configureKeyboard = function() {
	document.body.onkeydown = SHIV.utils.keyboardHandler;
	document.body.onkeypressed = SHIV.utils.keyboardHandler;
	document.body.onkeyup = SHIV.utils.keyboardHandler;
}

SHIV.parseCommand = function(jqXHR,data) {
	SHIV.datasetManager.parseDatasets(data);
}

/**
 * Method used to format a date to our specifications.
 * @param date Date to format
 * @return formated date
 */
SHIV.formatDate = function(date)
{
	if (date === undefined || date == null || !(date instanceof Date))
		return "N/A";
	// 2011-10-05T14:48:00.000Z
	var month = date.getMonth()+1;
	if (month < 10) month = "0"+month;
	var day = date.getDate();
	if (day < 10) day = "0"+day;
	var hour = date.getHours();
	if (hour < 10) hour = "0"+hour;
	var minutes = date.getMinutes();
	if (minutes < 10) minutes = "0"+minutes;
	var seconds = date.getSeconds();
	if (seconds < 10) seconds = "0"+seconds; 
	var offset = -1*date.getTimezoneOffset() / 60.0; // in hours, the -1 is to switch the offset order
	var offsetHours = Math.floor(offset);
	if (offsetHours < 10) offsetHours = "0"+offsetHours;
	var offsetMinutes = Math.floor((offset - offsetHours) * 60.0);
	if (offsetMinutes < 10) offsetMinutes = "0"+offsetMinutes;
	var formatedDate = date.getFullYear()+"-"+month+"-"+day+" "+hour+":"+minutes+":"+seconds+"."+date.getMilliseconds()+" "+(offset<0?"-":"+")+offsetHours+":"+offsetMinutes;
	return formatedDate;
}

/**
 * SHIV Initialization function.
 */
SHIV.init = function() {
//	ElementQueries.init();
	this.tableManager = new SHIV.Classes.TableManager();
	this.datasetManager = new SHIV.Classes.DatasetManager();
	this.visualizationManager = new SHIV.Classes.VisualizationManager();
	this.jobManager = new SHIV.Classes.JobManager();
	this.server = new SHIV.Classes.Server("localhost",5555);
	// try to connect to localhost server, if that fails the method will show the connect dialog

	this.server.Connect();
	this.datasetManager.updateDatasets(); // either will be executed or queued for execution upon connect
	this.visualizationManager.updateVisualizations(); // either will be executed or queued for execution upon connect
	this.jobManager.updateJobs(); // either will be executed or queued for execution upon connect
	this.datasetManager.populateSupportedDataTypes(); // populate supported data types
}

/**
 * Method used to clear all structures.
 */
SHIV.clearStructures = function() {
	SHIV.tableManager.reset();
	SHIV.datasetManager.reset();
	SHIV.visualizationManager.reset();
	SHIV.jobManager.reset();
}

/**
 * Method used to create a modal window.
 * Buttons are an array of objects with the following properties:
 * text - Text of the button
 * className - Names of classes to add to the button
 * onMouseDown - Function to execute on mouse down
 * onMouseUp - Function to execute on mouse up, note that the close action will execute first
 * @param title Window title
 * @param body body context
 * @param draggable, whether or not the window is draggable
 * @param buttons buttons to add to the window 
 * @param evt Optional event that triggered the window (used for positioning if available)
 */
SHIV.modal = function(title,body,draggable,buttons,evt) {
	var d1 = document.createElement('div');
	d1.className = "modal";
	var d2 = document.createElement('div');
	d2.className = "modal-dialog";
	var d3 = document.createElement('div');
	d3.className = 'modal-content';
	var dheader = document.createElement('div');
	dheader.className = 'modal-header';
	if (draggable) {
		if (evt !== undefined && evt != null) {
			d1.style.left =	""+ (parseInt(evt.pageX) - 300)+"px";
			d1.style.top = ""+ parseInt(evt.pageY)+"px";
			d1.style.transform = "translate(0%,0%)"; // clear transform
		}
		SHIV.Drag.init(dheader,d1);
	}
	var dismissButton = document.createElement('button');
	dismissButton.type = 'button';
	dismissButton.className = 'close';
	dismissButton.title = 'Close'; // i8n
	var spanDismiss = document.createElement('span');
	spanDismiss.setAttribute('aria-hidden','true');
	spanDismiss.innerHTML = '&times;';
	dismissButton.appendChild(spanDismiss);
	dismissButton.onclick = function() { 
					// this, footer, d3, d2, d1
					var container = this.parentNode.parentNode.parentNode.parentNode;
					container.parentNode.removeChild(container);
				};
	dheader.appendChild(dismissButton);
	var h4 = document.createElement('h4');
	h4.className = 'modal-title';
	h4.appendChild(document.createTextNode(title !== undefined && title != null ? title : ""));
	dheader.appendChild(h4);
	d3.appendChild(dheader);
	var dbody = document.createElement('div');
	dbody.className = 'modal-body';
	if (body !== undefined && body != null) {
		if (typeof body == 'string') {
			var p = document.createElement('p');
			if (body.indexOf('<') >= 0 && body.indexOf('>') > 0)
				p.innerHTML = body;
			else
				p.appendChild(document.createTextNode(body));
			dbody.appendChild(p);
		} else {
			dbody.appendChild(body);
		}
	}
	d3.appendChild(dbody);
	var dfooter = document.createElement('div');
	dfooter.className = 'modal-footer';
	var addDismissButton = true;
	if (buttons !== undefined && buttons != null && buttons instanceof Array) {
		for (var i = 0; i < buttons.length; i++) {
			var config = buttons[i];
			if (config == null) continue;
			var button = document.createElement('button');
			button.type = "button";
			button.className = 'btn '+ (config['className'] !== undefined && config['className'] != null ? config['className'] : '');
			if (config['text'] !== undefined && config['text'] != null)
				button.appendChild(document.createTextNode(config['text']));
			if (config['onMouseDown'] !== undefined && config['onMouseDown'] != null)
				button.onmousedown = config['onMouseDown'];
			if (config['onMouseUp'] !== undefined && config['onMouseUp'] != null) {
				button.onmouseupExtension = config['onMouseUp'];
				button.onmouseup = function(evt) { 
					if (!this.onmouseupExtension(evt))
						return; // some error occured
					// close action
					var container = this.parentNode.parentNode.parentNode.parentNode;
					container.parentNode.removeChild(container);
				};
			} else {
				button.onmouseup = function() { 
					// this, footer, d3, d2, d1
					var container = this.parentNode.parentNode.parentNode.parentNode;
					container.parentNode.removeChild(container);
				}
			}
			dfooter.appendChild(button);
			addDismissButton = false;
		};
	}
	if (addDismissButton) {
		// add a dismiss button
		var dismissButton = document.createElement('button');
		dismissButton.type = "button";
		dismissButton.className = "btn btn-primary";
		dismissButton.appendChild(document.createTextNode('Close')); // i8n
		dismissButton.onmouseup = function() { 
					// this, footer, d3, d2, d1
					var container = this.parentNode.parentNode.parentNode.parentNode;
					container.parentNode.removeChild(container);
				};
		dfooter.appendChild(dismissButton);
	}
	d3.appendChild(dfooter);
	d2.appendChild(d3);
	d1.appendChild(d2);
	var contentDiv = document.getElementById('content');
	if (contentDiv == null) contentDiv = document.body;
	contentDiv.appendChild(d1);
};

/**
 * Method used to show a message.
 * @param type Message type (one of: "info", "warning", "error")
 * @param msg Message content
 * @param header Message header (optional)
 * @param title Message title (optional)
 * @param targetToAppend Where to append the message (optional, default is content) 
 * @param onClickFunction Function to call onclick after the close action is executed
 */
SHIV.showMessage = function(type,msg,header,title,targetToAppend,onClickFunction)
{
	var buttons = null;
	if (onClickFunction !== undefined && onClickFunction != null) {
		buttons = new Array();
		var closeButton = new Object();
		closeButton['text'] = "Close";
		closeButton['onMouseUp'] = onClickFunction;
		closeButton['className'] = "btn-primary";
		buttons.push(closeButton);
	}
	SHIV.modal(title,msg,false,buttons);
}

/**
 * Method that creates an input group.
 * @param label Input label
 * @param placeholder Input value placeholder
 * @param value Input value (default: null)
 * @param tooltip Input tooltip (default: null)
 * @param inputId Input id
 * @param type Input type (default: text)
 * @param enabled Whether or not the input is enabled (default: true)
 * @param checked Whether or not the input is checked (only for radio or checkbox type, default: false)
 * @return Created div
 */
SHIV.createInputGroup = function(label, placeholder, value, tooltip, inputId, type, enabled, checked) {
	if (type === undefined || type == null)
		type = 'text';
	if (enabled === undefined || enabled == null || (typeof enabled) != 'boolean')
		enabled = true;
	if (checked === undefined || checked == null || (typeof checked) != 'boolean')
		checked = false;
	var div = document.createElement('div');
	div.className = "input-group";
	var span = document.createElement('span');
	span.className = "input-group-addon";
	span.id = "frm-"+label;
	var input = document.createElement('input');
	input.className = "form-control";
	span.appendChild(document.createTextNode(label != undefined && label != null ? label : ""));
	var ck = null;
	if (type == 'checkbox' || type == 'radio') {		
		ck = document.createElement('input');
		ck.type = type;
		if (inputId !== undefined && inputId != null)
			ck.id = inputId;
		ck.ariaLabel = label;
		if (tooltip !== undefined && tooltip != null) ck.title = tooltip;
		if (!enabled) ck.disabled = true;
		if (checked) ck.checked = true;
		enabled = false;
		input.type = 'text';
		input.disabled = true;
		if (inputId !== undefined && inputId != null) {
			input.id = inputId;
		}
		input.ariaDescribedby = span.id;
		if (placeholder !== undefined && placeholder != null) input.placeholder = placeholder;
	} else {
//		span.appendChild(document.createTextNode(label != undefined && label != null ? label : ""));
		input.type = type;
		if (inputId !== undefined && inputId != null) {
			input.id = inputId;
		}
		if (tooltip !== undefined && tooltip != null) input.title = tooltip;
		if (!enabled) input.disabled = true;
		input.ariaDescribedby = span.id;
		if (placeholder !== undefined && placeholder != null) input.placeholder = placeholder;
		if (value !== undefined && value != null) input.value = value;
	}
	div.appendChild(span);
	if (ck != null)
		div.appendChild(ck);
	else
		div.appendChild(input);
	return div;
}

/**
 * Method that adds auto-complete support to a given input.
 * Note: this requires html5 datalist support
 * @param button Input to which we want to add autocomplete support
 * @param options Autocomplete options
 */
SHIV.addAutoCompleteToInput = function(input,options) {
	if (input === undefined || input == null) return;
	var inputId = input.id;
	input.setAttribute("list",inputId+"-datalist");
	var datalist = document.getElementById(inputId+"-datalist");
	var clearOptionsOnly = (options == null || options.length == 0);
	if (datalist == null && clearOptionsOnly) return; // nothing to do
	if (datalist != null) {
		// need to clear options
		while (datalist.childNodes.length > 0)
			datalist.removeChild(datalist.lastChild); // should be faster?
	}
	if (clearOptionsOnly) return; // nothing more to do
	if (datalist == null) {
		datalist = document.createElement("datalist");
		datalist.id = inputId+"-datalist";
		input.parentNode.appendChild(datalist);
	}
	for (var i = 0; i < options.length; i++) {
		var option = document.createElement("option");
		option.value = options[i];
		datalist.appendChild(option);
	}
}

/**
 * Method that creates an input group for a select.
 * @param label Input label
 * @param values Select values in an array [{'value': "val", 'text': "some text"},{...}], can be null for empty
 * @param tooltip Select tooltip (default: null)
 * @param inputId Select id
 * @param enabled Whether or not the select is enabled (default: true)
 * @return Created div
 */
SHIV.createInputGroupSelect = function(label, values, tooltip, inputId, enabled) {
	if (enabled === undefined || enabled == null || (typeof enabled) != 'boolean')
		enabled = true;
	var div = document.createElement('div');
	div.className = "input-group input-group";
	var span = document.createElement('span');
	span.className = "input-group-addon";
	span.id = "frm-"+label;
	span.appendChild(document.createTextNode(label != undefined && label != null ? label : ""));
	var input = document.createElement('select');
	input.className = "form-control";
	if (inputId !== undefined && inputId != null)
		input.id = inputId;
	if (!enabled)
		input.disabled = true;
	if (values != null) {
		for (var i = 0; i < values.length; i++) {
			var item = values[i];
			var option = document.createElement("option");
			option.value = item.value;
			option.appendChild(document.createTextNode(item.text));
			if (item.selected)
				option.selected = true;
			if (item.tooltip !== undefined)
				option.title = item.tooltip;
			input.appendChild(option);
		}
	}
	div.appendChild(span);
	div.appendChild(input);
	return div;
}

/**
 * Method that shows the Connect dialog.
 */
SHIV.showConnectDialog = function() {
	var divConnectDialog = document.createElement('div');
	divConnectDialog.className = "panel panel-default";
	var divPanelHeading = document.createElement('div');
	divPanelHeading.className = "panel-heading";
	var h3 = document.createElement('h3');
	h3.className = "panel-title";
	h3.appendChild(document.createTextNode("Server Information"));
	divPanelHeading.appendChild(h3);
	divConnectDialog.appendChild(divPanelHeading);
	var divPanelBody = document.createElement('div');
	divPanelBody.className = "panel-body";

	// error display integrated but hidden by default
	var divAlert = document.createElement('div');
	divAlert.className = "alert alert-danger hidden";
	divAlert.role = "alert";
	divAlert.id = "connect-error";
	var divAlertCloseBtn = document.createElement('button');
	divAlertCloseBtn.className = "close";
	divAlertCloseBtn.ariaLabel = "Close";
	divAlertCloseBtn.onmouseup = function(evt) {
		var elem = evt.target;
		if (elem.nodeName.toLowerCase() == "span")
			elem = elem.parentNode.parentNode;
		else
			elem = elem.parentNode;
		var cn = elem.className;
		if (cn.indexOf(" show" > 0))
			elem.className = cn.replace(" show"," hidden");
	}
	var divAlertCloseBtnSpan = document.createElement('span');
	divAlertCloseBtnSpan.ariaHidden = "true";
	divAlertCloseBtnSpan.innerHTML ="&times;";
	divAlertCloseBtn.appendChild(divAlertCloseBtnSpan);
	divAlert.appendChild(divAlertCloseBtn);
	var divAlertSpanWarning = document.createElement('span');
	divAlertSpanWarning.className = "glyphicon glyphicon-exclamation-sign";
	divAlertSpanWarning.ariaHidden = "true";
	divAlert.appendChild(divAlertSpanWarning);
	var divAlertSpanSR = document.createElement('span');
	divAlertSpanSR.className = "sr-only";
	divAlertSpanSR.appendChild(document.createTextNode("Error:"));
	divAlert.appendChild(divAlertSpanSR);
	var divAlertMsg = document.createElement('span');
	divAlertMsg.id = "connect-error-msg";
	divAlert.appendChild(divAlertMsg);
	divPanelBody.appendChild(divAlert);


	divPanelBody.appendChild(
		SHIV.createInputGroup(
			'Hostname',
			SHIV.server != null && SHIV.server.hostname != null ? SHIV.server.hostname : "localhost",
			SHIV.server != null && SHIV.server.hostname != null ? SHIV.server.hostname : "localhost",
			"Hostname or IP address of the server",
			"connect-hostname"
		)
	);
	divPanelBody.appendChild(document.createElement('br'));
	divPanelBody.appendChild(
		SHIV.createInputGroup(
			'Port',
			SHIV.server != null && SHIV.server.port != null ? SHIV.server.port : "5555",
			SHIV.server != null && SHIV.server.port != null ? SHIV.server.port : "5555",
			"Remote server port",
			"connect-port"
		)
	);
	divPanelBody.appendChild(document.createElement('br'));
	divPanelBody.appendChild(
		SHIV.createInputGroup(
			'Username',
			SHIV.server != null && SHIV.server.credentials != null && SHIV.server.credentials.username != null ? SHIV.server.credentials.username : "",
			SHIV.server != null && SHIV.server.credentials != null && SHIV.server.credentials.username != null ? SHIV.server.credentials.username : "",
			"User name",
			"connect-username"
		)
	);
	divPanelBody.appendChild(document.createElement('br'));
	divPanelBody.appendChild(
		SHIV.createInputGroup(
			'Password',
			SHIV.server != null && SHIV.server.credentials != null && SHIV.server.credentials.password != null ? SHIV.server.credentials.password : "",
			SHIV.server != null && SHIV.server.credentials != null && SHIV.server.credentials.password != null ? SHIV.server.credentials.password : "",
			"User password",
			"connect-password",
			'password'
		)
	);
	divConnectDialog.appendChild(divPanelBody);

	var buttons = new Array();
	var okButton = new Object();
	okButton['text'] = "Connect";
	okButton['onMouseUp'] = function(evt) { 
		var hostname = document.getElementById('connect-hostname').value.trim();
		var port = document.getElementById('connect-port').value.trim();
		var username = document.getElementById('connect-username').value.trim();
		var password = document.getElementById('connect-password').value.trim();
		var errorMsg = "";
		// validations
		if (hostname.length == 0)
			errorMsg += "Hostname can not be empty"; // i8n
		if (port.length == 0) {
			port = 5555; // use default
			document.getElementById('connect-port').value = '5555';
		} else {
			var aux = parseInt(port,10);
			if (aux == NaN || aux < 0 || aux > 65535) {
				if (errorMsg.length > 0) errorMsg+="<br />";
					errorMsg += "Given port is invalid";// i8n
			}
		}
		if (!(hostname == "localhost" || hostname=="127.0.0.1" || hostname=="::1")) {
			// check if user has given username and password
			if (username.length == 0) {
				if (errorMsg.length > 0) errorMsg+="<br />";
					errorMsg += "Username can not be empty";// i8n
			}
			if (password.length == 0) {
				if (errorMsg.length > 0) errorMsg+="<br />";
					errorMsg += "Password can not be empty";// i8n
			}
		}
		if (errorMsg.length > 0) {
			var cn = document.getElementById('connect-error').className;
			if (cn.indexOf(" hidden" > 0))
				document.getElementById('connect-error').className = cn.replace(" hidden"," show");
			document.getElementById('connect-error-msg').innerHTML = errorMsg;
			return false; // don't close so that the user sees the errors and stuff
		}
		SHIV.server.Disconnect();
		SHIV.clearStructures();
		SHIV.server.hostname = hostname;
		SHIV.server.port = port;
		SHIV.server.setCredentials(username,password);
		SHIV.server.Connect();
		SHIV.datasetManager.updateDatasets(); // either will be executed or queued for execution upon connect
		SHIV.visualizationManager.updateVisualizations(); // either will be executed or queued for execution upon connect
		SHIV.jobManager.updateJobs(); // either will be executed or queued for execution upon connect
		return true;
	};
	okButton['className'] = "btn-default";
	buttons.push(okButton);
	var cancelButton = new Object();
	cancelButton['text'] = "Cancel";
	cancelButton['className'] = "btn-primary";
	buttons.push(cancelButton);
	SHIV.modal("Settings",divConnectDialog,false,buttons);
}

SHIV.Drag = {

	obj : null,

	init : function(o, oRoot, minX, maxX, minY, maxY, bSwapHorzRef, bSwapVertRef, fXMapper, fYMapper)
	{
		o.onmousedown	= SHIV.Drag.start;

		o.hmode			= bSwapHorzRef ? false : true ;
		o.vmode			= bSwapVertRef ? false : true ;

		o.root = oRoot && oRoot != null ? oRoot : o ;

		if (o.hmode  && isNaN(parseInt(o.root.style.left  ))) o.root.style.left   = "0px";
		if (o.vmode  && isNaN(parseInt(o.root.style.top   ))) o.root.style.top	= "0px";
		if (!o.hmode && isNaN(parseInt(o.root.style.right ))) o.root.style.right  = "0px";
		if (!o.vmode && isNaN(parseInt(o.root.style.bottom))) o.root.style.bottom = "0px";

		o.minX	= typeof minX != 'undefined' ? minX : null;
		o.minY	= typeof minY != 'undefined' ? minY : null;
		o.maxX	= typeof maxX != 'undefined' ? maxX : null;
		o.maxY	= typeof maxY != 'undefined' ? maxY : null;

		o.xMapper = fXMapper ? fXMapper : null;
		o.yMapper = fYMapper ? fYMapper : null;

		o.root.onDragStart	= new Function();
		o.root.onDragEnd	= new Function();
		o.root.onDrag		= new Function();
	},

	start : function(e)
	{
		var o = SHIV.Drag.obj = this;
		e = SHIV.Drag.fixE(e);
		var y = parseInt(o.vmode ? o.root.style.top  : o.root.style.bottom);
		var x = parseInt(o.hmode ? o.root.style.left : o.root.style.right );
		o.root.onDragStart(x, y);

		o.lastMouseX	= e.clientX;
		o.lastMouseY	= e.clientY;

		if (o.hmode) {
			if (o.minX != null)	o.minMouseX	= e.clientX - x + o.minX;
			if (o.maxX != null)	o.maxMouseX	= o.minMouseX + o.maxX - o.minX;
		} else {
			if (o.minX != null) o.maxMouseX = -o.minX + e.clientX + x;
			if (o.maxX != null) o.minMouseX = -o.maxX + e.clientX + x;
		}

		if (o.vmode) {
			if (o.minY != null)	o.minMouseY	= e.clientY - y + o.minY;
			if (o.maxY != null)	o.maxMouseY	= o.minMouseY + o.maxY - o.minY;
		} else {
			if (o.minY != null) o.maxMouseY = -o.minY + e.clientY + y;
			if (o.maxY != null) o.minMouseY = -o.maxY + e.clientY + y;
		}

		document.onmousemove	= SHIV.Drag.drag;
		document.onmouseup		= SHIV.Drag.end;
		o.style.cursor = "move";
		return false;
	},

	drag : function(e)
	{
		e = SHIV.Drag.fixE(e);
		var o = SHIV.Drag.obj;

		var ey	= e.clientY;
		var ex	= e.clientX;
		var y = parseInt(o.vmode ? o.root.style.top  : o.root.style.bottom);
		var x = parseInt(o.hmode ? o.root.style.left : o.root.style.right );
		var nx, ny;

		if (o.minX != null) ex = o.hmode ? Math.max(ex, o.minMouseX) : Math.min(ex, o.maxMouseX);
		if (o.maxX != null) ex = o.hmode ? Math.min(ex, o.maxMouseX) : Math.max(ex, o.minMouseX);
		if (o.minY != null) ey = o.vmode ? Math.max(ey, o.minMouseY) : Math.min(ey, o.maxMouseY);
		if (o.maxY != null) ey = o.vmode ? Math.min(ey, o.maxMouseY) : Math.max(ey, o.minMouseY);

		nx = x + ((ex - o.lastMouseX) * (o.hmode ? 1 : -1));
		ny = y + ((ey - o.lastMouseY) * (o.vmode ? 1 : -1));

		if (o.xMapper)		nx = o.xMapper(y)
		else if (o.yMapper)	ny = o.yMapper(x)

		SHIV.Drag.obj.root.style[o.hmode ? "left" : "right"] = nx + "px";
		SHIV.Drag.obj.root.style[o.vmode ? "top" : "bottom"] = ny + "px";
		SHIV.Drag.obj.lastMouseX	= ex;
		SHIV.Drag.obj.lastMouseY	= ey;

		SHIV.Drag.obj.root.onDrag(nx, ny);
		return false;
	},

	end : function()
	{
		document.onmousemove = null;
		document.onmouseup   = null;
		SHIV.Drag.obj.root.onDragEnd(	parseInt(SHIV.Drag.obj.root.style[SHIV.Drag.obj.hmode ? "left" : "right"]), 
									parseInt(SHIV.Drag.obj.root.style[SHIV.Drag.obj.vmode ? "top" : "bottom"]));
		SHIV.Drag.obj.style.cursor = "auto";
		SHIV.Drag.obj = null;
	},

	fixE : function(e)
	{
		if (typeof e == 'undefined') e = window.event;
		if (typeof e.layerX == 'undefined') e.layerX = e.offsetX;
		if (typeof e.layerY == 'undefined') e.layerY = e.offsetY;
		return e;
	}
};

// next is imports
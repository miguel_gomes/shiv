/**
 * SHIV Table Column representation.
 *
 * @author Miguel Gomes (alka.setzer@gmail.com)
 * @since 0.1
 * @depends shiv.js
 * 
 */

console.log("loading shiv-classes-column.js...");

/**
 * Method used to create new Column objects.
 * @param name Column name
 * @param description Column description
 * @param ucd Column UCD
 * @param unit Column unit
 * @param utype Column utype
 * @param dataType Column data type
 * @param elementSize Column element size
 * @return new Column object
 */
SHIV.Classes.Column = function(name, description, ucd, unit, utype, dataType, elementSize) {
	this.name = name;
	this.description = description;
	this.ucd = ucd;
	this.unit = unit;
	this.utype = utype;
	this.dataType = dataType;
	this.elementSize = elementSize;
}
/** Object type */
SHIV.Classes.Column.prototype.ObjectType = "Column";
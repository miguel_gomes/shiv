/**
 * SHIV Table and TableManager representation.
 *
 * @author Miguel Gomes (alka.setzer@gmail.com)
 * @since 0.1
 * @depends shiv.js
 * 
 */

console.log("loading shiv-classes-table.js...");

/**
 * Table Manager constructor.
 */
SHIV.Classes.TableManager = function() {
	this.tables = {}; // as a map
	this.tablesCount = 0;
}
/** Object type */
SHIV.Classes.TableManager.prototype.ObjectType = "TableManager";

/**
 * Method that resets this manager.
 */
SHIV.Classes.TableManager.prototype.reset = function () {
	this.tables = {}; // as a map
	this.tablesCount = 0;
};

/**
 * Method used to show/hide columns for a given Table.
 * @param table Table to which we want to toggle the columns, due to the way JS works this can also be an event
 */
SHIV.Classes.TableManager.prototype.toggleColumnsForTable = function(table)
{
	var baseTr = null;
	var expand = null;
	if (table === undefined || table == null || (table != null && table['ObjectType'] === undefined)) {
		// check if mouse event
		if (!(table != null && table['ObjectType'] === undefined))
			return;
		// a (target) > td > tr
		expand = table.target;
		baseTr = table.target.parentNode.parentNode;
		var id = baseTr.id.substring(baseTr.id.lastIndexOf('.')+1); // table.
		table = SHIV.tableManager.tables[id];
	}
	var rowKey = "columns."+table.id.replace(" ","_");
	var aux = null;
	if ((aux = document.getElementById(rowKey)) != null) {
		if (aux.style.display == 'none') {
			expand.className = 'table-columns-a glyphicon glyphicon-chevron-up';
			aux.style.display = '';
		}
		else {
			expand.className = 'table-columns-a glyphicon glyphicon-chevron-down';
			aux.style.display = 'none';
		}
		return; // already there
	}
	// TODO change this to an image or something
	expand.className = 'table-columns-a glyphicon glyphicon-chevron-up';
	// tables row
	var tablesRow = document.createElement('tr');
	tablesRow.id = rowKey;
	tablesRow.className = "table-columns-row";
	var tablesCell = document.createElement('td');
	tablesCell.colSpan = 4;
	// tables table
	var datasetTbls = document.createElement('table');
	datasetTbls.className = "table-columns-tbl table table-hover";
	var thead = document.createElement('thead');
	var tr = document.createElement('tr');
	// name, description, ucd, unit, utype, dataType, elementSize
	var th = document.createElement('th');
	th.appendChild(document.createTextNode('Name'))
	th.className="name";
	tr.appendChild(th);
	th = document.createElement('th');
	th.appendChild(document.createTextNode('UCD'))
	th.className="ucd";
	tr.appendChild(th);
	th = document.createElement('th');
	th.appendChild(document.createTextNode('Unit'))
	tr.appendChild(th);
	th = document.createElement('th');
	th.appendChild(document.createTextNode('Data type'))
	tr.appendChild(th);
	thead.appendChild(tr);
	datasetTbls.appendChild(thead);
	var tbody = document.createElement('tbody');
	for (var i = 0; i < table.columns.length; i++) {
		var col = table.columns[i];
		var tr = document.createElement('tr');
		tr.id = rowKey+"."+i;
		var td = document.createElement('td');
		var span = document.createElement("span");
		if (col.description != null)
			span.title = col.description;
		span.appendChild(document.createTextNode(col.name));
		td.appendChild(span);
		tr.appendChild(td);
		td = document.createElement('td');
		td.appendChild(document.createTextNode(col.ucd));
		tr.appendChild(td);
		td = document.createElement('td');
		td.appendChild(document.createTextNode(col.unit));
		tr.appendChild(td);
		td = document.createElement('td');
		td.appendChild(document.createTextNode(col.dataType));
		tr.appendChild(td);
		tbody.appendChild(tr);
	}
	datasetTbls.appendChild(tbody);
	tablesCell.appendChild(datasetTbls);
	tablesRow.appendChild(tablesCell);
	baseTr.parentNode.insertBefore(tablesRow,baseTr.nextSibling);
}

/**
 * Method used to add a DataSet.
 * @param table Table to add
 */
SHIV.Classes.TableManager.prototype.addTable = function(table) {
	if (table == null || table === undefined || table.ObjectType != "Table") return;
	if (this.tables[table.id] != null) return; // table already present
	this.tables[table.id] = table;
	this.tablesCount++;
}
/**
 * Method used to parse table from the server.
 * @param table JSON Object with dataset to parse
 */
SHIV.Classes.TableManager.prototype.parseTable = function(table)
{
	if (table == null || table === undefined) return null; // nothing to do
	// id, name, description, type, rowCnt, colCnt
	var result = new SHIV.Classes.Table(table['id'],table['name'],table['description'],table['type'],table['rows'],table['cols']);
	result.associatedDatasetId = table['dataset-id'];
	result.associatedDatasetName = table['dataset-name'];
	if (table['columns'] !== undefined && table['columns'] != null) {
		var cols = table['columns'];
		result.columns = [];
		for (var c = 0; c < cols.length; c++) {
			var aux = cols[c];
			// name, description, ucd, unit, utype, dataType, elementSize
			var column = new SHIV.Classes.Column(aux['name'],aux['description'],aux['ucd'],aux['unit'],aux['utype'],aux['data.type'],aux['element.size']);
			result.columns.push(column);			
		}
		if (result.columns.length != result.colCnt)
			result.colCnt = result.columns.length;
	}
	return result;
}

/**
 * Method used to parse datasets from the server.
 * @param datasets JSON Object with datasets to parse
 */
SHIV.Classes.TableManager.prototype.parseTables = function(tables) {
	if (tables === undefined || tables == null) return; // nothing to do
	var result = [];
	if (tables['payload'] != null) { // we were given the message
		var blocks = tables['payload'];
		for (var b = 0; b < blocks.length; b++) {
			var block = blocks[b];
			var data = block['data'];
			var aux = data['tables'];
			for (var i = 0; i < aux.length; i++) {
				var table = this.parseTable(aux[i]);
				this.addTable(table);
				result.push(table);
			}
		}
	} else { // we were given the tables array
		for (var i = 0; i < tables.length; i++) {
			var table = this.parseTable(tables[i]);
			this.addTable(table);
			result.push(table);
		}
	}
	//alert('We are now tracking '+this.tablesCount+" tables");
	return result;
}

/**
 * Method used to create new Table objects.
 * @param id Table identifier
 * @param name Table name
 * @param description Table description
 * @param type Table type
 * @param rowCnt Number of rows the table is known to have
 * @param colCnt Number of columns the table is known to have
 * @return new Table object
 */
SHIV.Classes.Table = function(id, name, description, type, rowCnt, colCnt) {
	this.id = id;
	this.name = name;
	this.description = description === undefined || description == null || description == "null" ? null : description;
	this.type = type === undefined || type == null ? "Unknown" : type;
	this.associatedDataset = null;
	this.associatedDatasetId = null;
	this.associatedDatasetName = null;
	this.rowCnt = rowCnt == null || rowCnt === undefined ? -1 : parseInt(rowCnt,10);
	this.colCnt = colCnt == null || colCnt === undefined ? -1 : parseInt(colCnt,10);
	this.columns = [];
	this.requests = {};
}
/** Object type */
SHIV.Classes.Table.prototype.ObjectType = "Table";
/**
 * Method used to add a column to this table
 * @param column Column to add
 */
SHIV.Classes.Table.prototype.addColumn = function(column) {
	if (column == null || column === undefined || column.ObjectType != "Column") return;
	this.columns.push(column);
}
/**
 * Method used to return the number of rows this table has.
 * @return Number of rows
 */
SHIV.Classes.Table.prototype.RowCount = function() {
	return this.rowCnt;
}
/**
 * Method used to return the number of known/associated columns.
 * @return Number of known columns (if none is associated) or number of associated columns
 */
SHIV.Classes.Table.prototype.ColumnCount = function() {
	if (this.columns.length == 0) return this.colCnt;
	else return this.columns.length;
}
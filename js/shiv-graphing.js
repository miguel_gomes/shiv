/**
 * SHIV graphing methods.
 *
 * @author Miguel Gomes (alka.setzer@gmail.com)
 * @depends d3.js (>= 3.5)
 * @since 0.1
 */

console.log("loading shiv-graphing.js...");

// Compatibility for Date.now which is missing on IE 8
if (!Date.now) {
	 Date.now = function() { return new Date().getTime(); }
}

/**
 * Graph Manager constructor.
 */
SHIV.Classes.GraphManager = function() {
	if (SHIV.graphManager !== undefined || SHIV.graphManager != null)
		return SHIV.graphManager;
	this.graphs = [];
	this.sourceMap = {}; // associative array
}
/** Object type */
SHIV.Classes.GraphManager.prototype.ObjectType = "GraphManager";

// Create the SHIV top level object
var SHIV = (Window.SHIV === null || Window.SHIV === undefined ? (Window.SHIV = new Object()) : Window.SHIV);
SHIV.graph = {};
SHIV.geom = {};
SHIV.graphManager = new SHIV.Classes.GraphManager();

/**
 * Method used to check if a given graph is tracked.
 * @param params Parameters of the graph to check
 * @return True if the graph exists, false otherwise
 */
SHIV.Classes.GraphManager.prototype.containsGraph = function(params) {
	if (params === undefined || params == null) return false;
	return this.graphs.indexOf(params) != -1;
}

/**
 * Method used to add a graph for tracking.
 * @param params Graph parameters
 * @return GraphManager
 */
SHIV.Classes.GraphManager.prototype.addGraph = function(params) {
	if (params === undefined || params == null) return this;
	if (this.containsGraph(params)) return this; // already exists
	// add graph
	this.graphs.push(params);
	if (params.data !== undefined && params.data != null) {
		for (var i = 0; i < params.data.length; i++) {
			var data = params.data[i];
			var sourceKey = data['source-id'];
			if (sourceKey === undefined || sourceKey == null) continue;
			var vals = this.sourceMap[sourceKey];
			if (vals === undefined || vals == null) {
				this.sourceMap[sourceKey] = [];
				vals = this.sourceMap[sourceKey];
			}
			vals.push(params);
		}
	}
	return this;
}

/**
 * Method used to get a graph by its identifier.
 * @param id Graph id
 * @return Graph or null if no match was found
 */
SHIV.Classes.GraphManager.prototype.getGraphById = function(id) {
	if (id === undefined || id == null) return null;
	for (var i = 0; i < this.graphs.length; i++) {
		var graph = this.graphs[i];
		if (graph.id == id) return graph;
	}
	return null;
}

/**
 * Method used to get a graph by its svg/canvas element.
 * @param elem SVG or canvas element
 * @return Graph or null if not found
 */
SHIV.Classes.GraphManager.prototype.getParamsBySVG = function(elem) {
	if (elem === undefined || elem == null) return null;
	if (elem.nodeName.toLowerCase() == "svg") {
		for (var i = 0; i < this.graphs.length; i++) {
			var graph = this.graphs[i];
			if (graph['svg-elem'] == elem) return graph;
		}
	} else if (elem.nodeName.toLowerCase() == "canvas") {
		for (var i = 0; i < this.graphs.length; i++) {
			var graph = this.graphs[i];
			if (graph['canvas-elem'] == elem) return graph;
		}
	}
	return null;
}

/**
 * Method used to remove a graph from tracking.
 * @param params Graph parameters
 * @return GraphManager
 */
SHIV.Classes.GraphManager.prototype.removeGraph = function(params) {
	if (params === undefined || params == null) return this;
	if (params instanceof SHIV.graph.ScatterPlot)
		params = params.params;
	var idx = this.graphs.indexOf(params);
	if (idx == -1) return this; // is not tracked
	// swap last index and this one
	this.graphs[idx] = this.graphs[this.graphs.length -1]; // swap
	this.graphs.pop(); // and pop
	if (params.data !== undefined && params.data != null) {
		for (var i = 0; i < params.data.length; i++) {
			var data = params.data[i];
			var sourceKey = data.source;
			if (sourceKey === undefined || sourceKey == null) continue;
			var vals = this.sourceMap[sourceKey];
			if (vals === undefined || vals == null) continue;
			idx = vals.indexOf(params);
			if (idx == -1) continue; // no present
			vals[idx] = vals[vals.length -1]; // swap
			vals.pop(); // pop
		}
	}
	return this;
}

/**
 * Method used to highlight a point in all linked graphs.
 * @param source Source id
 * @param uid Object to highlight id
 */
SHIV.Classes.GraphManager.prototype.highlight = function(evtSource,source,uid,highlight,onlyEvtSource) {
	if (source === undefined || source === null || source == "null") return; // nothing to do
	if (uid === undefined || uid === null || uid == "null") return; // nothing to do
	if (onlyEvtSource === undefined || onlyEvtSource == null) onlyEvtSource = false;
	if (highlight === undefined || highlight == null) highlight = true;
	var linkedGraphs = this.sourceMap[source];
	if (!onlyEvtSource && (linkedGraphs === undefined || linkedGraphs == null || linkedGraphs.length < 2)) return; // nothing to do
	for (var ii = 0; ii < linkedGraphs.length; ii++) {
		var graph = linkedGraphs[ii];
		var className = graph.chart.ObjectClassName;
		var selectedColor = "#FF0000";
		var curColor = "#4682B4";
		var test = false;
		if ((!onlyEvtSource && graph['svg-elem'] == evtSource) || (onlyEvtSource && graph['svg-elem'] != evtSource)) {
			continue;
		}
		var d0 = graph.data[0].color;
		if (d0["selected-color"] !== undefined && d0["selected-color"] != null) {
			selectedColor = d0["selected-color"];
			test = selectedColor != "#FF0000";
		}
		if (d0["functor"] !== undefined && d0["functor"] != null) {
			curColor = d0["functor"];
		}
		var svg = graph.svg;
		var addPoints = SHIV.utils.keyboard.ctrl;
		var removePoints = SHIV.utils.keyboard.shift;
		if (!addPoints && !removePoints) {
			svg.selectAll("."+className).attr()
				.each(function(d) { 
					d.scanned = d.selected = false; 
					d3.select(this).attr("class",className);
				}
			);
		}
		svg.selectAll("."+className).each(function(d) {
			var elem = d3.select(this);
			var esource = elem.attr("source-id");
			if (esource != source) return;
			var euid = elem.attr("source-uid");
			if (euid != null) {
				if (euid != uid) return;
				elem.attr("class",className+(highlight ? " selected" : ""));
				if (highligth && test) elem.style("fill",selectedColor);
				else elem.style("fill",curColor);
			} else {
				var euids = elem.data();
				if (euids == null) return;
				if (className == "point" || className == "tile")
					euids = euids[0][3];
				else
					euids = euids[0].sourceIds;
				var found = false;
				if (euids !== undefined) {
					for (var i = 0; i < euids.length; i++) {
						var euid = euids[i];
						if (euid < uid) continue; // euid is smaller than uid, a match may be somewhere in the future
						if (euid > uid) break; // we have an higher uid, no need to continue into the future
						found = true;
						break;
					}
				}
				if (found) {
					elem.attr("class",className+(highlight ? " selected" : ""));
					if (highligth && test) elem.style("fill",selectedColor);
					else elem.style("fill",curColor);
				}
			}
		});
	}
}

/**
 * Method used to highlight points in all linked graphs.
 * @param source Source id
 * @param uids Objects to highlight
 */
SHIV.Classes.GraphManager.prototype.highlightAll = function(evtSource,source,uids,highlight,onlyEvtSource) {
	if (source === undefined || source === null || source == "null") return; // nothing to do
	if (uids === undefined || uids === null || uids == "null") return; // nothing to do
	if (highlight === undefined || highlight == null) highlight = true;
	if (onlyEvtSource === undefined || onlyEvtSource == null) onlyEvtSource = false;
	var _uids = [];
	for (var i = 0; i < uids.length; i++)
		_uids.push(uids[i]);
	if (highlight === undefined || highlight == null) highlight = true;
	var linkedGraphs = this.sourceMap[source];
	if (!onlyEvtSource && (linkedGraphs === undefined || linkedGraphs == null || linkedGraphs.length < 2)) return; // nothing to do
	var selectedPointsPerGraph = [];
//	var start = Date.now();
	// selection sorting is complicated due to the fact we need to have support for quads and grids
	// as such it's disabled until a good solution is presented
	// sort uids also
	_uids.sort(function(a,b) { return a - b});
	for (var ii = 0; ii < linkedGraphs.length; ii++) {
		var graph = linkedGraphs[ii];
		var className = graph.chart.ObjectClassName;
		if ((!onlyEvtSource && graph['svg-elem'] == evtSource) || (onlyEvtSource && graph['svg-elem'] != evtSource)) {
			continue;
		}
		var selectedColor = "#FF0000";
		var curColor = "#4682B4";
		var test = false;
		var d0 = graph.data[0].color;
		if (d0["selected-color"] !== undefined && d0["selected-color"] != null) {
			selectedColor = d0["selected-color"];
			test = selectedColor != "#FF0000";
		}
		if (d0["functor"] !== undefined && d0["functor"] != null) {
			curColor = d0["functor"];
		}
		var svg = graph.svg;
		var addPoints = SHIV.utils.keyboard.ctrl;
		var removePoints = SHIV.utils.keyboard.shift;
		var elems = svg.selectAll("."+className);
		start = Date.now();
		if (!addPoints && !removePoints) {
			elems
				.each(function(d) { 
					d.scanned = d.selected = false; 
					d3.select(this).attr("class",className);
				}
			);
		}
		elems = elems[0];
		// console.log("clear took: "+(Date.now() - start)+" ms");
		start = Date.now();
		// we loop by elements that way we can use the uids sorting for something useful
		var fndCnt = 0;
		var tries = 0;
		var lastLen = _uids.length;
		while (_uids.length > 0) {
			for (var j = 0; j < elems.length; j++) {
				var elem = d3.select(elems[j]);
				// this would be great but it's possible we have missed something
//				if (elem.attr("class").indexOf(" selected") > 0) 
//					continue; // element was already selected
				var esource = elem.attr("source-id");
				if (esource != source) continue; // this one was easy and always works
				var euid = elem.attr("source-uid");
				var eclass = elem.attr("class");
				var ulen = _uids.length;
				var euids = [];
				if (euid != null)
					euids.push(parseInt(euid));
				else {
					euids = elem.data();
					if (euids == null) break;
					if (className == "point" || className == "tile") {
						euids = euids[0][3];
					}
					else
						euids = euids[0].sourceIds;
				}
				if (euids === undefined || euids == null) continue;
				if (euids.length < _uids.length) { // two loops so that processing can be faster
					var xk = 0;
					for (var k = 0; k < euids.length; k++) {
						var _euid = euids[k];
						for (; xk < ulen; xk++) {
							var uid = parseInt(_uids[xk]);
							if (uid < _euid) continue;
							if (uid > _euid) break;
							// found one!
							elem.attr("class",className+(highlight ? " selected" : ""));
							if (highligth && test) elem.style("fill",selectedColor);
							else elem.style("fill",curColor);
							fndCnt++;
							// splice the element from the uids array
							_uids.splice(xk,1); // splice on the copy
							ulen--;
							break; // continue to the next euid
						}
					}
				} else {
					var xk = 0;
					for (var k = 0; k < ulen; k++) {
						var uid = parseInt(_uids[k]);
						for (; xk < euids.length; xk++) {
							var _euid = euids[xk];
							if (_euid < uid) continue;
							if (_euid > uid) break;
							// found one!
							elem.attr("class",className+(highlight ? " selected" : ""));
							if (highligth && test) elem.style("fill",selectedColor);
							else elem.style("fill",curColor);
							fndCnt++;
							// splice the element from the uids array
							_uids.splice(k,1); // splice on the copy
							ulen--;
							k--; // so that we keep on the same idx
							break; // continue to the next uid
						}
					}
				}
				if (ulen <= 0) break; // no more points to process no need to continue
			}
			//if (_uids.length == uids.length) break; 
			if (_uids.length == lastLen) break; // no points found don't loop again
			lastLen = _uids.length;
			tries++;
		}
		console.log("select took: "+(Date.now() - start)+" ms selecting "+uids.length+" points, found: "+fndCnt+" of "+uids.length+"/"+_uids.length+", tries: "+tries);

	}
}

/**
 * Default data series params
 */
SHIV.graph.defaultDataParams = function() {
	return {
		'label': null, // series label
		'data': null, // actual data
		'source-id': null, // source table or id, if null no linking is possible
		'keyFunction': null, // key function
		'source': { // source id
			'idx': -1,
			'functor': null // function used to return the source value
		},
		'opacity': 1.0, // point opacity
		'shape': 'circle', // shape can be either circle or square at the moment
		'grid': null, // if different of null this means a grid has been used to reduce the amount of the data
		'quadtree': null, // backing quadtree, usually only non null if a grid exists
		'x': {
			'idx': 0, // array index
			'scale': null, // d3 scale, if null, one will be created from data
			'domain': null, // x data domain, if null will be infered from data
			'type': 'linear', // data type: can be any of d3 supported types plus date
			'dateFormat': null, // if data type is date, a date format function from string to numeric must be provided
			'domainFunctor': null, // function used to calculate the domain of the data
			'functor': null // function used to return the x value
		},
		'y': {
			'idx': 1, // array index
			'scale': null, // d3 scale, if null, one will be created from data
			'domain': null, // x data domain, if null will be infered from data
			'type': 'linear', // data type: can be any of d3 supported types plus date
			'dateFormat': null, // if data type is date, a date format function from string to numeric must be provided
			'domainFunctor': null, // function used to calculate the domain of the data
			'functor': null // function used to return the y value
		},
		'size': {
			'idx': -1, // array index
			'scale': null, // d3 scale, if null, one will be created from data
			'domain': null, // x data domain, if null will be infered from data
			'type': 'linear', // data type: can be any of d3 supported types plus date
			'dateFormat': null, // if data type is date, a date format function from string to numeric must be provided
			'domainFunctor': null, // function used to calculate the domain of the data
			'functor': 3 // function used to return the size value
		},
		'color': null // color but can also be setup as x and y above, but preferably just a color code there
	};
};

/**
 * Default graph parameters
 */
SHIV.graph.defaultParams = function() {
	return {
		'svg': null, // d3 base svg element
		'svg-elem': null, // dom svg element
		'margin': { // chart margins
			'top': 20, 
			'right': 20, 
			'bottom': 30, 
			'left': 40
		},
		'reductionPts': 2500, // number of points that serve as a threshold for data reduction to kick in
		'width': 400, // chart width, margins will be reduced from this value
		'height': 400, // chart height, margins will be reduced from this value
		'bindto': null, // i.e append to body
		'data': [ // series
		],
		'tooltip': {
			'elem': null, // tooltip div element
			'enabled': true, // whether or not tooltips are enabled
			'format': null // tooltip formating function, null implies disabled
		},
		'brushing': {
			'elem': null, // d3 brush element
			'enabled': false, // whether or not brushing is enabled
			'events': { // brush functions
				'brushstart': null, // on mousedown
				'brush': null, // on mousemove, if the brush extent has changed
				'brushend': null // on mouseup
			}
		},
		'zooming': {
			'enabled': false, // whether or not zoom is enabled
			'elem': null, // zoom area
			'events': { // zoom functions
				'zoomstart': null, // on mousedown
				'zoom': null, // on mousemove, if the zoom extent has changed
				'zoomend': null // on mouseup
			},
			'actions': {
				'no-data': null, // action to trigger when there is no data in a given viewport
				'low-data': null, // action to trigger when the amount of data in the viewport is less than "reductionPts"
				'high-data': null // action to trigger when the amount of data in the viewport is greater than "reductionPts"
			}
		},
		'axis': {
			'grid': {
				'enabled': true, // whether or not to show the grid
				'xelem': null, // d3 svg x axis grid elem
				'yelem': null // d3 svg y axis grid elem
			},
			'x': {
				'elem': null, // d3 svg axis element
				'elemMinor': null, // d3 svg axis minor ticks axis
				'enabled': true, // whether or not the axis will be rendered
				'position': 'bottom', // either bottom or top of chart
				'inverted': false, // whether or not the data is inverted for this axis
				'title': {
					'label': null, // if set to null no title will be added
					'position': 'inside', // either inside or outside of chart
					'align': 'end' // where to align the label
				},
				'tick': {
					'format': null, // tick label formatting function
					'rotate': 0, // tick label rotation
					'minor': {
						'enabled': true, // whether or not minor ticks should be enabled
						'size': 3 // minor tick size
					},
					'major': {
						'enabled': true, // whether or not major ticks should be enabled
						'size': 6 // major tick size
					}
				}
			},
			'y': {
				'elem': null, // d3 svg axis element
				'elemMinor': null, // d3 svg axis minor ticks axis
				'enabled': true, // whether or not the axis will be rendered
				'position': 'left', // either left or right of chart
				'inverted': false, // whether or not the data is inverted for this axis
				'title': {
					'label': null, // if set to null no title will be added
					'position': 'inside', // either inside or outside of chart
					'align': 'end' // where to align the label
				},
				'tick': {
					'format': null, // tick label formatting function
					'rotate': 0, // tick label rotation
					'minor': {
						'enabled': true, // whether or not minor ticks should be enabled
						'size': 3 // minor tick size
					},
					'major': {
						'enabled': true, // whether or not major ticks should be enabled
						'size': 6 // major tick size
					}
				}
			}
		},
		'title': { // title options
			'enabled': true, // whether or not the title should be enabled
			'location': 'top', // one of: top, bottom (ignored at the moment)
			'align': 'left', // text-alignment, one of: left, right, center (ignored at the moment)
			'label': "" // chart title, should override with correct title
		},
		'legend': { // legend options
			'enabled': false, // legend enabled or not
			'location': 'bottom' // one of: top, bottom, left, right
		}
	};
};



/**
 * Method used to merge parameters.
 * @param params Parameters object to merge with default parameters
 * @return Merged parameters
 */
SHIV.graph.mergeParams = function(params) {
	if (params === undefined || params == null) return params;
	if (params.data !== undefined && params.data != null) {
		for (var i = 0; i < params.data.length; i++) {
			var originalData = params.data[i];
			params.data[i] = SHIV.utils.MergeRecursive(originalData, SHIV.graph.defaultDataParams());
		}
	}
	return SHIV.utils.MergeRecursive(params,SHIV.graph.defaultParams());
}

/**
 * Method that creates a d3 scale.
 * @param Scale type (must be supported by d3)
 * @return D3 scale, if type is unsupported, linear will be used
 */
SHIV.graph.createScale = function(type) {
	var aux = type !== undefined && type != null ? type.toLowerCase() : null;
	var scale = null;
	if (aux == "identity") scale = d3.scale.identity();
	else if (aux == "log") scale = d3.scale.log();
	else if (aux == "pow") scale = d3.scale.pow();
	else if (aux == "quantile") scale = d3.scale.quantile();
	else if (aux == "quantize") scale = d3.scale.quantize();
	else if (aux == "sqrt") scale = d3.scale.sqrt();
	else if (aux == "threshold") scale = d3.scale.threshold();
	else if (aux == "time") scale = d3.time.scale();
	else if (aux == "ordinal") scale = d3.scale.ordinal();
	else /* if (aux == "linear") */ scale = d3.scale.linear();
	return scale;
}

/**
 * Method used to create and show the properties dialog for a given chart+visualization combo
 * @param chart Chart for which the properties dialog is needed
 * @param evt Optional event that triggered this dialog
 */
SHIV.graph.showProperties = function(chart,evt) {
	var divCreateNewVisualization = document.createElement('div');
	//divCreateNewVisualization.className = "panel panel-default";
/*
	var divPanelHeading = document.createElement('div');
	divPanelHeading.className = "panel-heading";
	var h3 = document.createElement('h3');
	h3.className = "panel-title";
	h3.appendChild(document.createTextNode("Chart Properties"));
	divPanelHeading.appendChild(h3);
	divCreateNewVisualization.appendChild(divPanelHeading);
*/
	// tabs
	var tabUL = document.createElement('ul');
	tabUL.className = "nav nav-tabs";
	tabUL.role = "tablist";
	// <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Home</a></li>
	var liGeneral = document.createElement('li');
	liGeneral.className = "active";
	liGeneral.role = "presentation";
	var aGeneral = document.createElement('a');
	aGeneral.appendChild(document.createTextNode("General"));
	//aGeneral.href="";
	aGeneral.role = "tab";
	aGeneral.style.cursor = "pointer";
	aGeneral.dataToggle = chart.params.id+"-tab-general";
	var toggleTabFn = function(evt) {
		var a = evt.target;
		var li = a.parentNode;
		var targetId = a.dataToggle;
		var ul = a;
		while (ul.nodeName.toLowerCase() != "ul") {
			ul = ul.parentNode;
		}
		var lis = ul.getElementsByTagName('li');
		for (var i = 0; i < lis.length; i++)
			lis[i].className = "";
		var parent = ul.parentNode;
		var panes = [];
		var possibilities = parent.getElementsByTagName('div');
		for (var i = 0; i < possibilities.length; i++)
			if (possibilities[i].className.indexOf("tab-pane") >= 0) panes.push(possibilities[i]);
		for (var i = 0; i < panes.length; i++)
			panes[i].className = "tab-pane";
		li.className = "active";
		var pane = document.getElementById(targetId);
		pane.className = "tab-pane active";
		return false;
	};
	aGeneral.onmouseup = toggleTabFn;
	liGeneral.appendChild(aGeneral);
	tabUL.appendChild(liGeneral);
	var axis = ["x","y","z"];
	var d0 = chart.params.axis;
	if (d0 !== undefined && d0 != null) {
		for (var i = 0; i < axis.length; i++) {
			if (d0[axis[i]] === undefined || d0[axis[i]] == null || d0[axis[i]].enabled == false) continue;
			var li = document.createElement("li");
			li.className = "";
			li.role = "presentation";
			var a = document.createElement('a');
			a.appendChild(document.createTextNode(axis[i].toUpperCase()+" Axis"));
			//a.href="#";
			a.role="profile";
			a.style.cursor = "pointer";
			a.onmouseup = toggleTabFn;
			a.dataToggle = chart.params.id+"-tab-axis-"+axis[i];
			li.appendChild(a);
			tabUL.appendChild(li);
		}
	}
	divCreateNewVisualization.appendChild(tabUL);

	// content
	var divPanelBody = document.createElement('div');
	divPanelBody.className = "tab-content";//"panel-body";
/*
	// error display integrated but hidden by default
	var divAlert = document.createElement('div');
	divAlert.className = "alert alert-danger hidden";
	divAlert.role = "alert";
	divAlert.id = "vis-create-error";
	var divAlertCloseBtn = document.createElement('button');
	divAlertCloseBtn.className = "close";
	divAlertCloseBtn.ariaLabel = "Close";
	divAlertCloseBtn.onmouseup = function(evt) {
		var elem = evt.target;
		if (elem.nodeName.toLowerCase() == "span")
			elem = elem.parentNode.parentNode;
		else
			elem = elem.parentNode;
		var cn = elem.className;
		if (cn.indexOf(" show" > 0))
			elem.className = cn.replace(" show"," hidden");
	}
	var divAlertCloseBtnSpan = document.createElement('span');
	divAlertCloseBtnSpan.ariaHidden = "true";
	divAlertCloseBtnSpan.innerHTML ="&times;";
	divAlertCloseBtn.appendChild(divAlertCloseBtnSpan);
	divAlert.appendChild(divAlertCloseBtn);
	var divAlertSpanWarning = document.createElement('span');
	divAlertSpanWarning.className = "glyphicon glyphicon-exclamation-sign";
	divAlertSpanWarning.ariaHidden = "true";
	divAlert.appendChild(divAlertSpanWarning);
	var divAlertSpanSR = document.createElement('span');
	divAlertSpanSR.className = "sr-only";
	divAlertSpanSR.appendChild(document.createTextNode("Error:"));
	divAlert.appendChild(divAlertSpanSR);
	var divAlertMsg = document.createElement('span');
	divAlertMsg.id = "vis-create-error-msg";
	divAlert.appendChild(divAlertMsg);
	divPanelBody.appendChild(divAlert);
*/
	// general pane
	var generalPane = document.createElement('div');
	generalPane.className = "tab-pane active";
	generalPane.id = chart.params.id+"-tab-general";
	generalPane.role = "tabpanel";

	generalPane.appendChild(document.createElement('br'));

	generalPane.appendChild(
		SHIV.createInputGroup(
			'Chart title',
			"Chart title",
			chart.params.title.label != null ? chart.params.title.label : "",
			"Chart name",
			"chart-title"
		)
	);
	// grid
	generalPane.appendChild(
		SHIV.createInputGroup(
			"Chart grid",
			"",
			null,
			"Toggle chart grid display",
			"chart-grid",
			"checkbox",
			true, // enabled
			chart.params.axis.grid.enabled
		)
	);
	if (chart.SupportsSize) {
		var data0 = chart.params.data[0];
		var hasData0 = data0 !== undefined && data0 != null;
		var curSize = 5;
		var szObject = hasData0 ? data0.size : null;
		if (szObject !== undefined && szObject != null) {
			if (szObject.functor !== undefined && szObject.functor != null && !isNaN(szObject.functor))
				curSize = szObject.functor;
			else if (!isNaN(szObject))
				curSize = szObject;
		}
		var elemGroup = SHIV.createInputGroup(
			"Element size",
			"",
			curSize,
			"Chart elements size",
			"chart-size",
			"range"
		);
		var elem = elemGroup.getElementsByTagName("INPUT")[0];
		elem.min = 1;
		elem.max = 50;
		elem.step = 1;
		generalPane.appendChild(elemGroup);
	}
	if (chart.SupportsColor) {
		var data0 = chart.params.data[0];
		var hasData0 = data0 !== undefined && data0 != null;
		var curColor = "#4682B4";
		var curSelColor = "#FF0000"; // red
		var cObject = hasData0 ? data0.color : null;
		if (cObject !== undefined && cObject != null) {
			if (cObject.functor !== undefined && cObject.functor != null)
				curColor = cObject.functor;
			else if ((typeof cObject) == "string") {
				curColor = cObject;
				cObject = {"functor":curColor,"selected-color":curSelColor};				
			}
			if (cObject["selected-color"] !== undefined && cObject["selected-color"] != null)
				curSelColor = cObject["selected-color"];
		}
		var elemGroup = SHIV.createInputGroup(
			"Element colour",
			"",
			curColor,
			"Chart elements colour",
			"chart-colour",
			"color"
		);
		var elem = elemGroup.getElementsByTagName("INPUT")[0];
		elem.patern = "#[a-f0-9]{6}[a-f0-9]?[a-f0-9]?";
		generalPane.appendChild(elemGroup);
		var elemGroup = SHIV.createInputGroup(
			"Selection colour",
			"",
			curSelColor,
			"Chart selected elements colour",
			"chart-colour-selected",
			"color"
		);
		var elem = elemGroup.getElementsByTagName("INPUT")[0];
		elem.patern = "#[a-f0-9]{6}[a-f0-9]?[a-f0-9]?";
		generalPane.appendChild(elemGroup);
	}
	if (chart.SupportsColorRange) {
		var data0 = chart.params.data[0];
		var hasData0 = data0 !== undefined && data0 != null;
		var cObject = hasData0 ? data0.color : {"min": "#FFFFFF", "max": "#4682B4", "range": "custom", "cardinality": "8"};
		if (cObject.range === undefined || cObject.range == null) cObject.range = "custom";
		var themeItems = [];
		themeItems.push({"value":"custom","text":"Custom","tooltip":"Create a custom linear color map"});
		for (var key in colorbrewer) {
			themeItems.push({"value":key,"text":key,"tooltip":"Color Brewer color map","selected":key==cObject.range});
		}
		var themeSelect = SHIV.createInputGroupSelect("Color map",themeItems,"Color maps","chart-range-theme");
		themeSelect.onchange = function(evt) {
			var sel = evt.target;
			var cardElem = document.getElementById("chart-range-theme-cardinality");
			var minElem = document.getElementById("chart-range-min");
			var maxElem = document.getElementById("chart-range-max");
			var rangeKey = sel.value;
			while (cardElem.childNodes.length > 0) {
				cardElem.removeChild(cardElem.childNodes[cardElem.childNodes.length-1]);
			}
			var cardinalityItems = [];
			if (rangeKey == "custom") {
				cardElem.disabled = true;
				minElem.disabled = false;
				maxElem.disabled = false;
				minElem.value = cObject.min;
				maxElem.value = cObject.max;
				cardinalityItems.push({"value":"2","text":"2","tooltip":"2 Color ramp map","selected":"true"});
			} else {
				cardElem.disabled = false;
				minElem.disabled = true;
				maxElem.disabled = true;
				var brewMap = colorbrewer[rangeKey];
				var found = false;
				var f0 = -1;
				for (var key in brewMap) {
					f0 = key;
					if (cObject.cardinality == key) {
						found = true;
						break;
					}
				}
				if (!found) cObject.cardinality = f0;
				for (var key in brewMap)
					cardinalityItems.push({"value":key,"text":key,"tooltip":key+" Color ramp map","selected":key == cObject.cardinality});
				var brewRange = brewMap[cObject.cardinality];
				minElem.value = brewRange[0];
				maxElem.value = brewRange[brewRange.length-1];
			}
			for (var i = 0; i < cardinalityItems.length; i++) {
				var item = cardinalityItems[i];
				var option = document.createElement("option");
				option.value = item.value;
				option.appendChild(document.createTextNode(item.text));
				if (item.selected)
					option.selected = true;
				if (item.tooltip !== undefined)
					option.title = item.tooltip;
				cardElem.appendChild(option);
			}
		}
		var xp = document.createElement('span');
		xp.className = 'form-inline';
		xp.appendChild(themeSelect);
		xp.appendChild(document.createTextNode(' '));
		var cardinalityItems = [];
		if (cObject.range != "custom") {
			var brewMap = colorbrewer[cObject.range];
			if (cObject.cardinality === undefined || cObject.cardinality == null) cObject.cardinality = ""; // we will fix this now
			var found = false;
			var f0 = -1;
			for (var key in brewMap) {
				f0 = key;
				if (cObject.cardinality == key) {
					found = true;
					break;
				}
			}
			if (!found) cObject.cardinality = f0;
			for (var key in brewMap) {
				cardinalityItems.push({"value":key,"text":key,"tooltip":key+" Color ramp map","selected":key == cObject.cardinality});
			}
		} else {
			cardinalityItems.push({"value":"2","text":"2","tooltip":"2 Color ramp map","selected":"true"});
		}
		var cardinalitySelect = SHIV.createInputGroupSelect("#",cardinalityItems,"Number of base colours in the map","chart-range-theme-cardinality",cObject.range != "custom");
		cardinalitySelect.onchange = function(evt) {
			var cardElem = evt.target;
			var themeElem = document.getElementById("chart-range-theme");
			var minElem = document.getElementById("chart-range-min");
			var maxElem = document.getElementById("chart-range-max");
			var rangeKey = themeElem.value;
			var brewMap = colorbrewer[rangeKey];
			var brewRange = brewMap[cardElem.value];
			minElem.value = brewRange[0];
			maxElem.value = brewRange[brewRange.length-1];
		}
		xp.appendChild(cardinalitySelect);
		xp.appendChild(document.createTextNode(' '));
		var elemGroup = SHIV.createInputGroup(
			"Min",
			"",
			cObject.min,
			"Color for the minimum value",
			"chart-range-min",
			"color",
			cObject.range == "custom"
		);
		var elem = elemGroup.getElementsByTagName("INPUT")[0];
		elem.patern = "#[a-f0-9]{6}[a-f0-9]?[a-f0-9]?";
		elem.style.width = "3em";
		xp.appendChild(elemGroup);
		xp.appendChild(document.createTextNode(' '));
		elemGroup = SHIV.createInputGroup(
			"Max",
			"",
			cObject.max,
			"Color for the maximum value",
			"chart-range-max",
			"color",
			cObject.range == "custom"
		);
		var elem = elemGroup.getElementsByTagName("INPUT")[0];
		elem.patern = "#[a-f0-9]{6}[a-f0-9]?[a-f0-9]?";
		elem.style.width = "3em";
		xp.appendChild(elemGroup);
		generalPane.appendChild(xp);
		generalPane.appendChild(document.createElement('br'));
	}
	if (chart.SupportsShape) {
		var data0 = chart.params.data[0];
		var hasData0 = data0 !== undefined && data0 != null;
		var curShape = hasData0 ? data0.shape : "square";
		var shapeItems = [];
		shapeItems.push({"value":"square","text":"Square","tooltip":"An axis-aligned square"});
		shapeItems.push({"value":"circle","text":"Circle","tooltip":"A circle"});
		shapeItems.push({"value":"cross","text":"Cross","tooltip":"A Greek cross or plus sign"});
		shapeItems.push({"value":"diamond ","text":"Diamond","tooltip":"A rhombus"});
		shapeItems.push({"value":"triangle-down","text":"Time","tooltip":"A downward-pointing equilateral triangle"});
		shapeItems.push({"value":"triangle-up","text":"Identity","tooltip":"An upward-pointing equilateral triangle"});
		if (hasData0) {
			for (var j = 0; j < shapeItems.length; j++) {
				var titem = shapeItems[j];
				if (curShape == titem["value"]) {
					titem["selected"] = true;
					break;
				}
			}
		} else
			shapeItems[0]["selected"]=true;

		var shapeSelect = SHIV.createInputGroupSelect("Element shape",shapeItems,"Shape of elements","chart-shape");
		generalPane.appendChild(shapeSelect);
	}

	if (chart.SupportsRegression) {
		var xp = document.createElement("span");
		xp.className = "form-inline";
		var chartRegressionType = chart.params.regressionType;
		var chartRegressionPolyDegree = chart.params.regressionPolynomialDegree;
		// allow regression utilitis
		var regressionItems = [];
		regressionItems.push({"value":"none","text":"None","tooltip":"No regression"});
		regressionItems.push({"value":"linear","text":"Linear","tooltip":"Linear regression"});
		regressionItems.push({"value":"linearThroughOrigin","text":"Linear through origin","tooltip":"Linear regression that passes through the origin"});
		regressionItems.push({"value":"exponential","text":"Exponential","tooltip":"Exponential regression"});
		regressionItems.push({"value":"logarithmic ","text":"Logarithmic","tooltip":"Logarithmic regression"});
		regressionItems.push({"value":"power","text":"Power","tooltip":"Power regression"});
		regressionItems.push({"value":"polynomial","text":"Polynomial","tooltip":"Polynomial Nth degree regression"});
		regressionItems.push({"value":"lastvalue","text":"Last value","tooltip":"Uses the last value to fill the blanks when forecasting (not a regression)"});
		if (chartRegressionType !== undefined && chartRegressionType != null) {
			for (var t = 0; t < regressionItems.length; t++) {
				var item = regressionItems[t];
				if (item.value == chartRegressionType) {
					item["selected"] = true;
					break;
				}
			}
		} else {
			regressionItems[0]["selected"] = true;
			chart.params.regressionType = chartRegressionType = "none";
		}

		var regressionSelect = SHIV.createInputGroupSelect("Regression",regressionItems,"Regression method to use","chart-regression");
		xp.appendChild(regressionSelect);
		regressionSelect.onchange = function (evt) {
			var sel = evt.target;
			var tgt = document.getElementById("chart-regression-degree");
			if (tgt == null) return;
			if (sel.value == "polynomial") {
				tgt.disabled = false;
			} else {
				tgt.disabled = true;
			}
		}

		if (chartRegressionPolyDegree === undefined || chartRegressionPolyDegree == null) {
			chart.params.regressionPolynomialDegree = chartRegressionPolyDegree = 1;
		}
		var polyFit = chartRegressionType == "polynomial";
		xp.appendChild(document.createTextNode(' '));
		var degreeInput = SHIV.createInputGroup(
				"Degree",
				"",
				polyFit ? chartRegressionPolyDegree : "",
				"Polynomial regression degree (higher levels take more time)",
				"chart-regression-degree",
				"text",
				polyFit
			);
		degreeInput.getElementsByTagName("INPUT")[0].style.width = "5em";
		xp.appendChild(degreeInput);

		generalPane.appendChild(xp);
		generalPane.appendChild(document.createElement('br'));
	}

	divPanelBody.appendChild(generalPane);

	//divPanelBody.appendChild(document.createElement('br'));
	var visDimsMap = [
		["X","Size","Color","Glyph","Source ID","Particle Count","Page ID","Type"],
		["X","Y","Size","Color","Glyph","Source ID","Particle Count","Page ID","Type"],
		["X","Y","Z","Size","Color","Glyph","Source ID","Particle Count","Page ID","Type"]
	];
	var dims = ["x","y","z"];
	var vd = chart.params.dimensionality != undefined ? chart.params.dimensionality : 3;
	if (vd < 1) vd = 1;
	var visDims = visDimsMap[vd-1];
	var data0 = chart.params.data[0];
	var hasData0 = data0 != undefined && data0 != null
	if (hasData0) {
		var ddesc = data0['data-description'];
		if (ddesc !== undefined && ddesc != null)
			visDims = ddesc;
	}

	for (var i = 0; i < chart.Dimensions; i++) {
		var name = dims[i].toUpperCase();
		var lname = dims[i];
		var block = document.createElement("div");
		block.id = chart.params.id+"-tab-axis-"+lname;
		block.role = "tabpanel";
		block.className = "tab-pane";
		//xBlock.className = "hidden";
		block.appendChild(document.createElement('br'));
		block.appendChild(
			SHIV.createInputGroup(
				name+" title",
				"",
				chart.params.axis[lname].title.label,
				name+" axis title",
				"chart-dim-title-"+lname
			)
		);
		//block.appendChild(document.createElement('br'));
		//var xp = document.createElement("span");
		//xp.className = "form-inline";
		var sourceItems = [];
		for (var j = 0; j < visDims.length; j++) {
			var iselected = (j == i);
			if (hasData0) iselected = (j == data0[lname].idx);
			sourceItems.push({"value":j,"text":visDims[j],"selected":iselected});
		}
		var sourceSelect = SHIV.createInputGroupSelect("Source",sourceItems,"Axis source","chart-dim-source-"+lname);
		block.appendChild(sourceSelect);
		var typeItems = [];
		typeItems.push({"value":"linear","text":"Linear","tooltip":"Linear scale"});
		typeItems.push({"value":"log","text":"Logarithmic","tooltip":"Logarithmic scale"});
		typeItems.push({"value":"pow","text":"Power","tooltip":"Power scale"});
//		typeItems.push({"value":"quantile","text":"Quantile","tooltip":"Quantile scale"});
//		typeItems.push({"value":"quantize","text":"Quantize","tooltip":"Quantize scale"});
		typeItems.push({"value":"sqrt","text":"Square Root","tooltip":"Square Root scale"});
//		typeItems.push({"value":"threshold","text":"Threshold","tooltip":"Threshold scale"});
		typeItems.push({"value":"time","text":"Time","tooltip":"Time scale"});
		typeItems.push({"value":"identity","text":"Identity","tooltip":"Identity scale"});
		typeItems.push({"value":"ordinal","text":"Ordinal","tooltip":"Ordinal scale"});
		if (chart.params.data.length > 0) {
			var axisType = chart.params.data[0][lname].type;
			for (var j = 0; j < typeItems.length; j++) {
				var titem = typeItems[j];
				if (axisType == titem["value"]) {
					titem["selected"] = true;
					break;
				}
			}
		} else
			typeItems[0]["selected"]=true;

		var typeSelect = SHIV.createInputGroupSelect("Type",typeItems,"Axis type","chart-dim-type-"+lname,chart.params.type != "heatmap-table");
		block.appendChild(typeSelect);
		var positionItems = [];
		positionItems.push({"value":"inside","text":"Inside","tooltip":"Axis label is inside the graph","selected":(chart.params.axis[lname].title.position == "inside")});
		positionItems.push({"value":"outside","text":"Outside","tooltip":"Axis label is outside the graph","selected":(chart.params.axis[lname].title.position == "outside")});
		var positionSelect = SHIV.createInputGroupSelect("Position",positionItems,"Title position","chart-dim-title-position-"+lname);
		block.appendChild(positionSelect);
		//block.appendChild(document.createElement('br'));
		block.appendChild(
			SHIV.createInputGroup(
				"Inverted",
				"",
				null,
				name+" axis inverted",
				"chart-dim-inverted-"+lname,
				"checkbox",
				true, // enabled
				chart.params.axis[lname].inverted
			)
		);
		//block.appendChild(xp);
		divPanelBody.appendChild(block);
	}

	//divPanelBody.appendChild(document.createElement('br'));
	divCreateNewVisualization.appendChild(divPanelBody);

	var buttons = new Array();
	var okButton = new Object();
	okButton['text'] = "Apply";
	okButton['onMouseUp'] = function(evt) { 
		var h3 = document.getElementById(chart.params.id+"-title");
		while (h3.childNodes.length > 0)
			h3.removeChild(h3.childNodes[0]);
		h3.appendChild(document.createTextNode(document.getElementById("chart-title").value));

		chart.params.axis.grid.enabled = document.getElementById("chart-grid").checked;

		// now, handle axis
		// track if any axis type changed
		var axisTypeChanged = false;
		
		for (var i = 0; i < chart.params.data.length; i++) {
			var dataParams = chart.params.data[i];
			if (chart.SupportsSize) {
				var elemSize = document.getElementById("chart-size").value;
				var sObject = dataParams.size;
				if (sObject !== undefined && sObject != null) {
					if (sObject.functor != elemSize) axisTypeChanged = true;
					sObject.functor = elemSize;
				} else {
					axisTypeChanged = true;
					dataParams.size = {
						'functor': elemSize
					}
				}
			}
			if (chart.SupportsColor) {
				var elemColour = document.getElementById("chart-colour").value;
				var elemSelColour = document.getElementById("chart-colour-selected").value;
				var sObject = dataParams.color;
				if (sObject !== undefined && sObject != null) {
					if (sObject.functor != elemColour) axisTypeChanged = true;
					if (sObject["selected-color"] != elemSelColour) axisTypeChanged = true;
					sObject.functor = elemColour;
					sObject["selected-color"] = elemSelColour;
				} else {
					axisTypeChanged = true;
					dataParams.color = {"functor":elemColour,"selected-color":elemSelColour};
				}
			}
			if (chart.SupportsColorRange) {
				var theme = document.getElementById("chart-range-theme").value;
				var card = document.getElementById("chart-range-theme-cardinality").value;
				var min = document.getElementById("chart-range-min").value;
				var max = document.getElementById("chart-range-max").value;
				var sObject = dataParams.color;
				if (sObject.range != theme ||
					sObject.cardinality != card ||
					sObject.min != min ||
					sObject.max != max) 
					axisTypeChanged = true;
				sObject.range = theme;
				sObject.cardinality = card;
				sObject.min = min;
				sObject.max = max;

			}
			if (chart.SupportsShape) {
				if (dataParams.shape != document.getElementById("chart-shape").value) axisTypeChanged = true;
				dataParams.shape = document.getElementById("chart-shape").value;
			}
		}



		for (var i = 0; i < chart.Dimensions; i++) {
			var dim = dims[i];
			chart.params.axis[dim].title.position = document.getElementById("chart-dim-title-position-"+dim).value;
			chart.params.axis[dim].title.label = document.getElementById("chart-dim-title-"+dim).value;
			for (var j = 0 ; j < chart.params.data.length; j++) {
				var dataParams = chart.params.data[j];
				var newIdx = parseInt(document.getElementById("chart-dim-source-"+dim).value);
				if (newIdx != dataParams[dim].idx) axisTypeChanged = true;
				dataParams[dim].idx = newIdx;
				var newType = document.getElementById("chart-dim-type-"+dim).value;
				if (newType != dataParams[dim].type) axisTypeChanged = true;
				dataParams[dim].type = newType;
			}
			var _inverted = document.getElementById("chart-dim-inverted-"+dim).checked;
			if (_inverted != chart.params.axis[dim].inverted) axisTypeChanged = true;
			chart.params.axis[dim].inverted = _inverted;
		}

	if (chart.SupportsRegression) {
			var chartRegressionType = chart.params.regressionType;
			var chartRegressionPolyDegree = chart.params.regressionPolynomialDegree;

			var newChartRegressionType = document.getElementById("chart-regression").value;
			var newChartRegressionPolyDegree = document.getElementById("chart-regression-degree").value;

			var executeRegression = false;

			if (chartRegressionType != newChartRegressionType) executeRegression == true;
			if (chartRegressionType == newChartRegressionType && 
				chartRegressionType == "polynomial" &&
				chartRegressionPolyDegree != newChartRegressionPolyDegree) executeRegression == true;

			chart.params.regressionType = newChartRegressionType;
			chart.params.regressionPolynomialDegree = newChartRegressionPolyDegree;

			if (executeRegression == true)
				SHIV.graph.executeRegression(chart);
		}

		if (axisTypeChanged) 
			chart.updateAxisType();
		if (chart.Dimensions >= 1) chart.updateAxisX();
		if (chart.Dimensions >= 2) chart.updateAxisY();
		if (chart.Dimensions >= 3) chart.updateAxisZ();
		chart.updateAxis();
		chart.updateData();

		return true;
	};
	okButton['className'] = "btn-default";
	buttons.push(okButton);
	var cancelButton = new Object();
	cancelButton['text'] = "Cancel";
	cancelButton['className'] = "btn-primary";
	buttons.push(cancelButton);
	SHIV.modal("Properties",divCreateNewVisualization,true,buttons,evt);
}

/**
 * Method used to create new charts.
 * @param params Chart parameters
 * @param type Chart type
 * @return Created chart
 */
SHIV.graph.createChart = function(params, type) {
	if (params === undefined || params == null) return null; // will not do an empty chart
	if (type === undefined || type == null) {
		if (params.type !== undefined && params.type != null)
			type = params.type;
		else {
			alert("Chart type can not be empty or null");
			return null;
		}
	}
	params = SHIV.graph.mergeParams(params);
	
	// we have enough metadata to continue
	var chartArea = document.getElementById('chart-area');
	if (chartArea == null) chartArea = document.body;
	var chartid = Date.now();
	params.id = "chart-"+chartid;
	params.bindto = "#"+params.id;
	var chartDiv = document.getElementById('chart-'+chartid);
	var chartPanel = null;
	var chart = null;
	if (chartDiv == null) {
		chartPanel = document.createElement('div');
		chartPanel.className = "panel panel-default resizable";
		chartPanel.style.minWidth = "450px";
		chartPanel.style.minHeight = "150px";
		chartPanel.style.width = "450px";
		chartPanel.style.position = "relative";
		//chartPanel.style.display = "table";
		var chartPanelHead = document.createElement('div');
		chartPanelHead.className = "panel-heading";
		SHIV.Drag.init(chartPanelHead,chartPanel);

		var chartCloseBtn = document.createElement('button');
		chartCloseBtn.id ='btn-close-chart-'+chartid
		chartCloseBtn.className = "close";
		chartCloseBtn.style.fontSize = "16px";
		chartCloseBtn.ariaLabel = "Close";
		chartCloseBtn.title = "Close Chart";
		chartCloseBtn.onmouseup = function(evt) {
			var elem = evt.target;
			var visid = null;
			if (elem.nodeName.toLowerCase() == "span") {
				visid = elem.parentNode.id.replace("btn-close-chart-","");
				elem = elem.parentNode.parentNode.parentNode;
			} else {
				visid = elem.id.replace("btn-close-chart-","");
				elem = elem.parentNode.parentNode;
			}
			var params = SHIV.graphManager.getGraphById("chart-"+visid);
			SHIV.graphManager.removeGraph(params);
			elem.parentNode.removeChild(elem);
		}
		var chartCloseBtnSpan = document.createElement('span');
		chartCloseBtnSpan.ariaHidden = "true";
		chartCloseBtnSpan.className = "glyphicon glyphicon-remove";
		//chartCloseBtnSpan.innerHTML ="&times;";
		chartCloseBtn.appendChild(chartCloseBtnSpan);
		chartPanelHead.appendChild(chartCloseBtn);

		var chartExportBtn = document.createElement('button');
		chartExportBtn.id = "btn-export-chart-"+chartid;
		chartExportBtn.className = "close";
		chartExportBtn.style.fontSize = "16px";
		chartExportBtn.ariaLabel = "Export";
		chartExportBtn.title = "Export Chart to PNG";
		chartExportBtn.onmouseup = function(evt) {
			//alert("export chart: "+visualization.id);
			var elem = evt.target;
			if (elem.nodeName.toLowerCase() == "span") {
				visid = elem.parentNode.id.replace("btn-export-chart-","");
				elem = elem.parentNode.parentNode.parentNode;
			} else {
				visid = elem.id.replace("btn-export-chart-","");
				elem = elem.parentNode.parentNode;
			}
			var params = SHIV.graphManager.getGraphById("chart-"+visid);
			saveSvgAsPng(params["svg-elem"],"chart.png");
		};

		var chartExportBtnSpan = document.createElement('span');
		chartExportBtnSpan.ariaHidden = "true";
		chartExportBtnSpan.className ="glyphicon glyphicon-export";
		chartExportBtn.appendChild(chartExportBtnSpan);
		chartPanelHead.appendChild(chartExportBtn);

		var chartPropsBtn = document.createElement('button');
		chartPropsBtn.id = "btn-props-chart-"+chartid;
		chartPropsBtn.className = "close";
		chartPropsBtn.style.fontSize = "16px";
		chartPropsBtn.ariaLabel = "Settings";
		chartPropsBtn.title = "Chart Settings";
		chartPropsBtn.onmouseup = function(evt) {
			var elem = evt.target;
			if (elem.nodeName.toLowerCase() == "span") {
				visid = elem.parentNode.id.replace("btn-props-chart-","");
				elem = elem.parentNode.parentNode.parentNode;
			} else {
				visid = elem.id.replace("btn-props-chart-","");
				elem = elem.parentNode.parentNode;
			}
			var params = SHIV.graphManager.getGraphById("chart-"+visid);
			SHIV.graph.showProperties(params.chart,evt);
			//alert("edit chart for visualization: "+visualization.id);
		};

		var chartPropsBtnSpan = document.createElement('span');
		chartPropsBtnSpan.ariaHidden = "true";
		chartPropsBtnSpan.className ="glyphicon glyphicon-cog";
		chartPropsBtn.appendChild(chartPropsBtnSpan);
		chartPanelHead.appendChild(chartPropsBtn);

		var chartResetBtn = document.createElement('button');
		chartResetBtn.id = "btn-reset-chart-"+chartid;
		chartResetBtn.className = "close";
		chartResetBtn.style.fontSize = "16px";
		chartResetBtn.ariaLabel = "Reset Chart";
		chartResetBtn.title = "Reset chart to initial state";
		chartResetBtn.onmouseup = function(evt) {
			var elem = evt.target;
			if (elem.nodeName.toLowerCase() == "span") {
				visid = elem.parentNode.id.replace("btn-reset-chart-","");
				elem = elem.parentNode.parentNode.parentNode;
			} else {
				visid = elem.id.replace("btn-reset-chart-","");
				elem = elem.parentNode.parentNode;
			}
			var params = SHIV.graphManager.getGraphById("chart-"+visid);
			params.chart.reset();
		};

		var chartResetBtnSpan = document.createElement('span');
		chartResetBtnSpan.ariaHidden = "true";
		chartResetBtnSpan.className ="glyphicon glyphicon-repeat";
		chartResetBtn.appendChild(chartResetBtnSpan);
		chartPanelHead.appendChild(chartResetBtn);

		var chartToggleBrushingBtn = document.createElement('button');
		chartToggleBrushingBtn.id = "btn-brushing-chart-"+chartid;
		chartToggleBrushingBtn.className = "close";
		chartToggleBrushingBtn.style.fontSize = "16px";
		chartToggleBrushingBtn.ariaLabel = "Toggle brushing";
		chartToggleBrushingBtn.title = "Enter Brushing mode, while in this mode the zoom/pan functionality can not be used";
		chartToggleBrushingBtn.onmouseup = function(evt) {
			var span = evt.target;
			if (span.nodeName.toLowerCase() == "span") {
				visid = span.parentNode.id.replace("btn-brushing-chart-","");
				elem = span.parentNode.parentNode.parentNode;
			} else {
				visid = span.id.replace("btn-brushing-chart-","");
				elem = span.parentNode.parentNode;
			}
			var params = SHIV.graphManager.getGraphById("chart-"+visid);
			var chart = params.chart;
			var btn = span.parentNode;
			if (span.className.indexOf("-move") > -1) {
				// we are in zoom+pan mode and need to switch to brushing mode
				span.className = "glyphicon glyphicon-pencil";
				btn.ariaLabel = "Toggle Zoom+Pan";
				btn.title = "Enter Zoom+Pan mode, while in this mode the brushing functionality can not be used";
				chart.detachZoom();
				chart.attachBrush();
			} else {
				// we are in brushing mode and need to switch to zoom+pan mode
				span.className = "glyphicon glyphicon-move";
				btn.ariaLabel = "Toggle Brushing";
				btn.title = "Enter Brushing mode, while in this mode the zoom/pan functionality can not be used"
				chart.detachBrush();
				chart.attachZoom();
			}
		};

		var chartToggleBrushingBtnSpan = document.createElement('span');
		chartToggleBrushingBtnSpan.ariaHidden = "true";
		chartToggleBrushingBtnSpan.className ="glyphicon glyphicon-move";
		chartToggleBrushingBtn.appendChild(chartToggleBrushingBtnSpan);
		chartPanelHead.appendChild(chartToggleBrushingBtn);

		var h3 = document.createElement('h3');
		h3.className = "panel-title";
		h3.id = "chart-"+chartid+"-title";
		h3.appendChild(document.createTextNode(params.title.label));
		chartPanelHead.appendChild(h3);
		chartPanel.appendChild(chartPanelHead);
		var chartPanelBody = document.createElement('div');
		chartPanelBody.className = "panel-body";
		chartDiv = document.createElement('div');
		chartDiv.id = 'chart-'+chartid;
		chartDiv.height = "400px";
		chartDiv.width = "400px";
		chartPanelBody.appendChild(chartDiv);
		var clearDiv = document.createElement('div');
		clearDiv.style.clear = "both";
		chartPanelBody.appendChild(clearDiv);
		chartPanel.appendChild(chartPanelBody)
		chartArea.appendChild(chartPanel);
	} else {
		// chart already exists..
		alert('Chart '+chartid+" already exists");
	}

	new ResizeSensor(chartPanel, function() {
		var svg = this.getElementsByTagName(chart.id);
/*
		if (svg.length != 1) return;
		svg = svg[0];
		var chartDiv = svg.parentNode;
		var cid = chartDiv.id.substring(6);
		var visualization = SHIV.visualizationManager.visualizations[cid];
*/
		var w = parseInt(document.defaultView.getComputedStyle(this).width, 10)-64;
		var h = parseInt(document.defaultView.getComputedStyle(this).height, 10)-74;
		chart.resizeViewport(w,h);
	});

	if (type == "scatterplot") {
		if (params.dimensionality == 2)
			chart = new SHIV.graph.ScatterPlot(params);
		else if (params.dimensionality == 3)
			chart = new SHIV.graph.ScatterPlot3D(params);
	}
	else if (type == "scatterplot2d") {
		chart = new SHIV.graph.ScatterPlot(params);
	}
	else if (type == "scatterplot3d") {
		chart = new SHIV.graph.ScatterPlot3D(params);
	}
	else if (type == "histogram") {
		chart = new SHIV.graph.Histogram(params);
	}
	else if (type == "linechart") {
		chart = new SHIV.graph.LineChart(params);
	}
	else if (type == "barchart") {
		chart = new SHIV.graph.BarChart(params);
	}
	else if (type == "heatmap" || type == "heatmap2d") {
		chart = new SHIV.graph.HeatMap(params);
	}
	else if (type == "heatmap-table") {
		// lets make sure we don't break
		var data0 = params.data[0];
		data0.x.type = "ordinal";
		data0.y.type = "ordinal";
		var rowsDesc = data0.x["rows-description"];
		var colsDesc = data0.y["cols-description"];
		if (rowsDesc === undefined || rowsDesc == null)
			data0.x["rows-description"] = rowsDesc = [];
		if (colsDesc === undefined || colsDesc == null)
			data0.y["cols-description"] = colsDesc = [];
		chart = new SHIV.graph.HeatMap(params);
	}
	else if (type == "blockchart") {
		chart = new SHIV.graph.BlockChart(params);
	}
	params.chart = chart;
	SHIV.graphManager.addGraph(params);
	return chart;
}

// Quadtree implementation

SHIV.geom.quadrant = function(parent) {
	this.parent = parent;
	this.children = [];
	this.centre = null;
	this.halfSize = null;
	this.points = [];
}

SHIV.geom.quadrant.prototype.clear = function() {
	this.points = [];
}

SHIV.geom.quadrant.prototype.childIndicies = function(p) {
	var res = 0;
	if (p[0] > this.centre[0]) res |= 1;
	if (p[1] > this.centre[1]) res |= 2;
	return res;
}

SHIV.geom.quadrant.prototype.isInside = function(p) {
	return ((p[0] <= this.centre[0] + this.halfSize[0]) &&
			(p[0] >= this.centre[0] - this.halfSize[0]) &&
			(p[1] <= this.centre[1] + this.halfSize[1]) &&
			(p[1] >= this.centre[1] - this.halfSize[1]));
}

SHIV.geom.quadrant.prototype.isLeaf = function() {
	return this.points != null && this.points.length > 0;
}

SHIV.geom.quadrant.prototype.minimum = function() {
	return [this.centre[0] - this.halfSize[0], this.centre[1] - this.halfSize[1]];
}

SHIV.geom.quadrant.prototype.maximum = function() {
	return [this.centre[0] + this.halfSize[0], this.centre[1] + this.halfSize[1]];
}

SHIV.geom.quadrant.prototype.extent = function(extent) {
	if (extent === undefined || extent == null) return [this.getMinimum(),this.getMaximum];
	var min = extent[0];
	var max = extent[1];
	this.centre = [(max[0]+min[0])*0.5,(max[1]+min[1])*0.5];
	this.halfSize = [(max[0]-min[0])*0.5,(max[1]-min[1])*0.5];
	return this;
}

SHIV.geom.quadrant.prototype.add = function(particle) {
	this.points.push(particle);
}


SHIV.geom.quadtree = function(points) {
	this.maxDepth = 28;
	this.currentDepth = 0;
	this.root = new SHIV.geom.quadrant(null);
	if (points === undefined || points == null) return this;
	// find extents
	var min = [Number.MAX_VALUE,Number.MAX_VALUE];
	var max = [Number.MIN_VALUE,Number.MIN_VALUE];
	for (var i = 0; i < points.length; i++) {
		var p = points[i];
		if (p[0] < min[0]) min[0] = p[0];
		if (p[1] < min[1]) min[1] = p[1];
		if (p[0] > max[0]) max[0] = p[0];
		if (p[1] > max[1]) max[1] = p[1];
	}
	// set extents
	this.root.extent([min,max]);
	for (var i = 0; i < points.length; i++)
		this.add(p);
	return this;
}

SHIV.geom.quadtree.prototype.extent = function(extent) {
	if (extent === undefined || extent == null) return this.extent;
	this.root.extent(extent);
	return this;
}

SHIV.geom.quadtree.prototype.insert = function(particle, quadrant, depth) {
	if (quadrant === undefined) {
		console.log("quadrant is invalid");
	}
	if ( depth > this.currentDepth) this.currentDepth = depth;
	if ( depth < this.maxDepth && quadrant.isInside(particle) ) {
		var res = quadrant.childIndicies(particle);
		var x = (res & 1) == 1 ? 1 : 0;
		var y = (res & 2) == 2 ? 1 : 0;

		if ((child = quadrant.children[ res ]) == null) {
			var child = quadrant.children[ res ] = new SHIV.geom.quadrant( quadrant );
			var quadrantMin = quadrant.minimum();
			var quadrantMax = quadrant.maximum();
			var min = [0,0];
			var max = [0,0];
			if ( x == 0 )
			{
				min[0] = quadrantMin[0];
				max[0] = ( quadrantMin[0] + quadrantMax[0] ) * 0.5;
			} 	
			else
			{
				min[0] = ( quadrantMin[0] + quadrantMax[0] ) * 0.5;
				max[0] = quadrantMax[0];
			}
			if ( y == 0 )
			{
				min[1] = quadrantMin[1];
				max[1] = ( quadrantMin[1] + quadrantMax[1] ) * 0.5;
			}
			else
			{
				min[1] = ( quadrantMin[1] + quadrantMax[1] ) * 0.5;
				max[1] = quadrantMax[1];
			}
			child.extent([min,max]);
		}
		this.insert(particle,child,++depth);
	} else {
		quadrant.add(particle);
	}
	return this;
}

SHIV.geom.quadtree.prototype.add = function(p) {
	this.insert(p,this.root,0);
	return this;
}

SHIV.geom.quadtree.prototype.quadrantVisibility = function(vc,vhs,quadrant) {
	var c = quadrant.centre;
	var hs = quadrant.halfSize;
	var distance = [vc[0]-c[0],vc[1]-c[1]];
	var sumHalfSizes = [vhs[0]+hs[0],vhs[1]+hs[1]];
	if ((Math.abs(distance[0]) + hs[0] <= vhs[0]) &
		(Math.abs(distance[1]) + hs[1] <= vhs[1]))
		return 2; // fully visible
	else if ((Math.abs( distance[0] ) <= sumHalfSizes[0] ) &
			 (Math.abs( distance[1] ) <= sumHalfSizes[1] ))
		return 1; // partly visible
	return 0; // not visible
}

SHIV.geom.quadtree.prototype.walkQuadrant = function(viewport,vc,vhs,quadrant,visible,cull) {
	var vis = this.quadrantVisibility(vc,vhs,quadrant);
	if (vis == 0) return; // nothing else to do
	if (vis == 2 || (vis == 1 && !cull)) {
		var pts = quadrant.points;
		for (var i = 0; i < pts.length; i++)
			visible.push(pts[i]);
	} else {
		var pts = quadrant.points;
		for (var i = 0; i < pts.length; i++) {
			var p = pts[i];
			if (p[0] > vc[0] + vhs[0]) continue;
			if (p[0] < vc[0] - vhs[0]) continue;
			if (p[1] > vc[1] + vhs[1]) continue;
			if (p[1] < vc[1] - vhs[1]) continue;
			visible.push(p);
		}
	}
	for (var i = 0; i < quadrant.children.length; i++) {
		var child = quadrant.children[i];
		if (child == null) continue;
		this.walkQuadrant(viewport,vc,vhs,child,visible,cull);
	}
}

SHIV.geom.quadtree.prototype.walk = function(viewport,cull) {
	if (viewport === undefined || viewport == null) viewport = [this.root.minimum(), this.root.maximum()];
	if (cull === undefined || cull == null) cull = true;
	var min = viewport[0];
	var max = viewport[1];
	var vc = [(max[0]+min[0])*0.5,(max[1]+min[1])*0.5];
	var vhs= [(max[0]-min[0])*0.5,(max[1]-min[1])*0.5];
	var visible = [];
	this.walkQuadrant(viewport,vc,vhs,this.root,visible,cull);
	return visible;
}

// Abstract functions for all graphs

/**
 * Updates the X axis of a given chart.
 * @param chart Chart which needs the update
 */
SHIV.graph.updateAxisX = function(chart) {
	
	chart.params.svg.selectAll(".x.axis").remove();
	chart.params.svg.selectAll(".tick").remove();
	chart.params.svg.selectAll(".x.grid").remove();

	if (chart.params.axis.x.enabled) {
		if (chart.params.axis.grid.enabled) {
			chart.params.axis.grid.xAxisGrid = chart.params.xAxisGrid = xAxisGrid = d3.svg.axis()
				.scale(chart.xScale)
				.tickSize(-(chart.params.height-(chart.params.margin.top+chart.params.margin.bottom)), 0, 0)
				.orient(chart.params.axis.x.position)
				.tickFormat("");
			chart.params.axis.grid.xelem = chart.params.auxArea.append("g")
				.attr("class","x grid")
				.attr("transform", "translate(0," + (chart.params.height-(chart.params.margin.top+chart.params.margin.bottom)) + ")")
				.call(chart.params.xAxisGrid);
		}
		chart.params.axis.x.xAxis = chart.params.xAxis = xAxis = d3.svg.axis()
			.scale(chart.xScale)
			.tickSize(chart.params.axis.x.tick.major.size)
			.orient(chart.params.axis.x.position);

		var tickFormat = chart.params.axis.x.tick.format;
		if (tickFormat == null) {
			if (chart.params.data[0] !== undefined && chart.params.data[0] != null) {
				var domain = chart.params.data[0].x.scale.domain();
				if (Math.abs(domain[1]-domain[0]) > 1000 || Math.abs(domain[1]-domain[0]) < 0.001)
					tickFormat = d3.format("s");
			}
		}
		chart.params.axis.x.elem = chart.params.auxArea.append("g")
			.attr("class", "x axis")
			.attr("transform", "translate(0," + (chart.params.height-(chart.params.margin.top+chart.params.margin.bottom)) + ")")
			.call(xAxis.tickFormat(tickFormat))
			.append("text")
				.attr("class", "label")
				.attr("x", chart.params.width-chart.params.margin.right-chart.params.margin.left)
				.attr("y", chart.params.axis.x.title.position == "inside" ? -6 : 6 + 22)
				.style("text-anchor", "end")
				.text(chart.params.axis.x.title.label != null ? chart.params.axis.x.title.label : "");
		if (chart.params.axis.x.tick.rotate != 0) {
			chart.params.svg.selectAll(".x.axis > .tick > text")	
				.style("text-anchor", "start")
				.attr("dx", ".8em")
				.attr("dy", ".15em")
				.attr("transform", "rotate("+chart.params.axis.x.tick.rotate+")" );
		}
		if (chart.params.axis.x.tick.minor.enabled) {
			chart.params.axis.x.xAxisMinor = chart.params.xAxisMinor = xAxisMinor = d3.svg.axis()
				.scale(chart.xScale)
				.tickSize(chart.params.axis.x.tick.minor.size)
				.ticks(chart.params.xAxis.ticks()*5)
				.orient(chart.params.axis.x.position)
				.tickFormat("");
			chart.params.axis.x.elemMinor = chart.params.auxArea.append("g")
				.attr("class", "x axis minor")
				.attr("transform", "translate(0," + (chart.params.height-(chart.params.margin.top+chart.params.margin.bottom)) + ")")
				.call(xAxisMinor);
		}
	}
}

/**
 * Updates the Y axis of a given chart.
 * @param chart Chart which needs the update
 */
SHIV.graph.updateAxisY = function(chart) {
	chart.params.svg.selectAll(".y.axis").remove();
	chart.params.svg.selectAll(".y.grid").remove();

	if (chart.params.axis.y.enabled) {
		if (chart.params.axis.grid.enabled){
			chart.params.axis.grid.yAxisGrid = chart.params.yAxisGrid = yAxisGrid = d3.svg.axis()
				.scale(chart.yScale)
				.tickSize(-(chart.params.width-(chart.params.margin.left+chart.params.margin.right)), 0, 0)
				.orient(chart.params.axis.y.position)
				.tickFormat("");
			chart.params.axis.grid.yelem = chart.params.auxArea.append("g")
				.attr("class","y grid")
				.call(chart.params.yAxisGrid);
		}
		chart.params.axis.y.yAxis = chart.params.yAxis = yAxis = d3.svg.axis()
			.scale(chart.yScale)
			.tickSize(chart.params.axis.y.tick.major.size)
			.orient(chart.params.axis.y.position);

		var tickFormat = chart.params.axis.y.tick.format;
		if (tickFormat == null) {
			if (chart.params.data[0] !== undefined && chart.params.data[0] != null) {
				var domain = chart.params.data[0].y.scale.domain();
				if (Math.abs(domain[1]-domain[0]) > 1000 || Math.abs(domain[1]-domain[0]) < 0.001)
					tickFormat = d3.format("s");
			}
		}
		chart.params.axis.y.elem = chart.params.auxArea.append("g")
				.attr("class", "y axis")
			.call(yAxis.tickFormat(tickFormat))
			.append("text")
				.attr("class", "label")
				.attr("transform", "rotate(-90)")
				.attr("y", chart.params.axis.y.title.position == "inside" ? 6 : -6 - 24)
				.attr("dy", ".71em")
				.style("text-anchor", "end")
				.text(chart.params.axis.y.title.label != null ? chart.params.axis.y.title.label : "");
		if (chart.params.axis.y.tick.rotate != 0) {
			chart.params.svg.selectAll(".y.axis > .tick > text")	
				.style("text-anchor", "start")
				.attr("dx", ".8em")
				.attr("dy", ".15em")
				.attr("transform", "rotate("+chart.params.axis.y.tick.rotate+")" );
		}
		if (chart.params.axis.y.tick.minor.enabled) {
			chart.params.axis.y.yAxisMinor = chart.params.yAxisMinor = yAxisMinor = d3.svg.axis()
				.scale(chart.yScale)
				.tickSize(chart.params.axis.y.tick.minor.size)
				.ticks(chart.params.yAxis.ticks()*6)
				.orient(chart.params.axis.y.position)
				.tickFormat("");
			chart.params.axis.y.elemMinor = chart.params.auxArea.append("g")
				.attr("class", "y axis minor")
				.call(chart.params.yAxisMinor);
		}
	}
}

/**
 * Resizes the view port of a given chart.
 * @param chart Chart which should be resized
 * @param width New chart width
 * @param height New chart height
 * @param maintainDomains Whether or not the domains should be maintained (default: false)
 */
SHIV.graph.resizeViewport = function(chart, width,height,maintainDomains) {
	if (maintainDomains === undefined || maintainDomains == null)
		maintainDomains = false; // domains are maintained by default
	// console.log("w="+width+", h="+height);
	if (!maintainDomains) {
		// domains are updated
		var xDiff = width / chart.params.width;
		var yDiff = height / chart.params.height;
		for (var i = 0; i < chart.params.data.length; i++) {
			var dataParams = chart.params.data[i];
			var xd = dataParams.x.scale.domain();
			var yd = dataParams.y.scale.domain();
			// console.log("old xDomain=["+xd[0]+", "+xd[1]+"], yDomain=["+yd[0]+", "+yd[1]+"]");
			if (dataParams.x.type != "ordinal") {
				xd[1]*=xDiff;
				dataParams.x.scale.domain(xd);
			}
			if (dataParams.y.type != "ordinal") {
				yd[1]*=yDiff;
				dataParams.y.scale.domain(yd);
			}
			// console.log("new xDomain=["+xd[0]+", "+xd[1]+"], yDomain=["+yd[0]+", "+yd[1]+"]");
		}
	}
	chart.params.width = width;
	chart.params.height = height;
	var chartWidth = chart.params.width - (chart.params.margin.left + chart.params.margin.right);
	var chartHeight = chart.params.height - (chart.params.margin.top + chart.params.margin.bottom);
	var xRange = [0, chartWidth];
	var yRange = [chartHeight,0];
	d3.select(chart.params['svg-elem'])
		.attr("width",width)
		.attr("height",height);
	for (var d = 0; d < chart.params.data.length; d++) {
		var dataParams = chart.params.data[d];
		if (dataParams.x.type != "ordinal")
			dataParams.x.scale.range(xRange);
		else
			dataParams.x.scale.rangeRoundBands([0,width-(chart.params.margin.left+chart.params.margin.right)],0.01);
		if (dataParams.y.type != "ordinal")
			dataParams.y.scale.range(yRange);
		else
			dataParams.y.scale.rangeRoundBands([height-(chart.params.margin.top+chart.params.margin.bottom),0],0.01);
	}
	// update clip area
	chart.params.svg.select("#clip-rect")
			.attr("width", chartWidth)
			.attr("height", chartHeight);
	// update zoom area
	chart.params.svg.select(".zoomarea").select("rect")
			.attr("width", chartWidth)
			.attr("height", chartHeight);
	if (chart.params.brushing.enabled) {
		chart.detachBrush();
		chart.attachBrush();
	}
	//this.updateAxis();
	if (chart.Dimensions >= 1) chart.updateAxisX();
	if (chart.Dimensions >= 2) chart.updateAxisY();
	if (chart.Dimensions >= 3) chart.updateAxisZ();
	if (chart.updateLegend != undefined) 
		chart.updateLegend();
	chart.updateData();
}

/**
 * Detaches the brush from a given chart.
 * @param chart Chart from which the brush should be detached
 */
SHIV.graph.detachBrush = function(chart) {
	chart.params.brushing.enabled = false;
	chart.params.brushing.elem.on(".brush",null);
	chart.params.brushing.elem.remove();
}

/**
 * Executes (or clears) a regression operation on the chart data.
 * @param chart Chart to execute the regression on
 */
SHIV.graph.executeRegression = function(chart) {
	if (chart === undefined || chart == null || chart.params === undefined) return;
	var type = chart.params.regressionType;
	// clear all previous lines
	chart.chartBody.selectAll(".regression").remove();
	if (type === undefined || type == null || type == "none") return; // nothing to do
	if (chart.Dimensions < 2) return; // can't execute a regression with less than 2 dimensions

	// TODO this a target for web workers
	// create regression data
	var cp = Date.now();
	for (var l = 0; l < chart.params.data.length; l++) {
		var regData = [];
		var dataParams = chart.params.data[l];
		var data = dataParams.data;
		for (var i = 0; i < data.length; i++) {
			var d = data[i];
			regData.push([d[dataParams.x.idx],d[dataParams.y.idx]]);
		}
		// sort by X
		regData.sort(function(a,b) { return a[0] - b[0]; });
		var regressionData = [];
		if (type != "polynomial")
			regressionData = regression(type, regData);
		else {
			var degree = chart.params.regressionPolynomialDegree;
			if (degree === undefined || degree == null || degree < 0) degree = 0;
			regressionData = regression(type, regData, degree);
		}
		// regressionData = {equation: [], points: [[x,y],[x,y]], string: "equation"}
		// create lines
		var line = d3.svg.line()
						.x(function(d) { return dataParams.x.scale(d[0]); })
						.y(function(d) { return dataParams.y.scale(d[1]); });
		var lines = chart.chartBody.append("path")
						.datum(regressionData.points)
						.attr("class","regression")
						.attr("d",line);
	}
	//console.log("executed and displayed "+type+" regression in "+(Date.now() - cp)+" ms");
}
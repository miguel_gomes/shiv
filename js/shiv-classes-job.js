/**
 * SHIV Job and JobManager representation.
 *
 * @author Miguel Gomes (alka.setzer@gmail.com)
 * @since 0.1
 * @depends shiv.js
 * 
 */

console.log("loading shiv-classes-job.js...");

/**
 * Job Manager constructor.
 */
SHIV.Classes.JobManager = function() {
	this.jobs = {}; // as a map
	this.jobsCount = 0;
}
/** Object type */
SHIV.Classes.JobManager.prototype.ObjectType = "JobManager";

/**
 * Method that resets this manager.
 */
SHIV.Classes.JobManager.prototype.reset = function () {
	this.jobs = {}; // as a map
	this.jobsCount = 0;
	var jobsTable = document.getElementById('tbl-jobs');
	if(jobsTable==null)
		return;
	jobsTable = jobsTable.getElementsByTagName('TBODY')[0];
	var cNode = jobsTable.cloneNode(false);
	jobsTable.parentNode.replaceChild(cNode ,jobsTable);
};

/**
 * Method used to show/hide information for a given Job.
 * @param job Job to which we want to toggle the tables, due to the way JS works this can also be an event
 */
SHIV.Classes.JobManager.prototype.toggleJobInfo = function(job)
{
	if (true) return;
	var baseTr = null;
	var expand = null;
	if (job === undefined || job == null || (job != null && job['ObjectType'] === undefined)) {
		// check if mouse event
		if (!(job != null && job['ObjectType'] === undefined))
			return;
		// a (target) > td > tr
		expand = job.target;
		baseTr = job.target.parentNode.parentNode;
		var id = baseTr.id.substring(8); // job.
		job = SHIV.jobManager.jobs[id];
	}
	var rowKey = "tables."+job.id.replace(" ","_");
	var aux = null;
	if ((aux = document.getElementById(rowKey)) != null) {
		if (aux.style.display == 'none') {
			expand.className = 'job-tables-a glyphicon glyphicon-chevron-up';
			aux.style.display = '';
		}
		else {
			expand.className = 'job-tables-a glyphicon glyphicon-chevron-down';
			aux.style.display = 'none';
		}
		return; // already there
	}
	// TODO change this to an image or something
	expand.className = 'job-tables-a glyphicon glyphicon-chevron-up';
	// tables row
	var tablesRow = document.createElement('tr');
	tablesRow.id = rowKey;
	tablesRow.className = "job-tables-row";
	var tablesCell = document.createElement('td');
	tablesCell.colSpan = 4;
	// tables table
	var jobTbls = document.createElement('table');
	jobTbls.className = "job-tables-tbl table table-hover";
	var thead = document.createElement('thead');
	var tr = document.createElement('tr');
	// id, name, description, RowCount(), ColumnCount()
	var th = document.createElement('th');
	th.appendChild(document.createTextNode('[+]'))
	th.className="id";
	tr.appendChild(th);
	th = document.createElement('th');
	th.appendChild(document.createTextNode('Name'))
	th.className="name";
	tr.appendChild(th);
	th = document.createElement('th');
	th.appendChild(document.createTextNode('#Rows'))
	tr.appendChild(th);
	th = document.createElement('th');
	th.appendChild(document.createTextNode('#Columns'))
	tr.appendChild(th);
	thead.appendChild(tr);
	jobTbls.appendChild(thead);
	var tbody = document.createElement('tbody');
	for (var i = 0; i < job.tables.length; i++) {
		var tbl = job.tables[i];
		var tr = document.createElement('tr');
		tr.id = "table."+tbl.id.replace(" ","_");
		var td = document.createElement('td');
		var a = document.createElement("a");
		a.className = "table-columns-a expand-ico";
		a.onclick = SHIV.tableManager.toggleColumnsForTable;
		td.appendChild(a);
		tr.appendChild(td);
		td = document.createElement('td');
		var span = document.createElement('span');
		if (tbl.description != null)
			span.title = tbl.description;
		span.appendChild(document.createTextNode(tbl.name));
		td.appendChild(span);
		tr.appendChild(td);
		td = document.createElement('td');
		td.appendChild(document.createTextNode(tbl.RowCount()));
		tr.appendChild(td);
		td = document.createElement('td');
		td.appendChild(document.createTextNode(tbl.ColumnCount()));
		tr.appendChild(td);
		tbody.appendChild(tr);
	}
	jobTbls.appendChild(tbody);
	tablesCell.appendChild(jobTbls);
	tablesRow.appendChild(tablesCell);
	baseTr.parentNode.insertBefore(tablesRow,baseTr.nextSibling);
}

/**
 * Method used to add a Job.
 * @param job Job to add
 */
SHIV.Classes.JobManager.prototype.addJob = function(job) {
	if (job == null || job === undefined || job.ObjectType != "Job") return;
	if (this.jobs[job.id] != null) return; // job already present
	this.jobs[job.id] = job;
	this.jobsCount++;
	// TODO remove this
	var rowKey = "job."+job.id.replace(" ","_");
	if (document.getElementById(rowKey) != null)
		return; // already added
	var jobsTable = document.getElementById('tbl-jobs');
	if(jobsTable==null)
		return;
	jobsTable = jobsTable.getElementsByTagName('TBODY')[0];
	var tr = document.createElement('tr');
	tr.id = rowKey;
	var td = document.createElement('td');
	var expand = document.createElement("span");
	expand.className = 'job-tables-a glyphicon glyphicon-chevron-down';
	expand.ariaHidden = true;
	expand.onclick = this.toggleJobInfo;
	td.appendChild(expand);
	tr.appendChild(td);
	td = document.createElement('td');
	var span = document.createElement('span');
	span.title = 'id: '+job.id;
	span.appendChild(document.createTextNode(job.name));
	td.appendChild(span);
	tr.appendChild(td);
	td = document.createElement('td');
	td.appendChild(document.createTextNode(job.phase));
	tr.appendChild(td);
	td = document.createElement('td');
	var atidx = job.owner.indexOf('@');
	if (atidx > 0) {
		var user = job.owner.substring(0,atidx);
		var domain = job.owner.substring(atidx+1);
		span = document.createElement('span');
		span.title = "Domain: "+domain;
		span.appendChild(document.createTextNode(user));
		td.appendChild(span);
	} else
		td.appendChild(document.createTextNode(job.owner));
	tr.appendChild(td);
	td = document.createElement('td');
	td.appendChild(document.createTextNode(SHIV.formatDate(job.startDate)));
	tr.appendChild(td);
	td = document.createElement('td');
	td.appendChild(document.createTextNode(SHIV.formatDate(job.endDate)));
	tr.appendChild(td);
	jobsTable.appendChild(tr);
}

/**
 * Method used to parse job from the server.
 * @param job JSON Object with job to parse
 */
SHIV.Classes.JobManager.prototype.parseJob = function(job)
{
	if (job == null || job === undefined) return null; // nothing to do
	//id,phase,href,run_id,owner,description,start,end
	var result = new SHIV.Classes.Job(job['id'],job['phase'],job['href'],job['run_id'],job['owner'],job['description'],job['start'],job['end']);
	return result;
}

/**
 * Method used to parse jobs from the server.
 * @param jobs JSON Object with jobs to parse
 */
SHIV.Classes.JobManager.prototype.parseJobs = function(jobs) {
	if (jobs === undefined || jobs == null) return; // nothing to do
	if (jobs['payload'] != null) { // we were given the message
		var blocks = jobs['payload'];
		for (var b = 0; b < blocks.length; b++) {
			var block = blocks[b];
			var data = block['data'];
			var aux = data['jobs'];
			for (var i = 0; i < aux.length; i++) {
				var job = this.parseJob(aux[i]);
				this.addJob(job);
			}
		}
	} else { // we were given the datsets array
		for (var i = 0; i < jobs.length; i++) {
			var job = this.parseJob(jobs[i]);
			this.addJob(job);
		}
	}
	console.log('We are now tracking '+this.jobsCount+" jobs");
}


/**
 * Callback used to update the local list of jobs from the server.
 */
SHIV.Classes.JobManager.prototype.updateJobs = function() {
	SHIV.server.executeCommand("jobs-list",this.updateJobsCallback);
}

/**
 * Callback used to update the local list of jobs from the server
 * @param jqXHR XML HTTP Request to parse
 * @param data Either the data the requested or an error
 */
SHIV.Classes.JobManager.prototype.updateJobsCallback = function(jqXHR,data) {
	if (jqXHR.status == 200) {
		SHIV.jobManager.parseJobs(data);
	}
}

// Job Related

/**
 * Job constructor.
 * @param id Job identifier
 * @param phase Job phase
 * @param href Job href
 * @param run_id Job name
 * @param owner Job owner
 * @param description Job description
 * @param start Job start time
 * @param end Job end time
 * @return new Job object
 */
SHIV.Classes.Job = function(id,phase,href,run_id,owner,description,start,end) {
	this.id = id;
	this.name = run_id !== undefined && run_id != null && run_id != "null" ? run_id : null;
	this.phase = phase !== undefined && phase != null && phase != "UNKNOWN" ? phase : null;
	this.description = description != null && description !== undefined && description != "null" ? description : null;
	this.href = href === undefined || href == null ? "" : href;
	this.owner = owner !== undefined && owner != null && owner != "Unknown" ? owner : null;
	this.startDate = null;
	this.endDate = null;
	if (start !== undefined && start != null) {
		var t = parseInt(start);
		if (!isNaN(t))
			this.startDate = new Date(t);
	}
	if (end !== undefined && end != null) {
		var t = parseInt(end);
		if (!isNaN(t))
			this.endDate = new Date(t);
	}
	this.quote = null; // will be date
	this.executionDuration = -1; // will be integer
	this.descruction = -1; // will be integer
	this.parameters = [];
	this.results = [];
	this.error = null; // will be object
}
/** Object type */
SHIV.Classes.Job.prototype.ObjectType = "Job";
/**
 * Method used to add a parameter to this Job.
 * @param byReference Is Parameter given by reference
 * @param id Parameter id
 * @param isPost Whether or not parameter is post
 */
SHIV.Classes.Job.prototype.addParameter = function(byReference,id,isPost) {
	
}
/**
 * Method used to add a result to this Job.
 * @param id Result id
 * @param href Result href
 * @param type Result type (if different from simple)
 */
SHIV.Classes.Job.prototype.addResult = function(id,href,type) {
	
}
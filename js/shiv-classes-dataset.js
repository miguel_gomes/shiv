/**
 * SHIV Dataset and DatasetManager representation.
 *
 * @author Miguel Gomes (alka.setzer@gmail.com)
 * @since 0.1
 * @depends shiv.js
 * 
 */

console.log("loading shiv-classes-dataset.js...");

/**
 * Dataset Manager constructor.
 */
SHIV.Classes.DatasetManager = function() {
	this.datasets = {}; // as a map
	this.datasetsCount = 0;
	this.supportedDataTypes = {};
	this.requests = {};
};
/** Object type */
SHIV.Classes.DatasetManager.prototype.ObjectType = "DatasetManager";

/**
 * Method that resets this manager.
 */
SHIV.Classes.DatasetManager.prototype.reset = function () {
	this.datasets = {}; // as a map
	this.datasetsCount = 0;
	var datasetsTable = document.getElementById('tbl-datasets');
	if(datasetsTable==null)
		return;
	datasetsTable = datasetsTable.getElementsByTagName('TBODY')[0];
	var cNode = datasetsTable.cloneNode(false);
	datasetsTable.parentNode.replaceChild(cNode ,datasetsTable);
	this.dataTypes = {};
	this.inputDataTypes = [];
	this.outputDataTypes = [];
};

/**
 * Method used to populate the list of supported data types.
 * Note async method.
 */
SHIV.Classes.DatasetManager.prototype.populateSupportedDataTypes = function() {
	SHIV.server.executeCommand("data-type-list",this.populateSupportedDataTypesCallback);
}

/**
 * Callback used to process server supported data-types.
 * @param jqXHR XHR result object
 * @param data Processed data or error if jqXHR.status == 0
 */
SHIV.Classes.DatasetManager.prototype.populateSupportedDataTypesCallback = function(jqXHR,data) {
	if (jqXHR.status == 200) {
		if (data === undefined || data == null) return; // nothing to do
		if (data['payload'] != null) { // we were given the message
			SHIV.datasetManager.supportedDataTypes = {}; // clear
			var blocks = data['payload'];
			for (var b = 0; b < blocks.length; b++) {
				var block = blocks[b];
				var payloadData = block['data'];
				var dataTypes = payloadData['data-types'];
				for (var i = 0; i < dataTypes.length; i++) {
					var dataType = dataTypes[i];
					SHIV.datasetManager.supportedDataTypes[dataType.name] = dataType;
				}
			}
		}
	}
}

/**
 * Gets a list of supported input data type names.
 * @return List of supported input data types
 */
SHIV.Classes.DatasetManager.prototype.getInputDataTypes = function() {
	var arr = [];
	for (var key in this.supportedDataTypes) {
		var dataType = this.supportedDataTypes[key];
		if (dataType.input) arr.push(key);
	}
	return arr;
}

/**
 * Gets a list of supported output data type names.
 * @return List of supported output data types
 */
SHIV.Classes.DatasetManager.prototype.getOutputDataTypes = function() {
	var arr = [];
	for (var key in this.supportedDataTypes) {
		var dataType = this.supportedDataTypes[key];
		if (dataType.output) arr.push(key);
	}
	return arr;
}

SHIV.Classes.DatasetManager.prototype.plotChart = function(table, type) {
	if (table === undefined || table == null) return;
	// todo show error
	var tableDesc = [];
	for (var i = 0; i < table.columns.length; i++) {
		var col = table.columns[i];
		tableDesc.push(col.name);
	}
	// first create an empty chart so that we can display stuff
	var params = {
		//'bindto': "#chart-"+visualization.id,
		'data': [
			{
				'source-id': table.id,
				'data': [], // empty array
				'data-description': tableDesc, // data description
				//'keyFunction': function(d) { return d[table.ColumnCount()]; },
				'source': {
					'idx': -1 // indicate that the row index is the idx
				},
				'x': {
					'idx': 0
				},
				'y': {
					'idx': 1
				},
				'z': {
					'idx': 2
				},
				'opacity': 0.75,
				'shape': 'square',
				'size': {
					'functor': 5
				},
				//'color': 'red'
			}
		],
		'axis': {
			'x': {
				'title': {
					'label': table.columns[0].name
				}
			},
			'y': {
				'title': {
					'label': table.columns[1].name
				}
			},
			'z': {
				'title': {
					'label': table.columns[2].name
				}
			}
		},
		'brushing': {
			'enabled': false
		},
		'zooming': {
			'enabled': true
		},
		'title': {
			'label': table.name
		},
		'dimensionality': table.ColumnCount(), // data dimensionality
		'type': type // chart type
	};
	var chart = SHIV.graph.createChart(params,type);
	//visualization.charts[chart.id] = chart;
	var reqId = Date.now();
	SHIV.datasetManager.requests[reqId] = {'chart':chart,'table':table};
	// need to fetch, we will only fetch level 0, we need the headers because we are going to lose track of the visualization..
	SHIV.server.executeCommand("table-items-get?tag="+reqId+"&dataset-id="+table.associatedDatasetId+"&table-id="+table.id+"&include-headers=false&items=0-"+table.RowCount(),
		SHIV.datasetManager.resolveTableItemsGet
	);
	return;
}

/**
 * Method used to process a Visualization Page request.
 * @param jqXHR JQuery XHR variable
 * @param data XHR data
 */
SHIV.Classes.DatasetManager.prototype.resolveTableItemsGet = function (jqXHR,data) {
	if (jqXHR.status != 200) {
		SHIV.modal("Error","Could not fetch table items");
		return;
	}
	// we got the data
	if (data === undefined || data == null) return; // nothing to do
	var tag = data['tag'];
	var request = SHIV.datasetManager.requests[tag];
	if (request == null) {
		console.log("No chart associated with request: "+tag);
		return;
	}
	var table = request.table;
	var chart = request.chart;
	var chartDiv = document.getElementById(chart.params.id);

	if (data['payload'] != null) { // we were given the message
		var start = Date.now();
		var blocks = data['payload'];
		var datasetData = null;
		for (var b = 0; b < blocks.length; b++) {
			var block = blocks[b];
			var data = block['data']; // block data
			var aux = data['data']; // actual data
			if (aux.length == 0) continue;
			var len = aux[0].length;
			if (datasetData == null)
				datasetData = aux;
			else {
				for (var i = 0; i < aux.length; i++)
					datasetData.push(aux[i]);
			}
		}
		// add all points in one go because it's faster than blocking while d3 processes the points
		var len = datasetData[0].length;
		if (chart.params.type == "heatmap-table") {
			// the table itself is the heatmap, so we are going to generate a new dataset for this table
			var aux = [];
			var ncols = datasetData[0].length;
			var nrows = datasetData.length;
			for (var r = 0; r < nrows; r++) {
				var row = datasetData[r];
				for (var c = 0; c < ncols; c++) {
					aux.push([r,c,row[c]]);
				}
			}
			// add to extra columns to the description
			var desc = chart.params.data[0]['data-description'];
			chart.params.margin.right = 90; // we need more space for the color legend
			chart.params.data[0]['x']['rows-description'] = desc;
			chart.params.data[0]['x'].type = "ordinal";
			chart.params.data[0]['y']['cols-description'] = desc;
			chart.params.data[0]['y'].type = "ordinal";
			chart.params.data[0]['color'] = { "idx": 2, "range": "custom", "min": "#FFFFF", "max": "#4682B4", "cardinality":"2" };
			chart.params.axis.x.title.label = "X";
			chart.params.axis.x.tick.rotate = 70;
			chart.params.axis.y.title.label = "Y";
			chart.params.axis.grid.enabled = false;
			chart.params.legend = {
				'enabled': true
			}
			chart.params.data[0]['data-description'] = ["X","Y","Value"];
			datasetData = aux;
		}
		var dataParams = [
		{
			'source-id': table.id,
			'data': datasetData,
				'source': {
					'idx': -1, // so that row is the identifier
				}
			}
		];
		chart.appendData(dataParams);
		delete SHIV.datasetManager.requests[tag];
		console.log("chart updated, process took: "+(Date.now()-start)+" ms");
	}
}

SHIV.Classes.DatasetManager.prototype.createChartUlElem = function(table) {
	var possibilities = [
		{'name':"2D Scatter plot",'type':"scatterplot2d",'dims-min':2},
		{'name':"3D Scatter plot",'type':"scatterplot3d",'dims-min':3},
		{'name':"Histogram",'type':"histogram",'dims-min':1},
		{'name':"Bar chart",'type':"barchart",'dims-min':2},
		{'name':"Line chart",'type':"linechart",'dims-min':2},
		{'name':"2D Heatmap",'type':"heatmap2d",'dims-min':2},
		{'name':"Table heatmap",'type':"heatmap-table",'dims-min':2},
		{'name':"Block chart",'type':"blockchart",'dims-min':2}
	];
	var ul = document.createElement('ul');
	ul.className = "dropdown-menu";
	ul.style.display = "none";
	for (var i = 0; i < possibilities.length; i++) {
		var chart = possibilities[i];
		if (table.ColumnCount() < chart["dims-min"]) continue;
		var li = document.createElement("li");
		var li_a = document.createElement("a");
		li_a.href = "#";
		li_a.className = "chart "+chart.type;
		li_a.onmouseup = function (evt) {
			var a = evt.target; 
			var li = a.parentNode;
			var ul = li.parentNode;
			ul.style.display = "none";
			var type = a.className.substring(6);
			var tableId = ul.parentNode.parentNode.parentNode.id;
			var fidx = tableId.indexOf('.')+1;
			var datasetId = tableId.substring(fidx,tableId.indexOf('.',fidx));
			tableId = tableId.substring(tableId.lastIndexOf('.')+1);
			var dataset = SHIV.datasetManager.datasets[datasetId];
			var table = null;
			for (var j = 0; j < dataset.tables.length; j++) {
				var t = dataset.tables[j];
				if (t.id == tableId) {
					table = t;
					break;
				}
			}
			if (table == null) return true;
			SHIV.datasetManager.plotChart(table,type);
		};
		li_a.appendChild(document.createTextNode(chart.name));
		li.appendChild(li_a);
		ul.appendChild(li);
	}
	return ul;
}

/**
 * Method used to show/hide tables for a given Dataset.
 * @param dataset Dataset to which we want to toggle the tables, due to the way JS works this can also be an event
 */
SHIV.Classes.DatasetManager.prototype.toggleTablesForDataset = function(dataset)
{
	var baseTr = null;
	var expand = null;
	if (dataset === undefined || dataset == null || (dataset != null && dataset['ObjectType'] === undefined)) {
		// check if mouse event
		if (!(dataset != null && dataset['ObjectType'] === undefined))
			return;
		// a (target) > td > tr
		expand = dataset.target;
		baseTr = dataset.target.parentNode.parentNode;
		var id = baseTr.id.substring(8); // dataset.
		dataset = SHIV.datasetManager.datasets[id];
	}
	var rowKey = "tables."+dataset.id.replace(" ","_");
	var aux = null;
	if ((aux = document.getElementById(rowKey)) != null) {
		if (aux.style.display == 'none') {
			expand.className = 'dataset-tables-a glyphicon glyphicon-chevron-up';
			aux.style.display = '';
		}
		else {
			expand.className = 'dataset-tables-a glyphicon glyphicon-chevron-down';
			aux.style.display = 'none';
		}
		return; // already there
	}
	// TODO change this to an image or something
	expand.className = 'dataset-tables-a glyphicon glyphicon-chevron-up';
	// tables row
	var tablesRow = document.createElement('tr');
	tablesRow.id = rowKey;
	tablesRow.className = "dataset-tables-row";
	var tablesCell = document.createElement('td');
	tablesCell.colSpan = 4;
	// tables table
	var datasetTbls = document.createElement('table');
	datasetTbls.className = "dataset-tables-tbl table table-hover";
	var thead = document.createElement('thead');
	var tr = document.createElement('tr');
	// id, name, description, RowCount(), ColumnCount()
	var th = document.createElement('th');
	th.appendChild(document.createTextNode(' '))
	th.className="id";
	tr.appendChild(th);
	th = document.createElement('th');
	th.appendChild(document.createTextNode('Name'))
	th.className="name";
	tr.appendChild(th);
	th = document.createElement('th');
	th.appendChild(document.createTextNode('#Rows'))
	tr.appendChild(th);
	th = document.createElement('th');
	th.appendChild(document.createTextNode('#Columns'))
	tr.appendChild(th);
	th = document.createElement('th');
	th.appendChild(document.createTextNode('Actions'))
	tr.appendChild(th);
	thead.appendChild(tr);
	datasetTbls.appendChild(thead);
	var tbody = document.createElement('tbody');
	for (var i = 0; i < dataset.tables.length; i++) {
		var tbl = dataset.tables[i];
		var tr = document.createElement('tr');
		tr.id = "table."+dataset.id.replace(" ","_")+"."+tbl.id.replace(" ","_");
		var td = document.createElement('td');
		var a = document.createElement("span");
		a.className = "table-columns-a glyphicon glyphicon-chevron-down";
		a.ariaHidden = true;
		a.onclick = SHIV.tableManager.toggleColumnsForTable;
		td.appendChild(a);
		tr.appendChild(td);
		td = document.createElement('td');
		var span = document.createElement('span');
		if (tbl.description != null)
			span.title = tbl.description;
		span.appendChild(document.createTextNode(tbl.name));
		td.appendChild(span);
		tr.appendChild(td);
		td = document.createElement('td');
		td.appendChild(document.createTextNode(tbl.RowCount()));
		tr.appendChild(td);
		td = document.createElement('td');
		td.appendChild(document.createTextNode(tbl.ColumnCount()));
		tr.appendChild(td);
		td = document.createElement('td');
		var distanceMatrixTable = document.createElement('span');
		distanceMatrixTable.className = 'glyphicon glyphicon-circle-arrow-down';
		distanceMatrixTable.id = "op_"+tbl.id;
		distanceMatrixTable.ariaHidden = "true";
		distanceMatrixTable.title = "Create distance matrix of table";
		distanceMatrixTable.style.cursor = 'pointer';
		distanceMatrixTable.onmouseup = function(evt) {
			var tblId = evt.target.id.substring(3);
			SHIV.server.executeCommand("table-distance-matrix?mode=async&source-table-id="+tblId,SHIV.datasetManager.handleOperationStart);
		}
		td.appendChild(distanceMatrixTable);
		var distanceMatrixTableTranspose = document.createElement('span');
		distanceMatrixTableTranspose.className = 'glyphicon glyphicon-circle-arrow-right';
		distanceMatrixTableTranspose.id = "op_"+tbl.id;
		distanceMatrixTableTranspose.ariaHidden = "true";
		distanceMatrixTableTranspose.title = "Create distance matrix of transposed table";
		distanceMatrixTableTranspose.style.cursor = 'pointer';
		distanceMatrixTableTranspose.onmouseup = function(evt) {
			var tblId = evt.target.id.substring(3);
			SHIV.server.executeCommand("table-distance-matrix?mode=async&transpose=true&source-table-id="+tblId,SHIV.datasetManager.handleOperationStart);
		}
		td.appendChild(distanceMatrixTableTranspose);
		var transposeTable = document.createElement('span');
		transposeTable.className = 'glyphicon glyphicon-retweet';
		transposeTable.id = "op_"+tbl.id;
		transposeTable.ariaHidden = "true";
		transposeTable.title = "Create transpose table";
		transposeTable.style.cursor = 'pointer';
		transposeTable.onmouseup = function(evt) {
			var tblId = evt.target.id.substring(3);
			SHIV.server.executeCommand("table-transpose?mode=async&source-table-id="+tblId,SHIV.datasetManager.handleOperationStart);
		}
		td.appendChild(transposeTable);
		var createVis = document.createElement('span');
		createVis.className = 'glyphicon glyphicon-new-window';
		createVis.id = "create_"+tbl.id;
		createVis.ariaHidden = "true";
		createVis.title = "Create visualization";
		createVis.style.cursor = 'pointer';
		createVis.onmouseup = function(evt) {
			var tblId = evt.target.id.substring(7);
			SHIV.visualizationManager.createNew(tblId);
		}
		td.appendChild(createVis);

		var showIconElem = document.createElement('span');
		showIconElem.className = "dropdown"
		var showIcon = document.createElement("span");
		showIcon.className = 'glyphicon glyphicon-modal-window';
		showIcon.ariaHidden = "true";
		showIcon.title = "Create chart";
		//showIcon.style.cursor = 'pointer';
		showIconElem.appendChild(showIcon);
		var caret = document.createElement('span');
		caret.className = "glyphicon glyphicon-triangle-bottom";
		caret.style.cursor = "pointer";
		caret.onmouseup = function(evt) {
			var caret = evt.target;
			var pElem = caret.parentNode;
			var ul = pElem.getElementsByTagName("UL")[0];
			if (ul.style.display == "none") {
				ul.style.display = "block";
			//	caret.className = "glyphicon glyphicon-triangle-top";
			} else {
				ul.style.display = "none";
			//	caret.className = "glyphicon glyphicon-triangle-bottom";
			}
		}
		showIconElem.appendChild(caret);
		
		showIconElem.appendChild(SHIV.datasetManager.createChartUlElem(tbl));
		
		//showIcon.onmouseup = this.plotVisualization;
		td.appendChild(showIconElem);

		tr.appendChild(td);
		tbody.appendChild(tr);
	}
	datasetTbls.appendChild(tbody);
	tablesCell.appendChild(datasetTbls);
	tablesRow.appendChild(tablesCell);
	baseTr.parentNode.insertBefore(tablesRow,baseTr.nextSibling);
}

/**
 * Method used to handle the reply of any table operation command
 * @param jqXHR JQuery XHR response
 * @param data Parsed data
 */
SHIV.Classes.DatasetManager.prototype.handleOperationStart = function(jqXHR, data) {
	// only two things could have happen that are positive
	SHIV.jobManager.updateJobs(); // either will be executed or queued for execution upon connect

	if (jqXHR.status != 200 && jqXHR.status != 202) {
		var msg = "";
		if (data != null) {
			msg += " - "+data['status-msg'];
		}
		SHIV.modal("Error","Could not execute operation "+msg);
		return;
	}
	if (jqXHR.status == 200) {
		SHIV.datasetManager.updateDatasets(); // update list of datasets...
		SHIV.showMessage("info","Operation executed successfully",null,"Success");
	} else { // job is ongoing
		var jobid = data["job-id"];
		SHIV.showMessage("info","Operation execution in progress...",null,"Information");
		// check status every 1second
		setTimeout(function(){
			SHIV.server.executeCommand("job-info?job-id="+jobid,SHIV.datasetManager.checkOperationStatus);
		},1000);
	}
}

SHIV.Classes.DatasetManager.prototype.checkOperationStatus = function(jqXHR, data) {
	if (jqXHR.status != 200) {
		var msg = "";
		if (data != null) {
			msg += " - "+data['status-msg'];
		}
		SHIV.modal("Error","Could not check status of Job "+msg);
		return;
	}
	var blocks = data['payload'];
	if (blocks == null || blocks.length == 0) {
		SHIV.modal("Error","Could not check status of Job - No data");
		return;
	}
	var block = blocks[0];
	var data = block['data'].job;
	var jobid = data['jobId'];
	var phase = data['phase'];
	if (phase == "COMPLETE" || phase == "COMPLETED") {
		SHIV.datasetManager.updateDatasets(); // update list of datasets...
		SHIV.jobManager.updateJobs();
		SHIV.showMessage("info","Operation executed successfully",null,"Success");
	} else if (phase == "EXECUTING" || phase == "RUNNING") {
		setTimeout(function(){
			SHIV.server.executeCommand("job-info?job-id="+jobid,SHIV.datasetManager.checkOperationStatus);
		},1000);
	} else if (phase == "ERROR") {
		SHIV.jobManager.updateJobs();
		SHIV.showMessage("error","Operation failed to execute.",null,"ERROR");
	} else {
		console.log("Phase: "+phase+", for job: "+jobid);
	}
}

/**
 * Method used to add a DataSet.
 * @param dataset Dataset to add
 */
SHIV.Classes.DatasetManager.prototype.addDataset = function(dataset) {
	if (dataset == null || dataset === undefined || dataset.ObjectType != "Dataset") return;
	if (this.datasets[dataset.id] != null) return; // dataset already present
	this.datasets[dataset.id] = dataset;
	this.datasetsCount++;
	// TODO remove this
	var rowKey = "dataset."+dataset.id.replace(" ","_");
	if (document.getElementById(rowKey) != null)
		return; // already added
	var datasetsTable = document.getElementById('tbl-datasets');
	if(datasetsTable==null)
		return;
	datasetsTable = datasetsTable.getElementsByTagName('TBODY')[0];
	var tr = document.createElement('tr');
	tr.id = rowKey;
	var td = document.createElement('td');
	var expand = document.createElement("span");
	expand.className = 'dataset-tables-a glyphicon glyphicon-chevron-down';
	expand.ariaHidden = true;
	expand.onclick = this.toggleTablesForDataset;
	td.appendChild(expand);
	tr.appendChild(td);
	td = document.createElement('td');
	var span = document.createElement('span');
	span.title = 'id: '+dataset.id;
	span.appendChild(document.createTextNode(dataset.name));
	td.appendChild(span);
	tr.appendChild(td);
	td = document.createElement('td');
	var desc = document.createElement('span');
	if (dataset.description != null)
		desc.title = dataset.description;
	desc.appendChild(document.createTextNode(dataset.title));
	td.appendChild(desc);
	tr.appendChild(td);
	td = document.createElement('td');
	td.appendChild(document.createTextNode(dataset.TablesCount()));
	tr.appendChild(td);
	td = document.createElement('td');
	var downloadDataset = document.createElement('span');
	downloadDataset.className = 'glyphicon glyphicon-save-file';
	downloadDataset.ariaHidden = "true";
	downloadDataset.title = "Save file";
	downloadDataset.style.cursor = 'pointer';
	downloadDataset.onmouseup = function(evt) {
	}
	td.appendChild(downloadDataset);
	var deleteDataset = document.createElement('span');
	deleteDataset.className = 'glyphicon glyphicon-remove';
	deleteDataset.ariaHidden = "true";
	deleteDataset.title = "Delete Dataset";
	deleteDataset.style.cursor = 'pointer';
	deleteDataset.onmouseup = function(evt) {
		var buttons = [];
		var closeButton = new Object();
		closeButton['text'] = "No";
		closeButton['className'] = "btn-primary";
		var acceptButton = new Object();
		acceptButton['text'] = "Yes";
		acceptButton['className'] = "btn-default";
		acceptButton['onMouseUp'] = function(evt) {
			SHIV.server.executeCommand("dataset-unload?id="+dataset.id,function(jqXHR,data) {
				if (jqXHR.status == 200) {
					tr.parentNode.removeChild(tr);
				} else {
					var msg = "";
					if (data != null)
						msg = " - "+data['status-code'];
					SHIV.showMessage("error","Could not delete Dataset"+msg,null,"Error");
				}
			});
			return true;
		};
		buttons.push(acceptButton);
		buttons.push(closeButton);
		SHIV.modal("Delete Dataset?","Do you really wish to delete this Dataset?",false,buttons);
	}
	td.appendChild(deleteDataset);
	tr.appendChild(td);
	datasetsTable.appendChild(tr);
}

/**
 * Method used to parse dataset from the server.
 * @param dataset JSON Object with dataset to parse
 */
SHIV.Classes.DatasetManager.prototype.parseDataset = function(dataset)
{
	if (dataset == null || dataset === undefined) return null; // nothing to do
	var result = new SHIV.Classes.DataSet(dataset['id'],dataset['name'],dataset['title'],dataset['description'],dataset['type'],dataset['tables-cnt']);
	if (dataset['tables'] !== undefined && dataset['tables'] != null) {
		result.tables = SHIV.tableManager.parseTables(dataset['tables']);
		if (result.tablesCnt != result.tables.length)
			result.tablesCnt = result.tables.length;
	}
	return result;
}

/**
 * Method used to parse datasets from the server.
 * @param datasets JSON Object with datasets to parse
 */
SHIV.Classes.DatasetManager.prototype.parseDatasets = function(datasets) {
	if (datasets === undefined || datasets == null) return; // nothing to do
	if (datasets['payload'] != null) { // we were given the message
		var blocks = datasets['payload'];
		for (var b = 0; b < blocks.length; b++) {
			var block = blocks[b];
			var data = block['data'];
			var aux = data['datasets'];
			for (var i = 0; i < aux.length; i++) {
				var dataset = this.parseDataset(aux[i]);
				this.addDataset(dataset);
			}
		}
	} else { // we were given the datsets array
		for (var i = 0; i < datasets.length; i++) {
			var dataset = this.parseDataset(datasets[i]);
			this.addDataset(dataset);
		}
	}
	console.log('We are now tracking '+this.datasetsCount+" datasets");
}


/**
 * Callback used to update the local list of datasets/tables from the server.
 */
SHIV.Classes.DatasetManager.prototype.updateDatasets = function() {
	SHIV.server.executeCommand("datasets-list?list-tables=true&list-columns=true",this.updateDatasetsCallback);
}

/**
 * Callback used to update the local list of datasets/tables from the server
 * @param jqXHR XML HTTP Request to parse
 * @param data Either the data the requested or an error
 */
SHIV.Classes.DatasetManager.prototype.updateDatasetsCallback = function(jqXHR,data) {
	if (jqXHR.status == 200) {
		SHIV.datasetManager.reset();
		SHIV.datasetManager.parseDatasets(data);
	}
}

SHIV.Classes.DatasetManager.prototype.uploadNew = function() {
	var divCreateNewVisualization = document.createElement('div');
	divCreateNewVisualization.className = "panel panel-default";
	var divPanelHeading = document.createElement('div');
	divPanelHeading.className = "panel-heading";
	var h3 = document.createElement('h3');
	h3.className = "panel-title";
	h3.appendChild(document.createTextNode("Add new Dataset"));
	divPanelHeading.appendChild(h3);
	divCreateNewVisualization.appendChild(divPanelHeading);
	var divPanelBody = document.createElement('div');
	divPanelBody.className = "panel-body";

	// error display integrated but hidden by default
	var divAlert = document.createElement('div');
	divAlert.className = "alert alert-danger hidden";
	divAlert.role = "alert";
	divAlert.id = "dataset-add-error";
	var divAlertCloseBtn = document.createElement('button');
	divAlertCloseBtn.className = "close";
	divAlertCloseBtn.ariaLabel = "Close";
	divAlertCloseBtn.onmouseup = function(evt) {
		var elem = evt.target;
		if (elem.nodeName.toLowerCase() == "span")
			elem = elem.parentNode.parentNode;
		else
			elem = elem.parentNode;
		var cn = elem.className;
		if (cn.indexOf(" show" > 0))
			elem.className = cn.replace(" show"," hidden");
	}
	var divAlertCloseBtnSpan = document.createElement('span');
	divAlertCloseBtnSpan.ariaHidden = "true";
	divAlertCloseBtnSpan.innerHTML ="&times;";
	divAlertCloseBtn.appendChild(divAlertCloseBtnSpan);
	divAlert.appendChild(divAlertCloseBtn);
	var divAlertSpanWarning = document.createElement('span');
	divAlertSpanWarning.className = "glyphicon glyphicon-exclamation-sign";
	divAlertSpanWarning.ariaHidden = "true";
	divAlert.appendChild(divAlertSpanWarning);
	var divAlertSpanSR = document.createElement('span');
	divAlertSpanSR.className = "sr-only";
	divAlertSpanSR.appendChild(document.createTextNode("Error:"));
	divAlert.appendChild(divAlertSpanSR);
	var divAlertMsg = document.createElement('span');
	divAlertMsg.id = "dataset-add-error-msg";
	divAlert.appendChild(divAlertMsg);
	divPanelBody.appendChild(divAlert);

	divPanelBody.appendChild(
		SHIV.createInputGroup(
			'Location',
			"Location",
			null,
			"Location of the Dataset to add",
			"dataset-add-location"
		)
	);

	// input type
	var supportedTypes = [];
	for (var key in this.supportedDataTypes) {
		var dataType = this.supportedDataTypes[key];
		if (key.toLowerCase() == "unknown") continue; // this is the same as automatic
		supportedTypes.push({"value":dataType.name.toLowerCase(),"text":dataType.name,"tooltip":dataType.description});
	}
	if (supportedTypes.length == 0)
		supportedTypes.push({"value":"automatic","text":"Automatic","tooltip":"Infer format from data"});
	supportedTypes[0]["selected"] = true;
	var typesSelect = SHIV.createInputGroupSelect("Format",supportedTypes,"Data format","dataset-add-format");
	divPanelBody.appendChild(typesSelect);

	//divPanelBody.appendChild(document.createElement('br'));
	divCreateNewVisualization.appendChild(divPanelBody);

	var buttons = new Array();
	var okButton = new Object();
	okButton['text'] = "Add";
	okButton['onMouseUp'] = function(evt) { 
		var flocation = document.getElementById('dataset-add-location').value.trim();
		var fmt = document.getElementById('dataset-add-format').value.trim();
		if (flocation.length == 0) return true;

		SHIV.server.executeCommand("dataset-load?mode=async&format="+fmt+"&location="+flocation,SHIV.datasetManager.handleDatasetAdd);

		return true;
	};
	okButton['className'] = "btn-default";
	buttons.push(okButton);
	var cancelButton = new Object();
	cancelButton['text'] = "Cancel";
	cancelButton['className'] = "btn-primary";
	buttons.push(cancelButton);
	SHIV.modal("Dataset",divCreateNewVisualization,false,buttons);
}

/**
 * Method used to handle the reply of the Dataset Load command
 * @param jqXHR JQuery XHR response
 * @param data Parsed data
 */
SHIV.Classes.DatasetManager.prototype.handleDatasetAdd = function(jqXHR,data) {
	// only two things could have happen that are positive
	SHIV.jobManager.updateJobs(); // either will be executed or queued for execution upon connect

	if (jqXHR.status != 200 && jqXHR.status != 202) {
		var msg = "";
		if (data != null) {
			msg += " - "+data['status-msg'];
		}
		SHIV.modal("Error","Could not load Dataset "+msg);
		return;
	}
	if (jqXHR.status == 200) {
		this.updateDatasets(); // update list of visualizations...
		var dataset = data["dataset-id"];
		SHIV.showMessage("info","Dataset loaded",null,"Success");
	} else { // job is ongoing
		var visid = data["dataset-id"];
		var jobid = data["job-id"];
		SHIV.showMessage("info","Dataset loading in progress...",null,"Information");
		// check status every 1second
		setTimeout(function(){
			SHIV.server.executeCommand("job-info?job-id="+jobid,SHIV.datasetManager.checkDatasetLoadStatus);
		},1000);
	}
}

SHIV.Classes.DatasetManager.prototype.checkDatasetLoadStatus = function(jqXHR,data) {
	if (jqXHR.status != 200) {
		var msg = "";
		if (data != null) {
			msg += " - "+data['status-msg'];
		}
		SHIV.modal("Error","Could not check status of Job "+msg);
		return;
	}
	var blocks = data['payload'];
	if (blocks == null || blocks.length == 0) {
		SHIV.modal("Error","Could not check status of Job - No data");
		return;
	}
	var block = blocks[0];
	var data = block['data'].job;
	var jobid = data['jobId'];
	var phase = data['phase'];
	if (phase == "COMPLETE" || phase == "COMPLETED") {
		SHIV.datasetManager.updateDatasets(); // update list of visualizations...
		SHIV.jobManager.updateJobs();
		SHIV.showMessage("info","Dataset loaded",null,"Success");
	} else if (phase == "EXECUTING" || phase == "RUNNING") {
		setTimeout(function(){
			SHIV.server.executeCommand("job-info?job-id="+jobid,SHIV.datasetManager.checkDatasetLoadStatus);
		},1000);
	} else if (phase == "ERROR") {
		SHIV.jobManager.updateJobs();
		SHIV.showMessage("error","Dataset failed to load.",null,"ERROR");
	} else {
		console.log("Phase: "+phase+", for job: "+jobid);
	}
}


// DATASET Related

/**
 * DataSet constructor.
 * @param id Dataset identifier
 * @param name Dataset name
 * @param title Dataset title
 * @param description Dataset description
 * @param type Dataset type
 * @param tablesCnt Number of tables the dataset is known to have
 * @return new Dataset object
 */
SHIV.Classes.DataSet = function(id,name,title,description,type,tablesCnt) {
	this.id = id;
	this.name = name !== undefined && name != null &&  name != "null" ? name : null;
	this.title = title !== undefined && title != null && title != "null" ? title : null;
	this.description = description != null && description !== undefined && description != "null" ? description : null;
	this.type = type === undefined || type == null ? "Unknown" : type;
	this.tablesCnt = tablesCnt != null && tablesCnt !== undefined ? parseInt(tablesCnt,10) : 0;
	this.tables = [];
}
/** Object type */
SHIV.Classes.DataSet.prototype.ObjectType = "Dataset";
/**
 * Method used to add a table to this dataset
 * @param table Table to add
 */
SHIV.Classes.DataSet.prototype.addTable = function(table) {
	if (table == null || table === undefined || table.ObjectType != "Table") return;
	this.tables.push(table);
}
/**
 * Method used to return the number of known/associated tables.
 * @return Number of known tables (if none is associated) or number of associated tables
 */
SHIV.Classes.DataSet.prototype.TablesCount = function() {
	if (this.tables.length == 0) return this.tablesCnt;
	else return this.tables.length;
}

/**
 * SHIV graphing methods.
 *
 * @author Miguel Gomes (alka.setzer@gmail.com)
 * @depends d3.js (>= 3.5)
 * @depends shiv-graphing.js (>= 0.1)
 * @since 0.1
 */

console.log("loading shiv-graphing-scatterplot3d.js...");

/**
 * Creates a scatter plot.
 * @param data Data to render (array of arrays, where each row is a new data point)
 * @param params Plot parameters
 * @return Scatter plot
 */
SHIV.graph.ScatterPlot3D = function(params) {
	var chart = this.chart = this; // recursive link
	if (params === undefined || params == null)
		params = SHIV.graph.defaultParams();
	else {
		params = SHIV.graph.mergeParams(params);
	}
	this.params = params;
	if (params.axis.z === undefined || params.axis.z == null) {
		params.axis.z = {
					'enabled': true,
					'title': {
						'label': "Z Axis"
					},
					'tick': {
						'rotate': 0
					}
				};
	}

	params.chart = this;
	SHIV.graphManager.addGraph(params);
	var xScale = this.xScale = null;
	var yScale = this.yScale = null;
	var grid = null;
	var paramsMap = {}; // parameters map

	// then we need to create the graph

	if ( ! Detector.webgl ) Detector.addGetWebGLMessage();
	
	// init stuff (will also call updateAxisX,Y and Z)
	this.init();
	this.animate();

	// this will create all the necessary scales
	this.updateAxisType(paramsMap);

	this.updateData();
}

SHIV.graph.ScatterPlot3D.prototype.ObjectType = "ScatterPlot3D";
SHIV.graph.ScatterPlot3D.prototype.ObjectClassName = "point";
SHIV.graph.ScatterPlot3D.prototype.Dimensions = 3;
SHIV.graph.ScatterPlot3D.prototype.SupportsSize = true;
SHIV.graph.ScatterPlot3D.prototype.SupportsColor = true;
SHIV.graph.ScatterPlot3D.prototype.SupportsShape = true;

SHIV.graph.ScatterPlot3D.prototype.init = function() {
	console.log("init()");
	this.params.three = {}
	var camera = this.params.three.camera = new THREE.PerspectiveCamera( 75, this.params.width / this.params.height, 1, 1000000 );
	// will need to make this automatic
	camera.position.z = 200;

	this.initScene();

	// create geometry
	// todo: this has a limit of 65K points (unsigned short), so in the future we need to update to multiple geometry lists
	this.params.three.sprite = THREE.ImageUtils.loadTexture( "textures/sprites/disc.png" );

	var renderer = this.params.three.renderer = new THREE.WebGLRenderer();
	renderer.setClearColor(0xffffff, 1);
	renderer.setPixelRatio( window.devicePixelRatio );
	renderer.setSize( this.params.width, this.params.height);
	var canvas = this.canvas = d3.select(this.params.bindto == null ? 'body' : this.params.bindto);
	var canvasElem = this.canvasElem = d3.select(this.params.bindto == null ? 'body' : this.params.bindto);
	this.params['canvas-elem'] = this.canvasElem[0][0];
	this.params['canvas-elem'].appendChild(renderer.domElement);

	this.params.three.controls = new THREE.OrbitControls(camera, document, renderer.domElement);
}

SHIV.graph.ScatterPlot3D.prototype.initScene = function() {
	console.log("initScene()");
	this.params.three.scene = new THREE.Scene();
	this.updateAxisX();
	this.updateAxisY();
	this.updateAxisZ();
}

SHIV.graph.ScatterPlot3D.prototype.animate = function(chart) {
	//if (!SHIV.RenderState) return;
//	console.log("animate()");
	if (this.animate !== undefined) {
		requestAnimationFrame( this.animate );
		
		this.params.three.renderer.render( this.params.three.scene, this.params.three.camera );
		this.params.three.controls.update();
	}
	else { // lost context due to async nature of the call
		// so lets get it back..
		for (var i = 0; i < SHIV.graphManager.graphs.length; i++) {
			var params = SHIV.graphManager.graphs[i]; 
			var chart = params.chart;
			if (chart.ObjectType != "ScatterPlot3D") continue;
			requestAnimationFrame( chart.animate );
		
			params.three.renderer.render( params.three.scene, params.three.camera );
			params.three.controls.update();
		}
	}

	//stats.update();

}

SHIV.graph.ScatterPlot3D.prototype.updateAxisType = function(paramsMap) {
	console.log("updateAxisType()");
	var params = this.params;
	var xInverted = params.axis.x.inverted;
	var yInverted = params.axis.y.inverted;
	var zInverted = this.params.axis.z.inverted;
	if (params.data !== undefined && params.data != null) {
		for (var i = 0; i < params.data.length; i++) {
			var dataset = params.data[i].data;
			var data = params.data[i];

			if (paramsMap !== undefined && paramsMap != null && data['source-id'] != null)
				paramsMap[data['source-id']] = data;

			// first we need to create the scales
			if (data.x.scale == null) 
				xScale = SHIV.graph.createScale(data.x.type);
			else {
				var _xScale = SHIV.graph.createScale(data.x.type);
				if (""+_xScale != ""+xScale)
					xScale = _xScale;
				else
					xScale = data.x.scale;
			}
			if (data.y.scale == null) 
				yScale = SHIV.graph.createScale(data.y.type);
			else {
				var _yScale = SHIV.graph.createScale(data.y.type);
				if (""+_yScale != ""+yScale)
					yScale = _yScale;
				else
					yScale = data.y.scale;
			}
			if (data.z.scale == null) 
				zScale = SHIV.graph.createScale(data.z.type);
			else {
				var _zScale = SHIV.graph.createScale(data.z.type);
				if (""+_zScale != ""+zScale)
					zScale = _zScale;
				else
					zScale = data.z.scale;
			}
			// then figure out the domains
			if (data.x.domain == null) {
				var dfunctor = data.x.domainFunctor;
				if (dfunctor == null)
					dfunctor = function(d) { 
						return (xInverted ? -1.0 : 1.0) * d[data.x.idx]; 
					}
				xScale.domain(d3.extent(dataset,dfunctor));
				data.x.domain = xScale.domain();
			}
			else 
			{
				var dfunctor = data.x.domainFunctor;
				if (dfunctor == null)
					dfunctor = function(d) { 
						return (xInverted ? -1.0 : 1.0) * d[data.x.idx]; 
					}
				var newDomain = d3.extent(dataset,dfunctor);
				var oldDomain = data.x.domain;
				if (newDomain[0] != oldDomain[0] || newDomain[1] != oldDomain[1])
					xScale.domain(newDomain);
				else
					xScale.domain(oldDomain);
			}
			if (data.y.domain == null) {
				var dfunctor = data.y.domainFunctor;
				if (dfunctor == null)
					dfunctor = function(d) { 
						return (yInverted ? -1.0 : 1.0) * d[data.y.idx]; 
					}
				yScale.domain(d3.extent(dataset,dfunctor));
				data.y.domain = yScale.domain();
			}
			else
			{
				var dfunctor = data.y.domainFunctor;
				if (dfunctor == null)
					dfunctor = function(d) { 
						return (yInverted ? -1.0 : 1.0) * d[data.y.idx]; 
					}
				var newDomain = d3.extent(dataset,dfunctor);
				var oldDomain = data.y.domain;
				if (newDomain[0] != oldDomain[0] || newDomain[1] != oldDomain[1])
					yScale.domain(newDomain);
				else
					yScale.domain(oldDomain);
			}
			if (data.z.domain == null) {
				var dfunctor = data.z.domainFunctor;
				if (dfunctor == null)
					dfunctor = function(d) { 
						return (zInverted ? -1.0 : 1.0) * d[data.z.idx]; 
					}
				zScale.domain(d3.extent(dataset,dfunctor));
				data.z.domain = zScale.domain();
			}
			else
			{
				var dfunctor = data.z.domainFunctor;
				if (dfunctor == null)
					dfunctor = function(d) { 
						return (zInverted ? -1.0 : 1.0) * d[data.z.idx]; 
					}
				var newDomain = d3.extent(dataset,dfunctor);
				var oldDomain = data.z.domain;
				if (newDomain[0] != oldDomain[0] || newDomain[1] != oldDomain[1])
					zScale.domain(newDomain);
				else
					zScale.domain(oldDomain);
			}
			// and set the scale ranges
			this.xScale = data.x.scale = xScale;
			this.yScale = data.y.scale = yScale;
			this.zScale = data.z.scale = zScale;

			var sizeScale = data.size.scale;
			if (data.size.idx != -1) {
				data.size.functor = function(d) { return sizeScale(d[data.size.idx]); };
			} else if (data.size.functor !== undefined && data.size.functor != null) {
				// no op
			} else
				data.size.functor = 3;
			var sizeFn = data.size.functor;
			if (data.source.idx != -1) {
				data.source.functor = function(d) { return d[data.source.idx]; }
				if (data.keyFunction === undefined || data.keyFunction == null)
					data.keyFunction = data.source.functor;
			} else if (data.source.functor !== undefined && data.source.functor != null) {
				// no op
			}

			// color / alpha
			var opacity = data.opacity;
			if (opacity === undefined || opacity == null)
				opacity = 1.0;
			else {
				var aux = parseFloat(opacity);
				if (isNaN(aux)) opacity = 1.0;
				else if (aux < 0) opacity = 0.0;
				else if (aux > 1.0) opacity = 1.0;
				else
					opacity = aux;
			}
			var fillColor = data.color;
			if (fillColor === undefined)
				fillColor = null;
			data.opacity = opacity;
		}
	}
}

SHIV.graph.ScatterPlot3D.prototype.updateData = function() {
	console.log("updateData()");
	this.initScene(); // clear scene

	var xInverted = this.params.axis.x.inverted;
	var yInverted = this.params.axis.y.inverted;
	var zInverted = this.params.axis.z.inverted;

	var start = Date.now();
	var totalPoints = 0;
	
	for (var i = 0; i < this.params.data.length; i++) {
		var dataParams = this.params.data[i];
		var dataset = dataParams.data;

		totalPoints+= dataset.length;
		var sizeFn = dataParams.size.functor;
		var opacity = dataParams.opacity;
		var sizeScale = dataParams.size.scale;
		var sourceUidFn = dataParams.source.functor;
		var fillColor = dataParams.color;
		var xFunc = function(d) { return dataParams.x.scale((xInverted ? -1.0 : 1.0) * d[dataParams.x.idx]); };
		var yFunc = function(d) { 
			return dataParams.y.scale((yInverted ? -1.0 : 1.0) * d[dataParams.y.idx]); 
		};
		var cp = Date.now();

		// geometry has a limit of 65K points, should split dataset here
		var geometry = new THREE.Geometry();

		for (var j = 0; j < dataset.length; j++) {
			var d = dataset[j];
			var vertex = new THREE.Vector3();

			vertex.x = (xInverted ? -1.0 : 1.0) * d[dataParams.x.idx];
			vertex.y = (yInverted ? -1.0 : 1.0) * d[dataParams.y.idx];
			vertex.z = (zInverted ? -1.0 : 1.0) * d[dataParams.z.idx];

			geometry.vertices.push(vertex);
		}

		var material = new THREE.PointsMaterial( { size: (isNaN(sizeFn) ? 5 : sizeFn) } );
		material.opacity = isNaN(opacity) ? 1.0 : opacity;
		if (fillColor != null && fillColor.functor !== undefined && fillColor.functor != null) fillColor = fillColor.functor;
		material.color = new THREE.Color(fillColor != null ? fillColor : "#4682B4");
		material.transparent = true;
		if (dataParams.shape == "circle") {
			material.map = this.params.three.sprite;
			material.alphaTest = 0.5;
		}

		this.params.three.scene.add( new THREE.Points(geometry, material) );
		//console.log("Create[B] Create points took: "+(Date.now() - cp)+" ms");
	}
	//console.log("Points creation took: "+(Date.now() - start)+" ms for "+totalPoints+" points");
	this.animate();
}

SHIV.graph.ScatterPlot3D.prototype.updateAxisX = function() {
	// todo: use domain info
	var params = this.params;
	if (this.params.axis.x.enabled) {
		if (this.params.axis.grid.enabled) {
			var gridXZ = params.three.gridXZ = new THREE.GridHelper(100, 10);
			gridXZ.setColors(new THREE.Color("lightgrey"), new THREE.Color("lightgrey"));
			gridXZ.position.set(0, -100, 0);
			params.three.scene.add(gridXZ);
		}
	}
}

SHIV.graph.ScatterPlot3D.prototype.updateAxisY = function() {
	var params = this.params;
	if (this.params.axis.y.enabled) {
		if (this.params.axis.grid.enabled) {
			var gridXY = params.three.gridXY = new THREE.GridHelper(100, 10);
			gridXY.setColors(new THREE.Color("lightgrey"), new THREE.Color("lightgrey"));
			gridXY.position.set(0, 0, -100);
			gridXY.rotation.x = Math.PI / 2;
			params.three.scene.add(gridXY);
		}
	}
}

SHIV.graph.ScatterPlot3D.prototype.updateAxisZ = function() {
	var params = this.params;
	if (this.params.axis.z.enabled) {
		if (this.params.axis.grid.enabled) {
			var gridYZ = params.three.gridYZ = new THREE.GridHelper(100, 10);
			gridYZ.setColors(new THREE.Color("lightgrey"), new THREE.Color("lightgrey"));
			gridYZ.position.set(-100, 0, 0);
			gridYZ.rotation.z = Math.PI / 2;
			params.three.scene.add(gridYZ);
		}
	}
}

SHIV.graph.ScatterPlot3D.prototype.resizeViewport = function(width,height,maintainDomains) {
	if (maintainDomains === undefined || maintainDomains == null)
		maintainDomains = false; // domains are maintained by default
	var chart = this.params.chart;
	this.params.width = width;
	this.params.height = height;

	var canvas = this.params["canvas-elem"].getElementsByTagName("canvas")[0];
	canvas.width = width;
	canvas.height = height;
	canvas.style.width = width+"px";
	canvas.style.height = height+"px";
	this.params.three.renderer.setSize(width,height);

	//this.updateAxis();
	chart.updateAxisX();
	chart.updateAxisY();
	chart.updateAxisZ();
	if (chart.updateLegend != undefined) 
		chart.updateLegend();
	chart.updateData();
}

SHIV.graph.ScatterPlot3D.prototype.detachBrush = function() {

}

SHIV.graph.ScatterPlot3D.prototype.attachBrush = function(params) {
	if (params === undefined || params == null) params = this.params;
}

SHIV.graph.ScatterPlot3D.prototype.zoomEvent = function(howMuch,evtSource) {
	if (howMuch === undefined || howMuch == null) return;
	if (!this.params.zooming.enabled) return;
}

SHIV.graph.ScatterPlot3D.prototype.zoomIn = function(evt,howMuch) {
	if (howMuch === undefined || howMuch == null) howMuch = "+1";
	this.zoomEvent(howMuch,evt);
}

SHIV.graph.ScatterPlot3D.prototype.zoomOut = function(evt,howMuch) {
	if (howMuch === undefined || howMuch == null) howMuch = "-1";
	this.zoomEvent(howMuch,evt);
}

SHIV.graph.ScatterPlot3D.prototype.detachZoom = function() {
	this.params.zooming.enabled = false;
	this.params.zooming.elem.on(".zoom",null);
}

SHIV.graph.ScatterPlot3D.prototype.attachZoom = function() {

}

SHIV.graph.ScatterPlot3D.prototype.createPoints = function(dataParams,selection,data,keyFunction,sizeFn,xFunc,yFunc,sourceUidFn,opacity,fillColor)
{

}

SHIV.graph.ScatterPlot3D.prototype.createPointsEventHandlers = function(isGrid, dataParams, selection, params)
{

}



SHIV.graph.ScatterPlot3D.prototype.updateAxis = function() {

}

SHIV.graph.ScatterPlot3D.prototype.clear = function() {
	
}

SHIV.graph.ScatterPlot3D.prototype.reset = function() {
	// for the moment we reset the domain from the first series
	var xdomain = this.params.data[0].x.domain;
	var ydomain = this.params.data[0].y.domain;
	this.xScale.domain(xdomain);
	this.yScale.domain(ydomain);
	this.updateAxis();
	// todo: reset zoom
}

SHIV.graph.ScatterPlot3D.prototype.appendData = function(data,recenterDomain) {
	var start = Date.now();
	var totalPoints = 0;
	if (recenterDomain === undefined || recenterDomain == null)
		recenterDomain = true;
	for (var i = 0; i < data.length; i++) {
		var dataParams = data[i];
		var originalData = dataParams;
		var newDataset = dataParams.data;
		dataParams.data = null; // null it so that the next step doesn't overwrite it
		var originalDataset = this.params.data[i].data;
		dataParams = SHIV.utils.MergeRecursive(originalData, this.params.data[i]);
		for (var j = 0; j < newDataset.length; j++)
			originalDataset.push(newDataset[j]);
		dataParams.data = originalDataset;
		var opacity = dataParams.opacity;
		if (opacity === undefined || opacity == null)
			opacity = 1.0;
		else {
			var aux = parseFloat(opacity);
			if (isNaN(aux)) opacity = 1.0;
			else if (aux < 0) opacity = 0.0;
			else if (aux > 1.0) opacity = 1.0;
			else
				opacity = aux;
		}
		var fillColor = dataParams.color;
		if (fillColor === undefined)
			fillColor = null;
		dataParams.opacity = opacity;
		var sizeScale = dataParams.size.scale;
		var sizeFn = null;
		if (dataParams.size.idx != -1) {
			sizeFn = function(d) { return sizeScale(d[dataParams.size.idx]); };
		} else if (dataParams.size.functor !== undefined && dataParams.size.functor != null) {
			sizeFn = dataParams.size.functor;
		} else
			sizeFn = 3;
		var sourceUidFn = null;
		if (dataParams.source.idx != -1) {
			sourceUidFn = function(d) { return d[dataParams.source.idx]; }
			dataParams.source.functor = sourceUidFn;
			if (dataParams.keyFunction === undefined || dataParams.keyFunction == null)
				dataParams.keyFunction = sourceUidFn;
		} else if (dataParams.source.functor !== undefined && dataParams.source.functor != null) {
			sourceUidFn = dataParams.source.functor;
		}
		var dataset = newDataset;

		// update xScale and yScale
		var updateOldData = false;
		var updateOldDomain = false;
		var dfunctor = dataParams.x.domainFunctor;
		if (dfunctor == null)
			dfunctor = function(d) { 
				return d[dataParams.x.idx]; 
			}
		var newDomain = d3.extent(dataset,dfunctor);
		var oldDomain = dataParams.x.scale.domain();
		var oldDataDomain = dataParams.x.domain;
		if (isNaN(oldDataDomain[0]) && isNaN(oldDataDomain[1])) {
			oldDataDomain = newDomain;
			updateOldDomain = true;
		}
		if (oldDomain[0] < newDomain[0]) {
			newDomain[0] = oldDomain[0];
			updateOldData = true;	
		}
		if (newDomain[0] < oldDataDomain[0]) { 
			oldDataDomain[0] = newDomain[0];
			updateOldDomain = true;
		}
		if (oldDomain[1] > newDomain[1]) {
			newDomain[1] = oldDomain[1];
			updateOldData = true;
		}
		if (newDomain[1] > oldDataDomain[1]) { 
			oldDataDomain[1] = newDomain[1];
			updateOldDomain = true;
		}
		if (recenterDomain && newDomain[0] !== undefined && newDomain[1] !== undefined)
			this.xScale.domain(newDomain);
		if (updateOldDomain) // only update domains if needed
			dataParams.x.domain = oldDataDomain;

		dfunctor = dataParams.y.domainFunctor;
		if (dfunctor == null)
			dfunctor = function(d) { 
				return d[dataParams.y.idx]; 
			}
		newDomain = d3.extent(dataset,dfunctor);
		oldDomain = dataParams.y.scale.domain();
		oldDataDomain = dataParams.y.domain;
		if (isNaN(oldDataDomain[0]) && isNaN(oldDataDomain[1])) {
			oldDataDomain = newDomain;
			updateOldDomain = true;
		}
		if (oldDomain[0] < newDomain[0]) {
			newDomain[0] = oldDomain[0];
			updateOldData = true;	
		}
		if (newDomain[0] < oldDataDomain[0]) { 
			oldDataDomain[0] = newDomain[0];
			updateOldDomain = true;
		}
		if (oldDomain[1] > newDomain[1]) {
			newDomain[1] = oldDomain[1];
			updateOldData = true;
		}
		if (newDomain[1] > oldDataDomain[1]) { 
			oldDataDomain[1] = newDomain[1];
			updateOldDomain = true;
		}
		if (recenterDomain && newDomain[0] !== undefined && newDomain[1] !== undefined)
			this.yScale.domain(newDomain);
		if (updateOldDomain) // only update domains if needed
			dataParams.y.domain = oldDataDomain;

		dfunctor = dataParams.z.domainFunctor;
		if (dfunctor == null)
			dfunctor = function(d) { 
				return d[dataParams.z.idx]; 
			}
		newDomain = d3.extent(dataset,dfunctor);
		oldDomain = dataParams.z.scale.domain();
		oldDataDomain = dataParams.z.domain;
		if (isNaN(oldDataDomain[0]) && isNaN(oldDataDomain[1])) {
			oldDataDomain = newDomain;
			updateOldDomain = true;
		}
		if (oldDomain[0] < newDomain[0]) {
			newDomain[0] = oldDomain[0];
			updateOldData = true;	
		}
		if (newDomain[0] < oldDataDomain[0]) { 
			oldDataDomain[0] = newDomain[0];
			updateOldDomain = true;
		}
		if (oldDomain[1] > newDomain[1]) {
			newDomain[1] = oldDomain[1];
			updateOldData = true;
		}
		if (newDomain[1] > oldDataDomain[1]) { 
			oldDataDomain[1] = newDomain[1];
			updateOldDomain = true;
		}
		if (recenterDomain && newDomain[0] !== undefined && newDomain[1] !== undefined)
			this.zScale.domain(newDomain);
		if (updateOldDomain) // only update domains if needed
			dataParams.z.domain = oldDataDomain;

		// update points
		var xScale = this.xScale;
		var yScale = this.yScale;
		var zScale = this.zScale;

		this.params.data[i] = dataParams;
	}

	this.updateAxisX();
	this.updateAxisY();
	this.updateAxisZ();
	this.updateAxis();
	this.updateData();
	//console.log("Points update took: "+(Date.now() - start)+" ms for "+totalPoints+" points");
}

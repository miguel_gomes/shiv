/**
 * SHIV Visualization and VisualizationManager representation.
 *
 * @author Miguel Gomes (alka.setzer@gmail.com)
 * @since 0.1
 * @depends shiv.js
 * 
 */

console.log("loading shiv-classes-visualization.js...");

// Visualization Related

/**
 * Visualization constructor.
 * @param id Visualization identifier
 * @param name Visualization name
 * @param table Source table identifier
 * @param type Visualization type
 * @param lodGenerator LoD Generator used to generate the LoDs for the visualization
 * @param dimensionality Number of dimensions of the visualization (i.e. 1~4)
 * @param lods Number of levels of detail
 * @return new Visualization object
 */
SHIV.Classes.Visualization = function(id,name,table,type,lodGenerator,dimensionality,lods) {
	this.id = id;
	this.name = name !== undefined && name !== null && name != "null" ? name : null;
	this.type = type === undefined || type == null ? "Unknown" : type;
	this.tableId = table;
	this.lodGenerator = lodGenerator === undefined || lodGenerator == null ? "Unknown" : lodGenerator;
	this.dimensionality = dimensionality === undefined || dimensionality == null ? 0 : parseInt(dimensionality,10);
	this.lods = lods === undefined || lods == null ? 0 : parseInt(lods,10);
	this.dimensions = {};
	this.description = null;
	this.metadata = null;
}
/** Object type */
SHIV.Classes.Visualization.prototype.ObjectType = "Visualization";
/**
 * Checks if the visualization has metadata information.
 * @return True if the visualization has metadata, false otherwise
 */
SHIV.Classes.Visualization.prototype.hasMetadata = function() {
	return this.metadata != null;
}

/**
 * Method used to define dimensions.
 * @param dimension One of x,y,z,size,color or glyph
 * @param value Dimension definition
 */
SHIV.Classes.Visualization.prototype.defineDimension = function(dimension,value) {
	if (dimension === undefined || dimension == null) return;
	dimension = dimension.trim().toLowerCase();
	if (dimension == "colour") dimension = "color";
	if (!(	dimension == "x" || 
			dimension == "y" || 
			dimension == "z" || 
			dimension == "color" ||
			dimension == "size" ||
			dimension == "glyph")) return;
	this.dimensions[dimension] = value;
}

/**
 * Visualization Manager constructor.
 */
SHIV.Classes.VisualizationManager = function() {
	this.visualizations = {}; // as a map
	this.visualizationsCount = 0;
	this.requests = {};
}
/** Object type */
SHIV.Classes.VisualizationManager.prototype.ObjectType = "VisualizationManager";

/**
 * Method that resets this manager.
 */
SHIV.Classes.VisualizationManager.prototype.reset = function () {
	this.visualizations = {}; // as a map
	this.visualizationsCount = 0;
	var visualizationsTable = document.getElementById('tbl-visualizations');
	if(visualizationsTable==null)
		return;
	visualizationsTable = visualizationsTable.getElementsByTagName('TBODY')[0];
	var cNode = visualizationsTable.cloneNode(false);
	visualizationsTable.parentNode.replaceChild(cNode ,visualizationsTable);
};

/**
 * Method used to create new Visualizations
 * @param sourceTable Optional source table
 */
SHIV.Classes.VisualizationManager.prototype.createNew = function(sourceTable) {
	var divCreateNewVisualization = document.createElement('div');
	divCreateNewVisualization.className = "panel panel-default";
	var divPanelHeading = document.createElement('div');
	divPanelHeading.className = "panel-heading";
	var h3 = document.createElement('h3');
	h3.className = "panel-title";
	h3.appendChild(document.createTextNode("Create new Visualization"));
	divPanelHeading.appendChild(h3);
	divCreateNewVisualization.appendChild(divPanelHeading);
	var divPanelBody = document.createElement('div');
	divPanelBody.className = "panel-body";

	// error display integrated but hidden by default
	var divAlert = document.createElement('div');
	divAlert.className = "alert alert-danger hidden";
	divAlert.role = "alert";
	divAlert.id = "vis-create-error";
	var divAlertCloseBtn = document.createElement('button');
	divAlertCloseBtn.className = "close";
	divAlertCloseBtn.ariaLabel = "Close";
	divAlertCloseBtn.onmouseup = function(evt) {
		var elem = evt.target;
		if (elem.nodeName.toLowerCase() == "span")
			elem = elem.parentNode.parentNode;
		else
			elem = elem.parentNode;
		var cn = elem.className;
		if (cn.indexOf(" show" > 0))
			elem.className = cn.replace(" show"," hidden");
	}
	var divAlertCloseBtnSpan = document.createElement('span');
	divAlertCloseBtnSpan.ariaHidden = "true";
	divAlertCloseBtnSpan.innerHTML ="&times;";
	divAlertCloseBtn.appendChild(divAlertCloseBtnSpan);
	divAlert.appendChild(divAlertCloseBtn);
	var divAlertSpanWarning = document.createElement('span');
	divAlertSpanWarning.className = "glyphicon glyphicon-exclamation-sign";
	divAlertSpanWarning.ariaHidden = "true";
	divAlert.appendChild(divAlertSpanWarning);
	var divAlertSpanSR = document.createElement('span');
	divAlertSpanSR.className = "sr-only";
	divAlertSpanSR.appendChild(document.createTextNode("Error:"));
	divAlert.appendChild(divAlertSpanSR);
	var divAlertMsg = document.createElement('span');
	divAlertMsg.id = "vis-create-error-msg";
	divAlert.appendChild(divAlertMsg);
	divPanelBody.appendChild(divAlert);

	divPanelBody.appendChild(
		SHIV.createInputGroup(
			'Name',
			"Visualization",
			null,
			"Visualization name",
			"vis-create-name"
		)
	);
	divPanelBody.appendChild(document.createElement('br'));
	divPanelBody.appendChild(
		SHIV.createInputGroup(
			'Description',
			"Description",
			null,
			"Visualization description",
			"vis-create-description"
		)
	);
	divPanelBody.appendChild(document.createElement('br'));
	var xp = document.createElement("span");
	xp.className = "form-inline";
	// Source table select
	var tableItems = [];
	tableItems.push({"value":"__NO_SELECTION__","text":"------------------------"});
	for (var tableId in SHIV.tableManager.tables) {
		var table = SHIV.tableManager.tables[tableId];
		var dataset = "";
		var selected = (sourceTable == tableId || sourceTable == table);
		if (table.associatedDatasetId !== undefined && table.associatedDatasetId != null)
			dataset=table.associatedDatasetId+"/";
		tableItems.push({"value":table.id,"text":table.name,"tooltip":dataset+tableId,"selected":selected});
	}
	var tableSelect = SHIV.createInputGroupSelect("Table",tableItems,"Source table","vis-create-table");
	tableSelect.onchange = function(evt) {
		var tableSelect = evt.target;
		var value = tableSelect.value;
		var xBlock = document.getElementById("vis-create-dim-x");
		var yBlock = document.getElementById("vis-create-dim-y");
		var zBlock = document.getElementById("vis-create-dim-z");
		if (value == "__NO_SELECTION__") {
			SHIV.addAutoCompleteToInput(xBlock,[]);
			SHIV.addAutoCompleteToInput(yBlock,[]);
			SHIV.addAutoCompleteToInput(zBlock,[]);
			return;
		}
		var table = SHIV.tableManager.tables[value];
		var cols = [];
		// only add auto-complete options if the number of columns are less that 255 (performance reasons)
		// TODO add this to options
		if (table.columns.length < 255) {
			for (var i = 0; i < table.columns.length; i++) {
				var col = table.columns[i];
				cols.push(col.name);
			}
		}
		SHIV.addAutoCompleteToInput(xBlock,cols);
		SHIV.addAutoCompleteToInput(yBlock,cols);
		SHIV.addAutoCompleteToInput(zBlock,cols);
	}
	xp.appendChild(tableSelect);
	xp.appendChild(document.createTextNode(' '));
	// 2D / 3D
	var dimensionalityItems = [];
	dimensionalityItems.push({"value":"__NO_SELECTION__","text":"----"});
	dimensionalityItems.push({"value":"1","text":"1D","tooltip":"One dimensional visualizations are useful for accessing the frequency"});
	dimensionalityItems.push({"value":"2","text":"2D","tooltip":"Two dimensional visualizations are useful for the majority of cases"});
	dimensionalityItems.push({"value":"3","text":"3D","tooltip":"Three dimensional visualizations are useful for cases where the 3rd dimension adds added value"});
	var dimSelect = SHIV.createInputGroupSelect("Dimensionality",dimensionalityItems,"Visualization dimensionality","vis-create-dimensionality");
	dimSelect.onchange = function(evt) {
		var dimSelect = evt.target;
		var value = dimSelect.value;
		var xBlock = document.getElementById("vis-create-dim-x-block");
		var yBlock = document.getElementById("vis-create-dim-y-block");
		var zBlock = document.getElementById("vis-create-dim-z-block");
		xBlock.className = "hidden";
		yBlock.className = "hidden";
		zBlock.className = "hidden";
		if (value == "__NO_SELECTION__") return;
		var dims = parseInt(value);
		if (dims >= 1) xBlock.className = "";
		if (dims >= 2) yBlock.className = "";
		if (dims >= 3) zBlock.className = "";
		if (sourceTable !== undefined && sourceTable != null)
			tableSelect.onchange({'target':tableSelect.getElementsByTagName("SELECT")[0]});
	}
	xp.appendChild(dimSelect);
	divPanelBody.appendChild(xp);
	divPanelBody.appendChild(document.createElement('br'));
	// X
	var xBlock = document.createElement("span");
	xBlock.id = "vis-create-dim-x-block";
	xBlock.className = "hidden";
	xBlock.appendChild(document.createElement('br'));
	xBlock.appendChild(
		SHIV.createInputGroup(
			'X',
			"X Dimension",
			null,
			"Definition for the X dimension",
			"vis-create-dim-x"
		)
	);
	divPanelBody.appendChild(xBlock);
	// Y
	var yBlock = document.createElement("span");
	yBlock.id = "vis-create-dim-y-block";
	yBlock.className = "hidden";
	yBlock.appendChild(document.createElement('br'));
	yBlock.appendChild(
		SHIV.createInputGroup(
			'Y',
			"Y Dimension",
			null,
			"Definition for the Y dimension",
			"vis-create-dim-y"
		)
	);
	divPanelBody.appendChild(yBlock);
	// Z
	var zBlock = document.createElement("span");
	zBlock.id = "vis-create-dim-z-block";
	zBlock.className = "hidden";
	zBlock.appendChild(document.createElement('br'));
	zBlock.appendChild(
		SHIV.createInputGroup(
			'Z',
			"Z Dimension",
			null,
			"Definition for the Z dimension",
			"vis-create-dim-z"
		)
	);
	divPanelBody.appendChild(zBlock);

	divPanelBody.appendChild(document.createElement('br'));
	// Size
	divPanelBody.appendChild(
		SHIV.createInputGroup(
			'Size',
			"Point size",
			null,
			"Definition for point Size",
			"vis-create-dim-size"
		)
	);
	divPanelBody.appendChild(document.createElement('br'));
	// Colour
	divPanelBody.appendChild(
		SHIV.createInputGroup(
			'Color',
			"Point color",
			null,
			"Definition for point Color",
			"vis-create-dim-color"
		)
	);
	divPanelBody.appendChild(document.createElement('br'));
	// Glyph
	divPanelBody.appendChild(
		SHIV.createInputGroup(
			'Glyph',
			"Point glyph",
			null,
			"Definition for point Glyphs",
			"vis-create-dim-glyph"
		)
	);

	//divPanelBody.appendChild(document.createElement('br'));
	divCreateNewVisualization.appendChild(divPanelBody);

	if (sourceTable !== undefined && sourceTable != null)
		tableSelect.onchange({'target':tableSelect.getElementsByTagName("SELECT")[0]});

	var buttons = new Array();
	var okButton = new Object();
	okButton['text'] = "Create";
	okButton['onMouseUp'] = function(evt) { 
		var name = document.getElementById('vis-create-name').value.trim();
		var description = document.getElementById('vis-create-description').value.trim();
		var sourceTable = document.getElementById('vis-create-table').value;
		var dimensionality = document.getElementById('vis-create-dimensionality').value;
		var x = document.getElementById('vis-create-dim-x').value.trim();
		var y = document.getElementById('vis-create-dim-y');
		if (y != null) y = y.value.trim();
		var z = document.getElementById('vis-create-dim-z');
		if (z != null) z = z.value.trim();
		var glyph = document.getElementById('vis-create-dim-glyph');
		if (glyph != null) glyph = glyph.value.trim();
		var size = document.getElementById('vis-create-dim-size');
		if (size != null) size = size.value.trim();
		var colour = document.getElementById('vis-create-dim-color');
		if (colour != null) colour = colour.value.trim();
		var dims = 0;
		var errorMsg = "";
		// validations
		if (name.length == 0)
			errorMsg += "Name can not be empty"; // i8n
		if (sourceTable == "__NO_SELECTION__") {
			errorMsg += "<br />Select one source table"; // i8n
		}
		if (dimensionality == "__NO_SELECTION__") {
			errorMsg += "<br />Select a dimensionality"; // i8n
		} else
			dims = parseInt(dimensionality);
		if (dims >= 1 && x.length == 0) {
			errorMsg += "<br />X definition can not be empty"; // i8n
		}
		if (dims >= 2 && y.length == 0) {
			errorMsg += "<br />Y definition can not be empty"; // i8n
		}
		if (dims >= 3 && y.length == 0) {
			errorMsg += "<br /> Z definition can not be empty"; // i8n
		}
		if (errorMsg.length > 0) {
			var cn = document.getElementById('vis-create-error').className;
			if (cn.indexOf(" hidden" > 0))
				document.getElementById('vis-create-error').className = cn.replace(" hidden"," show");
			document.getElementById('vis-create-error-msg').innerHTML = errorMsg;
			return false; // don't close so that the user sees the errors and stuff
		}

		var visStructure = {
			'name': name,
			'description': description,
			'tableId': sourceTable,
			'dimensionality': dimensionality,
			'dimensions': {
				'x': x,
				'y': y,
				'z': z,
				'glpyh': glyph,
				'size': size,
				'color': colour
			}
		};

		var visDims = "";
		if (dims >= 1) visDims += "&vis-x="+x;
		if (dims >= 2) visDims += "&vis-y="+y;
		if (dims >= 3) visDims += "&vis-z="+z;
		if (glyph != null && glyph.length > 0) visDims += "&vis-glyph="+glyph;
		if (size != null && size.length > 0) visDims += "&vis-size="+size;
		if (colour != null && colour.length > 0) visDims += "&vis-color="+colour;

		SHIV.server.executeCommand("visualization-create?mode=async&vis-dimensions="+dimensionality+"&vis-name="+name+"&vis-description="+description+"&table-id="+sourceTable+visDims,SHIV.visualizationManager.handleVisualizationCreate);

		return true;
	};
	okButton['className'] = "btn-default";
	buttons.push(okButton);
	var cancelButton = new Object();
	cancelButton['text'] = "Cancel";
	cancelButton['className'] = "btn-primary";
	buttons.push(cancelButton);
	SHIV.modal("Visualization",divCreateNewVisualization,false,buttons);
}

/**
 * Method used to handle the reply of the Visualization Create command
 * @param jqXHR JQuery XHR response
 * @param data Parsed data
 */
SHIV.Classes.VisualizationManager.prototype.handleVisualizationCreate = function(jqXHR, data) {
	// only two things could have happen that are positive
	SHIV.jobManager.updateJobs(); // either will be executed or queued for execution upon connect

	if (jqXHR.status != 200 && jqXHR.status != 202) {
		var msg = "";
		if (data != null) {
			msg += " - "+data['status-msg'];
		}
		SHIV.modal("Error","Could not create Visualization "+msg);
		return;
	}
	if (jqXHR.status == 200) {
		SHIV.visualizationManager.updateVisualizations(); // update list of visualizations...
		var visid = data["vis-id"];
		SHIV.showMessage("info","Visualization created",null,"Success");
	} else { // job is ongoing
		var visid = data["vis-id"];
		var jobid = data["job-id"];
		SHIV.showMessage("info","Visualization creation in progress...",null,"Information");
		// check status every 1second
		setTimeout(function(){
			SHIV.server.executeCommand("job-info?job-id="+jobid,SHIV.visualizationManager.checkVisualizationCreateStatus);
		},1000);
	}
}

SHIV.Classes.VisualizationManager.prototype.checkVisualizationCreateStatus = function(jqXHR, data) {
	if (jqXHR.status != 200) {
		var msg = "";
		if (data != null) {
			msg += " - "+data['status-msg'];
		}
		SHIV.modal("Error","Could not check status of Job "+msg);
		return;
	}
	var blocks = data['payload'];
	if (blocks == null || blocks.length == 0) {
		SHIV.modal("Error","Could not check status of Job - No data");
		return;
	}
	var block = blocks[0];
	var data = block['data'].job;
	var jobid = data['jobId'];
	var phase = data['phase'];
	if (phase == "COMPLETE" || phase == "COMPLETED") {
		SHIV.visualizationManager.updateVisualizations(); // update list of visualizations...
		SHIV.jobManager.updateJobs();
		SHIV.showMessage("info","Visualization created",null,"Success");
	} else if (phase == "EXECUTING" || phase == "RUNNING") {
		setTimeout(function(){
			SHIV.server.executeCommand("job-info?job-id="+jobid,SHIV.visualizationManager.checkVisualizationCreateStatus);
		},1000);
	} else if (phase == "ERROR") {
		SHIV.jobManager.updateJobs();
		SHIV.showMessage("error","Visualization failed to create.",null,"ERROR");
	} else {
		console.log("Phase: "+phase+", for job: "+jobid);
	}
}

/**
 * Method used to add a visualization parameter to the Visualization information table.
 * @param tbody Table body where to write the parameter
 * @param key Parameter key
 * @param value Parameter value
 * @param keyTitle Optional key tooltip
 * @param valueTitle Optional value tooltip
 */
SHIV.Classes.VisualizationManager.prototype.addVisualizationInfoParam = function(tbody,key,value,keyTitle,valueTitle) 
{
	tr = document.createElement('tr');
	td = document.createElement('td');
	td.className = "visualization-info key";
	if (keyTitle === undefined || keyTitle == null)
		td.appendChild(document.createTextNode(key));
	else {
		var span = document.createElement('span');
		span.title = keyTitle;
		span.appendChild(document.createTextNode(key));
		td.appendChild(span);
	}
	tr.appendChild(td);
	td = document.createElement('td');
	td.className = "visualization-info param";
	if (valueTitle === undefined || valueTitle == null)
		td.appendChild(document.createTextNode(value));
	else {
		var span = document.createElement('span');
		span.title = valueTitle;
		span.appendChild(document.createTextNode(value));
		td.appendChild(span);
	}
	tr.appendChild(td);
	tbody.appendChild(tr);
}

/**
 * Method used to show/hide information for a given Visualization.
 * @param visualization Visualization to which we want to toggle the information, due to the way JS works this can also be an event
 */
SHIV.Classes.VisualizationManager.prototype.toggleVisualizationInfo = function (visualization) {
	var baseTr = null;
	var expand = null;
	if (visualization === undefined || visualization == null || (visualization != null && visualization['ObjectType'] === undefined)) {
		// check if mouse event
		if (!(visualization != null && visualization['ObjectType'] === undefined))
			return;
		// a (target) > td > tr
		expand = visualization.target;
		baseTr = visualization.target.parentNode.parentNode;
		var id = baseTr.id.substring(14); // dataset.
		visualization = SHIV.visualizationManager.visualizations[id];
	}
	var rowKey = "visualization-info."+visualization.id.replace(" ","_");
	var aux = null;
	if ((aux = document.getElementById(rowKey)) != null) {
		if (aux.style.display == 'none') {
			expand.className = 'visualization-info-a glyphicon glyphicon-chevron-up';
			aux.style.display = '';
		}
		else {
			expand.className = 'visualization-info-a glyphicon glyphicon-chevron-down';
			aux.style.display = 'none';
		}
		return; // already there
	}
	// TODO change this to an image or something
	expand.className = 'visualization-info-a glyphicon glyphicon-chevron-up';
	// tables row
	var tablesRow = document.createElement('tr');
	tablesRow.id = rowKey;
	tablesRow.className = "visualization-info-row";
	var tablesCell = document.createElement('td');
	tablesCell.colSpan = 3;
	// tables table
	var datasetTbls = document.createElement('table');
	datasetTbls.className = "visualization-info-tbl table table-hover";
	var thead = document.createElement('thead');
	var tr = document.createElement('tr');
	// key, param
	var th = document.createElement('th');
	th.appendChild(document.createTextNode('Parameter'))
	th.className="id";
	tr.appendChild(th);
	th = document.createElement('th');
	th.appendChild(document.createTextNode('Value'))
	th.className="name";
	tr.appendChild(th);
	thead.appendChild(tr);
	datasetTbls.appendChild(thead);
	var tbody = document.createElement('tbody');
	// id, name, table, type, lod-generator, dimensionality, lods, x,y,z,size,color,glyph
	var tbl = SHIV.tableManager.tables[visualization['tableId']];
	SHIV.visualizationManager.addVisualizationInfoParam(tbody,'ID', visualization.id, 'Visualization Identifier');
	SHIV.visualizationManager.addVisualizationInfoParam(tbody,'Table',tbl != null ? tbl.name: visualization['tableId'], 'Source Table', tbl!=null ? 'id: '+tbl.id : null);
	SHIV.visualizationManager.addVisualizationInfoParam(tbody,'LoD Generator',visualization['lodGenerator'],"Level-of-Detail Generator");
	SHIV.visualizationManager.addVisualizationInfoParam(tbody,'LoDs',visualization['lods'],"Number of Levels-of-Detail");
	var dims = visualization['dimensionality'];
	if (dims >= 1) SHIV.visualizationManager.addVisualizationInfoParam(tbody,'X',visualization.dimensions['x'],'X axis definition');
	if (dims >= 2) SHIV.visualizationManager.addVisualizationInfoParam(tbody,'Y',visualization.dimensions['y'],'Y axis definition');
	if (dims >= 3) SHIV.visualizationManager.addVisualizationInfoParam(tbody,'Z',visualization.dimensions['z'],'Z axis definition');
	if (visualization.dimensions['size'] !== undefined && visualization.dimensions['size'] != null && visualization.dimensions['size'].length > 0)
		SHIV.visualizationManager.addVisualizationInfoParam(tbody,'Size',visualization.dimensions['size'],'Size definition');
	if (visualization.dimensions['color'] !== undefined && visualization.dimensions['color'] != null && visualization.dimensions['color'].length > 0)
		SHIV.visualizationManager.addVisualizationInfoParam(tbody,'Color',visualization.dimensions['color'],'Color definition');
	if (visualization.dimensions['glyph'] !== undefined && visualization.dimensions['glyph'] != null && visualization.dimensions['glyph'].length > 0)
		SHIV.visualizationManager.addVisualizationInfoParam(tbody,'Glyph',visualization.dimensions['glyph'],'Glpyh definition');
	datasetTbls.appendChild(tbody);
	tablesCell.appendChild(datasetTbls);
	tablesRow.appendChild(tablesCell);
	baseTr.parentNode.insertBefore(tablesRow,baseTr.nextSibling);
}

/**
 * Method used to plot a visualization.
 * @param visualization Visualization to plot (this may be a JS event)
 * @param type chart type
 */
SHIV.Classes.VisualizationManager.prototype.plotVisualization = function(visualization,type) {
	var baseTr = null;
	var expand = null;
	if (visualization === undefined || visualization == null || (visualization != null && visualization['ObjectType'] === undefined)) {
		// check if mouse event
		if (!(visualization != null && visualization['ObjectType'] === undefined))
			return;
		// span (target) > td > tr
		expand = visualization.target;
		baseTr = visualization.target.parentNode.parentNode;
		var id = baseTr.id.substring(14); // dataset.
		visualization = SHIV.visualizationManager.visualizations[id];
	}
	// we now have the visualization, we need to see if we need to fetch metadata
	if (!visualization.hasMetadata()) {
		// need to fetch, we will only fetch level 0, we need the headers because we are going to lose track of the visualization..
		SHIV.server.executeCommand("visualization-metadata?vis-id="+visualization.id+"&lod-level=0&include-headers=true",
			function (jqXHR,data) {
				if (jqXHR.status != 200) {
					SHIV.modal("Error","Could not fetch visualization metadata");
					return;
				}
				// we got the data
				if (data === undefined || data == null) return; // nothing to do
				if (data['payload'] != null) { // we were given the message
					var blocks = data['payload'];
					for (var b = 0; b < blocks.length; b++) {
						var block = blocks[b];
						var data = block['data']; // block data
						var uid = data['uid'];
						if (uid === undefined || uid == null) return;
						uid = uid.substring(0,uid.indexOf('_meta')); // strip out the last bit
						var visualization = SHIV.visualizationManager.visualizations[uid];
						if (visualization == null) return;
						var aux = data['data']; // actual data
						for (var i = 0; i < aux.length; i++) {
							// we are only interested in level 0
							visualization.metadata = aux[i];
							var hs = aux[i][2].substring(1,aux[i][2].length-1).split(",");
							for (var ii = 0; ii < hs.length; ii++)
								hs[ii] = (parseFloat(hs[ii])*2.0)/Math.pow(2,visualization.lods);
							visualization.zoomLevels = hs;
							//console.log("visualization.zoomLevels: "+visualization.zoomLevels);
							break;
						}
					}
					SHIV.visualizationManager.plotVisualization(visualization,type);
				}
			}
		);
		return;
	}
	/*if (visualization.chart == null)*/
	{
		var len = visualization.dimensionality+7;
		var visDimsMap = [
			["X","Size","Color","Glyph","Source ID","Particle Count","Page ID","Type"],
			["X","Y","Size","Color","Glyph","Source ID","Particle Count","Page ID","Type"],
			["X","Y","Z","Size","Color","Glyph","Source ID","Particle Count","Page ID","Type"]
		];

		var resolveFn = function(params,dataParams,pts) {
						//params.svg.selectAll(".point").remove();
						// get the current viewport
						var xScale = dataParams.x.scale;
						var yScale = dataParams.y.scale;
						var zScale = dataParams.z !== undefined && dataParams.z != null ? dataParams.z.scale : null;
						// figure out current point density
						var area = xScale.range()[1] * yScale.range()[0];
						var ptsize = 1;
						var szfn = parseFloat(dataParams.size.functor);
						if (!isNaN(szfn)) ptsize = szfn*szfn;
						var d = (pts*ptsize) / area;
						//if (d > 0.1) return; // too much points still in viewport
						var xd = xScale.domain();
						var yd = yScale.domain();
						var zd = [0,0];
						var viewportXYZ = "";
						var reqLevels = [];
						switch (visualization.dimensionality) 
						{
							case 1: 
								viewportXYZ = xd[0]+";"+xd[1]; 
								//console.log("vis["+visualization.id+"] fetching data for viewport:{l:["+xd[0]+","+xd[1]+"]}");
								reqLevels[0] = Math.floor(visualization.lods-Math.sqrt((xd[1]-xd[0])/visualization.zoomLevels[0]));
								break;
							case 2: 
								viewportXYZ = xd[0]+";"+yd[0]+";"+xd[1]+";"+yd[1];
								//console.log("vis["+visualization.id+"] fetching data for viewport:{tl:["+xd[0]+","+yd[0]+"],br:["+xd[1]+","+yd[1]+"]}");
								reqLevels[0] = Math.floor(visualization.lods-Math.sqrt((xd[1]-xd[0])/visualization.zoomLevels[0]));
								reqLevels[1] = Math.floor(visualization.lods-Math.sqrt((yd[1]-yd[0])/visualization.zoomLevels[1]));
								break;
							case 3:
								if (zScale != null) {
									zd = zScale.domain();
									//console.log("vis["+visualization.id+"] fetching data for viewport:{tl:["+xd[0]+","+yd[0]+","+zd[0]+"],br:["+xd[1]+","+yd[1]+","+zd[1]+"]}");
									viewportXYZ = xd[0]+";"+yd[0]+";"+zd[0]+";"+xd[1]+";"+yd[1]+";"+zd[1]; 
								} else {
									viewportXYZ = xd[0]+";"+yd[0]+";0;"+xd[1]+";"+yd[1]+";0"; 
								}
								reqLevels[0] = Math.floor(visualization.lods-Math.sqrt((xd[1]-xd[0])/visualization.zoomLevels[0]));
								reqLevels[1] = Math.floor(visualization.lods-Math.sqrt((yd[1]-yd[0])/visualization.zoomLevels[1]));
								reqLevels[2] = Math.floor(visualization.lods-Math.sqrt((zd[1]-zd[0])/visualization.zoomLevels[2]));
								break; // fake 3d
						}
						var reqId = Date.now();
						SHIV.visualizationManager.requests[reqId] = {"chart": chart, "visualization": visualization};
						var maxLod = Number.MAX_VALUE;
						for (var ii = 0; ii < reqLevels.length; ii++) {
							if (reqLevels[ii] < maxLod) maxLod = reqLevels[ii];
						}
						if (maxLod < 0) maxLod = 0;
						console.log("vis["+visualization.id+"] fetching data for viewport:{tl:["+xd[0]+","+yd[0]+","+zd[0]+"],br:["+xd[1]+","+yd[1]+","+zd[1]+"]}, max-lod:{"+reqLevels+"}");
						SHIV.server.executeCommand("visualization-resolve-viewport?tag="+reqId+"&vis-id="+visualization.id+"&include-headers=false&max-items=0&cull-visible=true&max-lod="+maxLod+"&viewport="+viewportXYZ,SHIV.visualizationManager.processViewportRequest);
					};

		// first create an empty chart so that we can display stuff
		var params = {
			//'bindto': "#chart-"+visualization.id,
			'data': [
				{
					'source-id': visualization.tableId,
					'data': [], // empty array
					'data-description': visDimsMap[visualization.dimensionality-1], // data description
					'keyFunction': function(d) { return d[d.length-4]; },
					'source': {
						'idx': len - 4
					},
					'opacity': 0.75,
					'shape': 'square',
					'size': {
						'functor': 5
					},
					//'color': 'red'
				}
			],
			//'reductionPts': 1000000,
			'brushing': {
				'enabled': false
			},
			'zooming': {
				'enabled': true,
				'actions': {
					'low-data': resolveFn,
					'high-data': resolveFn
				}
			},
			'axis': {
				'x': {
					'title': {
						'label': visualization.dimensions['x'],
						'position': 'inside'
					},
					'tick': {
						'rotate': 0
					}
				}
			},
			'title': {
				'label': visualization.name
			},
			'dimensionality': visualization.dimensionality, // data dimensionality
			'type': type // chart type
		};
		if (type == "scatterplot" || type == "scatterplot2" || type == "scatterplot3d") {
			if (visualization.dimensionality >= 2)
				params.axis.y = {
					'enabled': true,
					'title': {
						'label': visualization.dimensions['y'],
						'position': 'inside'
					},
					'tick': {
						'rotate': 0
					}
				};
			if (visualization.dimensionality >= 3) 
			{
				params.data[0].z = {
					'idx': 2
				};
				params.axis.z = {
					'enabled': true,
					'title': {
						'label': visualization.dimensions['z'],
						'position': 'inside'
					},
					'tick': {
						'rotate': 0
					}
				};
			}
		}
		else if (type == "histogram") {
			params.axis.y = {
					'title': {
						'label': "Frequency"
					},
					'tick': {
						'rotate': 0
					}
				};
		}
		else if (type == "linechart") {
			params.axis.y = {
					'title': {
						'label': visualization.dimensions['y'],
						'position': 'inside'
					},
					'tick': {
						'rotate': 0
					}
				};
		}
		else if (type == "barchart") {
			params.axis.y = {
					'title': {
						'label': visualization.dimensions['y'],
						'position': 'inside'
					},
					'tick': {
						'rotate': 0
					}
				};
		}
		else if (type == "heatmap") {
			params.axis.y = {
					'title': {
						'label': visualization.dimensions['y'],
						'position': 'inside'
					},
					'tick': {
						'rotate': 0
					}
				};
		}
		else if (type == "blockchart") {
			params.axis.y = {
				"enabled": false
			};
		}
		var chart = SHIV.graph.createChart(params,type);
		//visualization.charts[chart.id] = chart;
		var reqId = Date.now();
		SHIV.visualizationManager.requests[reqId] = {"chart": chart, "visualization": visualization};
		// need to fetch, we will only fetch level 0, we need the headers because we are going to lose track of the visualization..
		SHIV.server.executeCommand("visualization-page?tag="+reqId+"&vis-id="+visualization.id+"&page-id=0&include-headers=false",
			SHIV.visualizationManager.resolveVisualizationPage
		);
		return;
	}
}

/**
 * Method used to process a Visualization Page request.
 * @param jqXHR JQuery XHR variable
 * @param data XHR data
 */
SHIV.Classes.VisualizationManager.prototype.resolveVisualizationPage = function (jqXHR,data) {
	if (jqXHR.status != 200) {
		SHIV.modal("Error","Could not fetch visualization metadata");
		return;
	}
	// we got the data
	if (data === undefined || data == null) return; // nothing to do
	var tag = data['tag'];
	var request = SHIV.visualizationManager.requests[tag];
	if (request == null) {
		console.log("No chart associated with request: "+tag);
		return;
	}
	var visualization = request.visualization;
	var chart = request.chart;
	var chartDiv = document.getElementById(chart.params.id);
	if (data['payload'] != null) { // we were given the message
		var start = Date.now();
		var blocks = data['payload'];
		var datasetData = null;
		for (var b = 0; b < blocks.length; b++) {
			var block = blocks[b];
			var data = block['data']; // block data
			var aux = data['data']; // actual data
			if (aux.length == 0) continue;
			var len = aux[0].length;
			if (datasetData == null)
				datasetData = aux;
			else {
				for (var i = 0; i < aux.length; i++)
					datasetData.push(aux[i]);
			}
		}
		// add all points in one go because it's faster than blocking while d3 processes the points
		var len = datasetData[0].length;
		var dataParams = [
		{
			'source-id': visualization.tableId,
			'data': datasetData,
				'keyFunction': function(d) { return d[d.length-4]; },
				'source': {
					'idx': len - 4
				}
			}
		];
		chart.appendData(dataParams);
		delete SHIV.visualizationManager.requests[tag];
		console.log("chart updated, process took: "+(Date.now()-start)+" ms");
	}
}

/**
 * Method used to process a VisualizationViewport request.
 * @param jqXHR JQuery XHR variable
 * @param data XHR data
 */
SHIV.Classes.VisualizationManager.prototype.processViewportRequest = function (jqXHR,data) {
	if (jqXHR.status != 200) {
		SHIV.modal("Error","Could not fetch visualization viewport data");
		return;
	}
	// we got the data
	if (data === undefined || data == null) return; // nothing to do
	var tag = data['tag'];
	var request = SHIV.visualizationManager.requests[tag];
	if (request == null) {
		console.log("No chart associated with request: "+tag);
		return;
	}
	var visualization = request.visualization;
	var chart = request.chart;
	var chartDiv = document.getElementById(chart.params.id);
	if (data['payload'] != null) { // we were given the message
		var start = Date.now();
		var blocks = data['payload'];
		var datasetData = null;
		for (var b = 0; b < blocks.length; b++) {
			var block = blocks[b];
			var data = block['data']; // block data
			var aux = data['data']; // actual data
			if (aux.length == 0) continue;
			var len = aux[0].length;
			if (datasetData == null)
				datasetData = aux;
			else {
				for (var i = 0; i < aux.length; i++)
					datasetData.push(aux[i]);
			}
		}
		// add all points in one go because it's faster than blocking while d3 processes the points
		var len = datasetData[0].length;
		chart.clear();
		var dataParams = [
		{
			'source-id': visualization.tableId,
			'data': datasetData,
				'keyFunction': function(d) { return d[d.length-4]; },
				'source': {
					'idx': len - 4
				}
			}
		];
		chart.appendData(dataParams);
		delete SHIV.visualizationManager.requests[tag];
		console.log("chart updated, process took: "+(Date.now()-start)+" ms");
	}
}

SHIV.Classes.VisualizationManager.prototype.createChartUlElem = function(visualization) {
	var possibilities = [
		{'name':"Scatter plot 2D",'type':"scatterplot2d",'dims-min':2},
		{'name':"Scatter plot 3D",'type':"scatterplot3d",'dims-min':3},
		{'name':"Histogram",'type':"histogram",'dims-min':1},
		{'name':"Bar chart",'type':"barchart",'dims-min':2},
		{'name':"Line chart",'type':"linechart",'dims-min':2},
		{'name':"Heatmap",'type':"heatmap",'dims-min':2},
		{'name':"Block chart",'type':"blockchart",'dims-min':2}
	];
	var ul = document.createElement('ul');
	ul.className = "dropdown-menu";
	ul.style.display = "none";
	for (var i = 0; i < possibilities.length; i++) {
		var chart = possibilities[i];
		if (visualization.dimensionality < chart["dims-min"]) continue;
		var li = document.createElement("li");
		var li_a = document.createElement("a");
		li_a.href = "#";
		li_a.className = "chart "+chart.type;
		li_a.onmouseup = function (evt) {
			var a = evt.target; 
			var li = a.parentNode;
			var ul = li.parentNode;
			ul.style.display = "none";
			var type = a.className.substring(6);
			var visId = ul.parentNode.parentNode.parentNode.id;
			visId = visId.substring(visId.indexOf('.')+1);
			var visualization = SHIV.visualizationManager.visualizations[visId];
			SHIV.visualizationManager.plotVisualization(visualization,type);
		};
		li_a.appendChild(document.createTextNode(chart.name));
		li.appendChild(li_a);
		ul.appendChild(li);
	}
	return ul;
}

/**
 * Method used to add a Visualization.
 * @param visualization Visualization to add
 */
SHIV.Classes.VisualizationManager.prototype.addVisualization = function(visualization) {
	if (visualization == null || visualization === undefined || visualization.ObjectType != "Visualization") return;
	if (this.visualizations[visualization.id] != null) return; // visualization already present
	this.visualizations[visualization.id] = visualization;
	this.visualizationsCount++;
	// TODO remove this
	var rowKey = "visualization."+visualization.id.replace(" ","_");
	if (document.getElementById(rowKey) != null)
		return; // already added
	var visualizationsTable = document.getElementById('tbl-visualizations')
	if(visualizationsTable==null)
		return;
	visualizationsTable = visualizationsTable.getElementsByTagName('TBODY')[0];
	var tr = document.createElement('tr');
	tr.id = rowKey;
	var td = document.createElement('td');
	var expand = document.createElement("span");
	expand.className = 'visualization-info-a glyphicon glyphicon-chevron-down';
	expand.ariaHidden = "true";
	expand.onclick = this.toggleVisualizationInfo;
	td.appendChild(expand);
	tr.appendChild(td);
	td = document.createElement('td');
	var span = document.createElement('span');
	span.title = 'id: '+visualization.id;
	if (visualization.description != null && visualization.description.trim().length > 0)
		span.title += "\ndescription: "+visualization.description;
	span.appendChild(document.createTextNode(visualization.name));
	td.appendChild(span);
	tr.appendChild(td);
	td = document.createElement('td');
	td.appendChild(document.createTextNode(visualization.dimensionality));
	tr.appendChild(td);
	td = document.createElement('td');

	var showIconElem = document.createElement('span');
	showIconElem.className = "dropdown"
	var showIcon = document.createElement("span");
	showIcon.className = 'glyphicon glyphicon-new-window';
	showIcon.ariaHidden = "true";
	showIcon.title = "Open Visualization";
	//showIcon.style.cursor = 'pointer';
	showIconElem.appendChild(showIcon);
	var caret = document.createElement('span');
	caret.className = "glyphicon glyphicon-triangle-bottom";
	caret.style.cursor = "pointer";
	caret.onmouseup = function(evt) {
		var caret = evt.target;
		var pElem = caret.parentNode;
		var ul = pElem.getElementsByTagName("UL")[0];
		if (ul.style.display == "none") {
			ul.style.display = "block";
		//	caret.className = "glyphicon glyphicon-triangle-top";
		} else {
			ul.style.display = "none";
		//	caret.className = "glyphicon glyphicon-triangle-bottom";
		}
	}
	showIconElem.appendChild(caret);
	
	showIconElem.appendChild(this.createChartUlElem(visualization));
	
	//showIcon.onmouseup = this.plotVisualization;
	td.appendChild(showIconElem);
	//tr.appendChild(td);
	//td = document.createElement('td');
	td.appendChild(document.createTextNode(" "));
	var deleteVis = document.createElement('span');
	deleteVis.className = 'glyphicon glyphicon-remove';
	deleteVis.ariaHidden = "true";
	deleteVis.title = "Delete Visualization";
	deleteVis.style.cursor = 'pointer';
	deleteVis.onmouseup = function(evt) {
		var buttons = [];
		var closeButton = new Object();
		closeButton['text'] = "No";
		closeButton['className'] = "btn-primary";
		var acceptButton = new Object();
		acceptButton['text'] = "Yes";
		acceptButton['className'] = "btn-default";
		acceptButton['onMouseUp'] = function(evt) {
			SHIV.server.executeCommand("visualization-destroy?vis-id="+visualization.id,function(jqXHR,data) {
				if (jqXHR.status == 200) {
					tr.parentNode.removeChild(tr);
				} else {
					var msg = "";
					if (data != null)
						msg = " - "+data['status-code'];
					SHIV.showMessage("error","Could not delete Visualization"+msg,null,"Error");
				}
			});
			return true;
		};
		buttons.push(acceptButton);
		buttons.push(closeButton);
		SHIV.modal("Delete Visualization?","Do you really wish to delete this Visualization?",false,buttons);
	}
	td.appendChild(deleteVis);
	tr.appendChild(td);
	visualizationsTable.appendChild(tr);
}
/**
 * Method used to parse visualization from the server.
 * @param visualization JSON Object with visualization to parse
 */
SHIV.Classes.VisualizationManager.prototype.parseVisualization = function(visualization)
{
	if (visualization == null || visualization === undefined) return null; // nothing to do
	// id,name,table,type,lodGenerator,dimensionality,lods
	var result = new SHIV.Classes.Visualization(visualization['id'],visualization['name'],visualization['table'],visualization['type'],visualization['lod-generator'],visualization['dimensionality'],visualization['LoDs']);
	if (visualization['description'] !== undefined && visualization['description'] != null)
		result.description = visualization['description'];
	result.defineDimension("x",visualization['x']);
	result.defineDimension("y",visualization['y']);
	result.defineDimension("z",visualization['z']);
	result.defineDimension("size",visualization['size']);
	result.defineDimension("color",visualization['color']);
	result.defineDimension("glyph",visualization['glyph']);
	return result;
}

/**
 * Method used to parse visualizations from the server.
 * @param visualizations JSON Object with visualizations to parse
 */
SHIV.Classes.VisualizationManager.prototype.parseVisualizations = function(visualizations) {
	if (visualizations === undefined || visualizations == null) return; // nothing to do
	if (visualizations['payload'] != null) { // we were given the message
		var blocks = visualizations['payload'];
		for (var b = 0; b < blocks.length; b++) {
			var block = blocks[b];
			var data = block['data'];
			var aux = data['visualizations'];
			for (var i = 0; i < aux.length; i++) {
				var visualization = this.parseVisualization(aux[i]);
				this.addVisualization(visualization);
			}
		}
	} else { // we were given the visualizations array
		for (var i = 0; i < visualizations.length; i++) {
			var visualization = this.parseVisualization(visualizations[i]);
			this.addVisualization(visualization);
		}
	}
	console.log('We are now tracking '+this.visualizationsCount+" visualizations");
}


/**
 * Callback used to update the local list of visualizations from the server.
 */
SHIV.Classes.VisualizationManager.prototype.updateVisualizations = function() {
	SHIV.server.executeCommand("visualizations-list",this.updateVisualizationsCallback);
}

/**
 * Callback used to update the local list of visualizations from the server
 * @param jqXHR XML HTTP Request to parse
 * @param data Either the data the requested or an error
 */
SHIV.Classes.VisualizationManager.prototype.updateVisualizationsCallback = function(jqXHR,data) {
	if (jqXHR.status == 200) {
		SHIV.visualizationManager.reset();
		SHIV.visualizationManager.parseVisualizations(data);
	}
}

